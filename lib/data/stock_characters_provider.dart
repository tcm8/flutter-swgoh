import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_swgoh/data/models/stock_character.dart';
import 'package:flutter_swgoh/shared/state/path_state.dart';
import 'package:get_it/get_it.dart';

class StockCharacterProvider {
  List<StockCharacter> characterList = [];
  List<String> factionList = [];
  List<String> abilityList = [];

  Future<void> loadCharacters() async {
    File jsonFile = File('${GetIt.I<PathState>().docPath}/swgoh.json');
    var exists = await jsonFile.exists();
    if (!exists) {
      jsonFile = await _copyFromAssets();
    }
    String data = await jsonFile.readAsString();

    Map<String, dynamic> jsonData = jsonDecode(data);
    characterList = List<StockCharacter>.from(
        jsonData['characters'].map((e) => StockCharacter.fromData(e)));

    factionList =
        (jsonData['factions'] as List).map((item) => item as String).toList();
    factionList.sort((a, b) => a.compareTo(b));

    abilityList = (jsonData['abilityClasses'] as List)
        .map((item) => item as String)
        .toList();
    abilityList.sort((a, b) => a.compareTo(b));
  }

  Future<File> _copyFromAssets() async {
    String docPath = GetIt.I<PathState>().docPath;

    var manifest = await rootBundle.loadString('AssetManifest.json');
    Map json = jsonDecode(manifest);
    for (var element in json.keys) {
      if (element.contains('characters')) {
        var path = (element as String).split('/');
        path[0] = docPath;
        var newPath = path.join('/');
        if (!File(newPath).existsSync()) {
          ByteData imgData = await rootBundle.load(element);
          var buffer = imgData.buffer;
          File(newPath).writeAsBytes(
              buffer.asUint8List(imgData.offsetInBytes, imgData.lengthInBytes));
        }
      }
    }

    String data = await rootBundle.loadString('assets/swgoh.json');
    File jsonFile = File('$docPath/swgoh.json');
    return jsonFile.writeAsString(data);
  }
}
