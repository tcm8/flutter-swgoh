import '../../pages/units/widgets/mod_names.dart';
import '../../schema/units/unit.dart';

class UnitsMock {
  static List<Unit> unitList() {
    return [
      Unit()
        ..name = 'Hera Syndula'
        ..url = '/characters/hera-syndulla/'
        ..factions = ['Leader', 'Phoenix', 'Rebel', 'Support']
        ..classes = [
          'Assist',
          'Counter',
          'Dispel',
          'Expose',
          'Gain Turn Meter',
          'Leader: +Max Health',
          'Leader: +Speed',
          'Reduce Cooldowns',
          'Revive'
        ]
        ..level = 3
        ..relic = true
        ..alignment = 'Light Side'
        ..image = 'tex.charui_hera_s3.png'
        ..activated = true
        ..recommendedMods = (RecommendedMods()
          ..sets = [ModSets.health.name, ModSets.speed.name]
          ..triangle = ModProps.protection.name
          ..circle = ModProps.protection.name
          ..cross = ModProps.protection.name
          ..arrow = ModProps.speed.name)
        ..equippedMods = (Mods()
          ..square = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "b")
          ..diamond = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "a")
          ..circle = (ModSlot()
            ..set = ModSets.potency.name
            ..dots = 5
            ..level = "c")
          ..arrow = (ModSlot()
            ..set = ModSets.potency.name
            ..dots = 5
            ..level = "c")
          ..triangle = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "d")
          ..cross = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "b"))
        ..currentStats = (UnitStats()
          ..armor = 35
          ..damage = 2524
          ..health = 20684
          ..potency = 25.37
          ..protection = 40877
          ..specialDamage = 3071
          ..relic = 3
          ..speed = 189
          ..tenacity = 51.38)
        ..targetStats = (UnitStats()
          ..armor = 44.16
          ..damage = 4080
          ..health = 42280
          ..potency = 34.55
          ..protection = 41208
          ..specialDamage = 5428
          ..relic = 6
          ..speed = 237
          ..tenacity = 60.76),
      Unit()
        ..name = 'Kanan Jarrus'
        ..url = '/characters/kanan-jarrus/'
        ..factions = ['Jedi', 'Phoenix', 'Rebel', 'Tank']
        ..classes = [
          'Counter',
          'Defense Up',
          'Dispel',
          'Foresight',
          'Gain Turn Meter',
          'Offense Down',
          'Protection Up',
          'Taunt'
        ]
        ..alignment = 'Light Side'
        ..image = 'tex.charui_kanan_s3.png'
        ..level = 12
        ..relic = false
        ..recommendedMods = (RecommendedMods()
          ..sets = [ModSets.health.name]
          ..triangle = ModProps.protection.name
          ..circle = ModProps.protection.name
          ..cross = ModProps.protection.name
          ..arrow = ModProps.speed.name)
        ..equippedMods = (Mods()
          ..square = (ModSlot()
            ..set = ModSets.tenacity.name
            ..dots = 5
            ..level = "c")
          ..diamond = (ModSlot()
            ..set = ModSets.health.name
            ..dots = 5
            ..level = "c")
          ..circle = (ModSlot()
            ..set = ModSets.health.name
            ..dots = 5
            ..level = "d")
          ..arrow = (ModSlot()
            ..set = ModSets.tenacity.name
            ..dots = 5
            ..level = "d")
          ..triangle = (ModSlot()
            ..set = ModSets.health.name
            ..dots = 5
            ..level = "d")
          ..cross = (ModSlot()
            ..set = ModSets.health.name
            ..dots = 5
            ..level = "c"))
        ..currentStats = (UnitStats()
          ..armor = 36.2
          ..damage = 2708
          ..health = 35153
          ..potency = 41.09
          ..protection = 48973
          ..specialDamage = 1408
          ..relic = 0
          ..speed = 158
          ..tenacity = 66.32)
        ..targetStats = (UnitStats()
          ..armor = 51.93
          ..damage = 3632
          ..health = 61742
          ..potency = 40.94
          ..protection = 59825
          ..specialDamage = 2860
          ..relic = 5
          ..speed = 183
          ..tenacity = 59.99),
      Unit()
        ..name = 'Sabine Wren'
        ..url = '/characters/sabine-wren/'
        ..factions = ['Attacker', 'Mandalorian', 'Phoenix', 'Rebel']
        ..classes = [
          'AoE',
          'Armor Shred',
          'Counter',
          'Critical Chance Up',
          'Expose',
          'Offense Up',
          'Reduce Cooldowns',
          'Stagger'
        ]
        ..alignment = 'Light Side'
        ..image = 'tex.charui_sabine_s3.png'
        ..level = 11
        ..relic = false
        ..recommendedMods = (RecommendedMods()
          ..sets = [ModSets.speed.name, ModSets.potency.name]
          ..triangle = ModProps.critDmg.name
          ..circle = ModProps.protection.name
          ..cross = ModProps.potency.name
          ..arrow = ModProps.speed.name)
        ..equippedMods = (Mods()
          ..square = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "d")
          ..diamond = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "b")
          ..circle = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "d")
          ..arrow = (ModSlot()
            ..set = ModSets.potency.name
            ..dots = 5
            ..level = "d")
          ..triangle = (ModSlot()
            ..set = ModSets.speed.name
            ..dots = 5
            ..level = "c")
          ..cross = (ModSlot()
            ..set = ModSets.potency.name
            ..dots = 5
            ..level = "c"))
        ..currentStats = (UnitStats()
          ..armor = 35
          ..damage = 2524
          ..health = 20684
          ..potency = 25.37
          ..protection = 40877
          ..specialDamage = 3071
          ..relic = 3
          ..speed = 189
          ..tenacity = 51.38)
        ..targetStats = (UnitStats()
          ..armor = 51.93
          ..damage = 3632
          ..health = 61742
          ..potency = 40.94
          ..protection = 59825
          ..specialDamage = 2860
          ..relic = 5
          ..speed = 183
          ..tenacity = 59.99),
      Unit()
        ..name = 'Chopper'
        ..url = '/characters/chopper/'
        ..factions = ['Droid', 'Phoenix', 'Rebel', 'Support']
        ..classes = [
          'AoE',
          'Assist',
          'Dispel',
          'Gain Turn Meter',
          'Offense Up',
          'Protection Up',
          'Reduce Cooldowns',
          'Speed Up',
          'Stun',
          'Taunt'
        ]
        ..alignment = 'Light Side'
        ..image = 'tex.charui_chopper.png'
        ..level = 10
        ..relic = false,
      Unit()..name = 'Captain Rex',
      Unit()
        ..name = 'Admiral Raddus'
        ..url = '/characters/admiral-raddus/'
        ..factions = [
          'Fleet Commander',
          'Leader',
          'Rebel',
          'Rogue One',
          'Support'
        ]
        ..classes = [
          'AoE',
          'Assist',
          'Counter',
          'Critical Chance Up',
          'Critical Damage Up',
          'Daze',
          'Defense Up',
          'Dispel',
          'Expose',
          'Gain Turn Meter',
          'Heal',
          'Healing Immunity',
          'Leader: +Critical',
          'Leader: +Defense',
          'Leader: +Speed',
          'Leader: +Tenacity',
          'Leader: Protection Up',
          'Offense Up',
          'Potency Up',
          'Protection Up',
          'Reduce Cooldowns',
          'Revive',
          'Taunt'
        ]
        ..alignment = 'Light Side'
        ..image = 'tex.charui_admiralraddus.png',
      Unit()
        ..name = 'Cassian Andor'
        ..url = '/characters/cassian-andor/'
        ..factions = ['Rebel', 'Rebel Fighter', 'Rogue One', 'Support']
        ..classes = [
          'Ability Block',
          'Assist',
          'Buff Immunity',
          'Counter',
          'Defense Down',
          'Defense Up',
          'Expose',
          'Gain Turn Meter',
          'Healing Immunity',
          'Offense Down',
          'Potency Up',
          'Protection Up',
          'Speed Down',
          'Tenacity Up'
        ]
        ..alignment = 'Light Side'
        ..image = 'tex.charui_cassian.png',
      Unit()
        ..name = 'Jyn Erso'
        ..url = '/characters/jyn-erso/'
        ..factions = [
          'Attacker',
          'Leader',
          'Rebel',
          'Rebel Fighter',
          'Rogue One'
        ]
        ..classes = [
          'Advantage',
          'AoE',
          'Buff Immunity',
          'Critical Damage Up',
          'Critical Hit Immunity',
          'Evasion Up',
          'Expose',
          'Gain Turn Meter',
          'Heal',
          'Offense Up',
          'Remove Turn Meter',
          'Revive',
          'Speed Up',
          'Stun',
          'Taunt'
        ]
        ..alignment = 'Light Side'
        ..image = 'tex.charui_jyn.png',
      Unit()
        ..name = "K-2SO"
        ..url = "/characters/k-2so/"
        ..factions = ["Droid", "Rebel", "Rebel Fighter", "Rogue One", "Tank"]
        ..classes = [
          "Assist",
          "Counter",
          "Daze",
          "Dispel",
          "Offense Down",
          "Taunt"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_k2so.png",
      Unit()
        ..name = "Baze Malbus"
        ..url = "/characters/baze-malbus/"
        ..factions = ["Rebel", "Rebel Fighter", "Rogue One", "Tank"]
        ..classes = [
          "AoE",
          "Assist",
          "Counter",
          "Dispel",
          "Dispel - All Enemies",
          "Gain Turn Meter",
          "Speed Down",
          "Taunt"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_bazemalbus.png",
      Unit()
        ..name = "Finn"
        ..url = "/characters/finn/"
        ..factions = ["Leader", "Resistance", "Tank"]
        ..classes = [
          "Advantage",
          "Counter",
          "Defense Up",
          "Dispel",
          "Expose",
          "Gain Turn Meter",
          "Leader: +Defense",
          "Leader: +Max Health",
          "Leader: +Speed",
          "Leader: Protection Up",
          "Protection Up",
          "Reduce Cooldowns",
          "Revive",
          "Stun",
          "Taunt"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_finnjakku.png",
      Unit()
        ..name = "Poe Dameron"
        ..url = "/characters/poe-dameron/"
        ..factions = ["Leader", "Resistance", "Tank"]
        ..classes = [
          "Buff Immunity",
          "Expose",
          "Offense Down",
          "Remove Turn Meter",
          "Taunt"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_poe.png",
      Unit()
        ..name = "Amilyn Holdo"
        ..url = "/characters/amilyn-holdo/"
        ..factions = ["Fleet Commander", "Resistance", "Tank"]
        ..classes = [
          "AoE",
          "Daze",
          "Defense Down",
          "Dispel",
          "Evasion Down",
          "Foresight",
          "Taunt"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_holdo.png",
      Unit()
        ..name = "Veteran Smuggler Chewbacca"
        ..url = "/characters/veteran-smuggler-chewbacca/"
        ..factions = ["Attacker", "Resistance", "Scoundrel", "Smuggler"]
        ..classes = [
          "AoE",
          "Daze",
          "Dispel",
          "Gain Turn Meter",
          "Reset Cooldown",
          "Speed Down",
          "Stun"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_tfa_chewbacca.png",
      Unit()
        ..name = "Veteran Smuggler Han Solo"
        ..url = "/characters/veteran-smuggler-han-solo/"
        ..factions = ["Attacker", "Resistance", "Scoundrel", "Smuggler"]
        ..classes = [
          "Ability Block",
          "AoE",
          "Bonus Attack",
          "Counter",
          "Gain Turn Meter",
          "Reset Cooldown",
          "Stagger",
          "Stun"
        ]
        ..alignment = "Light Side"
        ..image = "tex.charui_tfa_han.png",
    ];
  }
}
