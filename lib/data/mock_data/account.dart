import '../../schema/account.dart';

class AccountMock {
  static final xanzje = Account()
    ..name = "Xanzje Elias"
    ..gp = "5377483"
    ..unitCount = 229
    ..zetaCount = 58
    ..omicronCount = 7
    ..sevenStarCount = 180
    ..g12Count = 49
    ..relicCount = 46
    ..lightSideCount = 132
    ..darkSideCount = 96
    ..shipCount = 47
    ..sevenStarShips = 32;
}
