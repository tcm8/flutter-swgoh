import 'dart:convert';

import 'character_stats.dart';
import 'character_targets.dart';
import 'recommended_mods.dart';

class StockCharacter {
  final String id;
  final String name;
  final String url;
  final String image;
  final String alignment;
  final List<String> factions;
  final List<String> abilityClasses;
  final Map<String, CharacterStats> stats;
  final RecommendedMods mods;
  final CharacterTargets targets;

  const StockCharacter(
      {this.id = '',
      this.name = '',
      this.url = '',
      this.image = '',
      this.alignment = '',
      this.stats = const {},
      this.factions = const [],
      this.abilityClasses = const [],
      this.mods = const RecommendedMods(),
      this.targets = const CharacterTargets()});

  String toJson() {
    Map<String, dynamic> json = {
      'id': id,
      'name': name,
      'url': url,
      'factions': factions,
      'abilityClasses': abilityClasses,
      'alignment': alignment,
      'image': image,
      'stats': {
        'GEAR_12': stats['TW']?.toMap(),
        'GEAR_13': stats['TH']?.toMap(),
        'RELIC_1': stats['R1']?.toMap(),
        'RELIC_2': stats['R2']?.toMap(),
        'RELIC_3': stats['R3']?.toMap(),
        'RELIC_4': stats['R4']?.toMap(),
        'RELIC_5': stats['R5']?.toMap(),
        'RELIC_6': stats['R6']?.toMap(),
        'RELIC_7': stats['R7']?.toMap(),
        'RELIC_8': stats['R8']?.toMap(),
        'RELIC_9': stats['R9']?.toMap(),
      },
      'mods': mods.toMap(),
      'targets': targets.toMap()
    };

    return jsonEncode(json);
  }

  factory StockCharacter.fromData(dynamic rawData) {
    return StockCharacter(
        id: rawData['id'],
        name: rawData['name'],
        url: rawData['url'],
        factions: List<String>.from(rawData['factions']),
        abilityClasses: List<String>.from(rawData['abilityClasses']),
        alignment: rawData['alignment'],
        image: rawData['image'],
        mods: RecommendedMods.fromData(rawData['mods']),
        targets: CharacterTargets.fromMap(rawData['targets']),
        stats: {
          'GEAR_12': CharacterStats.fromJsonData(rawData['stats']['GEAR_12']),
          'GEAR_13': CharacterStats.fromJsonData(rawData['stats']['GEAR_13']),
          'RELIC_1': CharacterStats.fromJsonData(rawData['stats']['RELIC_1']),
          'RELIC_2': CharacterStats.fromJsonData(rawData['stats']['RELIC_2']),
          'RELIC_3': CharacterStats.fromJsonData(rawData['stats']['RELIC_3']),
          'RELIC_4': CharacterStats.fromJsonData(rawData['stats']['RELIC_4']),
          'RELIC_5': CharacterStats.fromJsonData(rawData['stats']['RELIC_5']),
          'RELIC_6': CharacterStats.fromJsonData(rawData['stats']['RELIC_6']),
          'RELIC_7': CharacterStats.fromJsonData(rawData['stats']['RELIC_7']),
          'RELIC_8': CharacterStats.fromJsonData(rawData['stats']['RELIC_8']),
          'RELIC_9': CharacterStats.fromJsonData(rawData['stats']['RELIC_9'])
        });
  }
}
