class RecommendedMods {
  final String set1;
  final String set2;
  final String set3;
  final String triangle;
  final String circle;
  final String cross;
  final String arrow;

  const RecommendedMods(
      {this.set1 = '',
      this.set2 = '',
      this.set3 = '',
      this.triangle = '',
      this.circle = '',
      this.cross = '',
      this.arrow = ''});

  RecommendedMods copyWith(
      {String? set1,
      String? set2,
      String? set3,
      String? triangle,
      String? circle,
      String? cross,
      String? arrow}) {
    return RecommendedMods(
        set1: set1 ?? this.set1,
        set2: set2 ?? this.set2,
        set3: set3 ?? this.set3,
        triangle: triangle ?? this.triangle,
        circle: circle ?? this.circle,
        cross: cross ?? this.cross,
        arrow: arrow ?? this.arrow);
  }

  Map<String, dynamic> toMap() {
    return {
      'set1': set1,
      'set2': set2,
      'set3': set3,
      'triangle': triangle,
      'circle': circle,
      'cross': cross,
      'arrow': arrow
    };
  }

  factory RecommendedMods.fromData(dynamic rawData) {
    return RecommendedMods(
        set1: rawData['set1'],
        set2: rawData['set2'],
        set3: rawData['set3'] ?? '',
        triangle: rawData['triangle'],
        circle: rawData['circle'],
        cross: rawData['cross'],
        arrow: rawData['arrow']);
  }
}
