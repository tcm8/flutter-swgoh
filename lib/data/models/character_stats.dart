import 'dart:convert';

class CharacterStats {
  final num power;
  final num strength;
  final num agility;
  final num intelligence;
  final num mastery;

  final num health;
  final num protection;
  final num speed;
  final num critDamage;
  final num potency;
  final num tenacity;
  final num healthSteal;

  final num physDamage;
  final num physCritChance;
  final num armorPenetration;
  final num physicalAccuracy;

  final num armor;
  final num dodge;
  final num physCritAvoidance;

  final num specialDamage;
  final num specialCritChance;
  final num resistancePenetration;
  final num specialAccuracy;

  final num resistance;
  final num deflectionChance;
  final num specialCritAvoidance;

  const CharacterStats({
    this.power = 0,
    this.strength = 0,
    this.agility = 0,
    this.intelligence = 0,
    this.mastery = 0,
    this.health = 0,
    this.protection = 0,
    this.speed = 0,
    this.critDamage = 0,
    this.potency = 0,
    this.tenacity = 0,
    this.healthSteal = 0,
    this.physDamage = 0,
    this.physCritChance = 0,
    this.armorPenetration = 0,
    this.physicalAccuracy = 0,
    this.armor = 0,
    this.dodge = 0,
    this.physCritAvoidance = 0,
    this.specialDamage = 0,
    this.specialCritChance = 0,
    this.resistancePenetration = 0,
    this.specialAccuracy = 0,
    this.resistance = 0,
    this.deflectionChance = 0,
    this.specialCritAvoidance = 0,
  });

  CharacterStats copyWith({
    num? power,
    num? strength,
    num? agility,
    num? intelligence,
    num? mastery,
    num? health,
    num? protection,
    num? speed,
    num? critDamage,
    num? potency,
    num? tenacity,
    num? healthSteal,
    num? physDamage,
    num? physCritChance,
    num? armorPenetration,
    num? physicalAccuracy,
    num? armor,
    num? dodge,
    num? physCritAvoidance,
    num? specialDamage,
    num? specialCritChance,
    num? resistancePenetration,
    num? specialAccuracy,
    num? resistance,
    num? deflectionChance,
    num? specialCritAvoidance,
  }) {
    return CharacterStats(
      power: power ?? this.power,
      strength: strength ?? this.strength,
      agility: agility ?? this.agility,
      intelligence: intelligence ?? this.intelligence,
      mastery: mastery ?? this.mastery,
      health: health ?? this.health,
      protection: protection ?? this.protection,
      speed: speed ?? this.speed,
      critDamage: critDamage ?? this.critDamage,
      potency: potency ?? this.potency,
      tenacity: tenacity ?? this.tenacity,
      healthSteal: healthSteal ?? this.healthSteal,
      physDamage: physDamage ?? this.physDamage,
      physCritChance: physCritChance ?? this.physCritChance,
      armorPenetration: armorPenetration ?? this.armorPenetration,
      physicalAccuracy: physicalAccuracy ?? this.physicalAccuracy,
      armor: armor ?? this.armor,
      dodge: dodge ?? this.dodge,
      physCritAvoidance: physCritAvoidance ?? this.physCritAvoidance,
      specialDamage: specialDamage ?? this.specialDamage,
      specialCritChance: specialCritChance ?? this.specialCritChance,
      resistancePenetration:
          resistancePenetration ?? this.resistancePenetration,
      specialAccuracy: specialAccuracy ?? this.specialAccuracy,
      resistance: resistance ?? this.resistance,
      deflectionChance: deflectionChance ?? this.deflectionChance,
      specialCritAvoidance: specialCritAvoidance ?? this.specialCritAvoidance,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'power': power,
      'strength': strength,
      'agility': agility,
      'intelligence': intelligence,
      'mastery': mastery,
      'health': health,
      'protection': protection,
      'speed': speed,
      'critDamage': critDamage,
      'potency': potency,
      'tenacity': tenacity,
      'healthSteal': healthSteal,
      'physDamage': physDamage,
      'physCritChance': physCritChance,
      'armorPenetration': armorPenetration,
      'physicalAccuracy': physicalAccuracy,
      'armor': armor,
      'dodge': dodge,
      'physCritAvoidance': physCritAvoidance,
      'specialDamage': specialDamage,
      'specialCritChance': specialCritChance,
      'resistancePenetration': resistancePenetration,
      'specialAccuracy': specialAccuracy,
      'resistance': resistance,
      'deflectionChance': deflectionChance,
      'specialCritAvoidance': specialCritAvoidance,
    };
  }

  String toJson() {
    var map = toMap();
    return jsonEncode(map);
  }

  factory CharacterStats.fromGgData(dynamic rawData) {
    return CharacterStats(
      power: rawData['power'],
      strength: rawData['stat']['2']['value'],
      agility: rawData['stat']['3']['value'],
      intelligence: rawData['stat']['4']['value'],
      mastery: rawData['stat']['61']['value'],
      health: rawData['stat']['1']['value'],
      protection: rawData['stat']['28']['value'],
      speed: rawData['stat']['5']['value'],
      critDamage: rawData['stat']['16']['value'],
      potency: rawData['stat']['17']['value'],
      tenacity: rawData['stat']['18']['value'],
      healthSteal: rawData['stat']['27']['value'],
      physDamage: rawData['stat']['6']['value'],
      physCritChance: rawData['stat']['14']['value'],
      armorPenetration: rawData['stat']['10']['value'],
      physicalAccuracy: rawData['stat']['37']['value'],
      armor: rawData['stat']['8']['value'],
      dodge: rawData['stat']['12']['value'],
      physCritAvoidance: rawData['stat']['39']['value'],
      specialDamage: rawData['stat']['7']['value'],
      specialCritChance: rawData['stat']['15']['value'],
      resistancePenetration: rawData['stat']['11']['value'],
      specialAccuracy: rawData['stat']['38']['value'],
      resistance: rawData['stat']['9']['value'],
      deflectionChance: rawData['stat']['13']['value'],
      specialCritAvoidance: rawData['stat']['40']['value'],
    );
  }

  factory CharacterStats.fromJsonData(dynamic rawData) {
    return CharacterStats(
        power: rawData['power'],
        strength: rawData['strength'],
        agility: rawData['agility'],
        intelligence: rawData['intelligence'],
        mastery: rawData['mastery'],
        health: rawData['health'],
        protection: rawData['protection'],
        speed: rawData['speed'],
        critDamage: rawData['critDamage'],
        potency: rawData['potency'],
        tenacity: rawData['tenacity'],
        healthSteal: rawData['healthSteal'],
        physDamage: rawData['physDamage'],
        physCritChance: rawData['physCritChance'],
        armorPenetration: rawData['armorPenetration'],
        physicalAccuracy: rawData['physicalAccuracy'],
        armor: rawData['armor'],
        dodge: rawData['dodge'],
        physCritAvoidance: rawData['physCritAvoidance'],
        specialDamage: rawData['specialDamage'],
        specialCritChance: rawData['specialCritChance'],
        resistancePenetration: rawData['resistancePenetration'],
        specialAccuracy: rawData['specialAccuracy'],
        resistance: rawData['resistance'],
        deflectionChance: rawData['deflectionChance'],
        specialCritAvoidance: rawData['specialCritAvoidance']);
  }
}
