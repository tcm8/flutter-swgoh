class CharacterTargets {
  final String relic;
  final String speed;
  final String health;
  final String protection;
  final String damage;
  final String specialDamage;
  final String potency;
  final String tenacity;
  final String armor;

  const CharacterTargets(
      {this.relic = '0',
      this.speed = '0',
      this.health = '0',
      this.protection = '0',
      this.damage = '0',
      this.specialDamage = '0',
      this.potency = '0%',
      this.tenacity = '0%',
      this.armor = '0%'});

  CharacterTargets copyWith({
    String? relic,
    String? speed,
    String? health,
    String? protection,
    String? damage,
    String? specialDamage,
    String? potency,
    String? tenacity,
    String? armor,
  }) {
    return CharacterTargets(
      relic: relic ?? this.relic,
      speed: speed ?? this.speed,
      health: health ?? this.health,
      protection: protection ?? this.protection,
      damage: damage ?? this.damage,
      specialDamage: specialDamage ?? this.specialDamage,
      potency: potency ?? this.potency,
      tenacity: tenacity ?? this.tenacity,
      armor: armor ?? this.armor,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'relic': relic,
      'speed': speed,
      'health': health,
      'protection': protection,
      'damage': damage,
      'specialDamage': specialDamage,
      'potency': potency,
      'tenacity': tenacity,
      'armor': armor,
    };
  }

  factory CharacterTargets.fromMap(Map<String, dynamic> map) {
    return CharacterTargets(
      relic: map['relic'] as String,
      speed: map['speed'] as String,
      health: map['health'] as String,
      protection: map['protection'] as String,
      damage: map['damage'] as String,
      specialDamage: map['specialDamage'] ?? map['special damage'] as String,
      potency: map['potency'] as String,
      tenacity: map['tenacity'] as String,
      armor: map['armor'] as String,
    );
  }
}
