import 'character_stats.dart';

class ParsedCharacterData {
  final Map<String, CharacterStats> stats;
  final List<String> factions;
  final List<String> abilityClasses;
  final String alignment;

  ParsedCharacterData({
    this.stats = const {},
    this.factions = const [],
    this.abilityClasses = const [],
    this.alignment = ''
  });

}
