import 'dart:async';

class StockFilterState {
  final List<String> selectedFactions = [];
  final List<String> selectedAbilities = [];
  final List<String> selectedFilters = [];

  final _nameStream = StreamController<String>.broadcast();
  final _factionStream = StreamController<List<String>>.broadcast();
  final _abilityStream = StreamController<List<String>>.broadcast();
  final _hintStream = StreamController<List<String>>.broadcast();

  Stream<String> get nameStream {
    return _nameStream.stream;
  }

  Stream<List<String>> get factionStream {
    return _factionStream.stream;
  }

  Stream<List<String>> get abilityStream {
    return _abilityStream.stream;
  }

  Stream<List<String>> get hintStream {
    return _hintStream.stream;
  }

  setTextFilter(String text) {
    _nameStream.add(text);
  }

  selectFaction(String faction) {
    if (selectedFactions.contains(faction)) {
      selectedFactions.remove(faction);
      selectedFactions.remove(faction);
      if (selectedFilters.isEmpty) {
        selectedFilters.add("No filters");
      }
    } else {
      selectedFilters.remove("No filters");
      selectedFilters.add(faction);
      selectedFactions.add(faction);
    }

    _factionStream.add([...selectedFactions]);
    _hintStream.add([...selectedFilters]);
  }

  selectAbility(String ability) {
    if (selectedAbilities.contains(ability)) {
      selectedAbilities.remove(ability);
      selectedAbilities.remove(ability);
      if (selectedFilters.isEmpty) {
        selectedFilters.add("No filters");
      }
    } else {
      selectedFilters.remove("No filters");
      selectedFilters.add(ability);
      selectedAbilities.add(ability);
    }

    _abilityStream.add([...selectedAbilities]);
    _hintStream.add([...selectedFilters]);
  }
}
