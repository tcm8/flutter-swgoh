import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/stock_characters_provider.dart';
import 'package:flutter_swgoh/old-pages/stock/state/stock_filter_state.dart';
import 'package:get_it/get_it.dart';

import '../../../data/models/stock_character.dart';

class StockPageState {
  final readingData = ValueNotifier(true);
  final _displayedCharacters = ValueNotifier<List<StockCharacter>>([]);

  final stockCharacterProvider = GetIt.I<StockCharacterProvider>();
  final filtersState = GetIt.I<StockFilterState>();

  List<StockCharacter> _filteredCharacters = [];
  List<String> _selectedFactions = [];
  List<String> _selectedAbilities = [];
  String _filterText = '';

  ValueNotifier<List<StockCharacter>> get characterList => _displayedCharacters;

  StockPageState() {
    init();
    filtersState.nameStream.listen((event) {
      _filterText = event.toLowerCase();
      filterCharacters();
    });
    filtersState.factionStream.listen((event) {
      _selectedFactions = [...event];
      filterCharacters();
    });
    filtersState.abilityStream.listen((event) {
      _selectedAbilities = [...event];
      filterCharacters();
    });
  }

  init() async {
    await stockCharacterProvider.loadCharacters();
    var characters = stockCharacterProvider.characterList;
    _displayedCharacters.value = [...characters];

    readingData.value = false;
  }

  filterCharacters() async {
    var filtered = [...stockCharacterProvider.characterList];
    if (_selectedFactions.isNotEmpty) {
      var factionSet = _selectedFactions.toSet();
      filtered = filtered.where((c) {
        var charSet = c.factions.toSet();
        var intersection = charSet.intersection(factionSet);
        return intersection.isNotEmpty;
      }).toList();
    }

    if (_selectedAbilities.isNotEmpty) {
      var abilitySet = _selectedAbilities.toSet();
      filtered = filtered.where((c) {
        var charSet = c.abilityClasses.toSet();
        var intersection = charSet.intersection(abilitySet);
        return intersection.isNotEmpty;
      }).toList();
    }

    if (_filterText.isNotEmpty) {
      filtered = filtered
          .where((c) => c.name.toLowerCase().contains(_filterText))
          .toList();
    }

    _filteredCharacters = filtered;
    _displayedCharacters.value = _filteredCharacters;
  }
}
