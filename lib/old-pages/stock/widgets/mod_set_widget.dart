import 'package:flutter/material.dart';

class ModSetWidget extends StatelessWidget {
  final Map<String, double> offsets = {
    "Health": -1,
    "Offense": -0.73,
    "Defense": -0.43,
    "Speed": -0.14,
    "Crit Chance": 0.15,
    "Crit Damage": 0.42,
    "Potency": 0.7,
    "Tenacity": 1,
    "-": 2
  };

  final String set;

  ModSetWidget({Key? key, this.set = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 36,
        height: 36,
        child: Container(
            margin: const EdgeInsets.only(right: 3, bottom: 6),
            child: ColorFiltered(
                colorFilter: tintMatrix(
                    tintColor: Theme.of(context).colorScheme.onSurface,
                    scale: 0.5),
                child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            alignment: Alignment(-1, offsets[set]!),
                            fit: BoxFit.none,
                            scale: 1.3,
                            image: const AssetImage(
                                'assets/images-ui/mod-icon-sprite-lg.png')))))));
  }

  ColorFilter tintMatrix({
    Color tintColor = Colors.grey,
    double scale = 1,
  }) {
    final int r = tintColor.red;
    final int g = tintColor.green;
    final int b = tintColor.blue;

    final double rTint = r / 255;
    final double gTint = g / 255;
    final double bTint = b / 255;

    const double rL = 0.2126;
    const double gL = 0.7152;
    const double bL = 0.0722;

    final double translate = 1 - scale * 0.5;

    return ColorFilter.matrix(<double>[
      (rL * rTint * scale),
      (gL * rTint * scale),
      (bL * rTint * scale),
      (0),
      (r * translate),
      (rL * gTint * scale),
      (gL * gTint * scale),
      (bL * gTint * scale),
      (0),
      (g * translate),
      (rL * bTint * scale),
      (gL * bTint * scale),
      (bL * bTint * scale),
      (0),
      (b * translate),
      (0),
      (0),
      (0),
      (1),
      (0),
    ]);
  }
}
