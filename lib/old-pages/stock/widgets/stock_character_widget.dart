import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/models/stock_character.dart';
import 'package:flutter_swgoh/shared/image_handler.dart';
import 'package:flutter_swgoh/theme/swgoh_colors.dart';

import './stock_character_mods_widget.dart';

class StockCharacterWidget extends StatelessWidget {
  final StockCharacter character;

  const StockCharacterWidget(
      {Key? key, this.character = const StockCharacter()})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: theme.dividerColor, width: 2.0))),
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
            color: theme.colorScheme.surface,
            padding: const EdgeInsets.fromLTRB(5, 3, 3, 3),
            child: Text(character.name,
                style: theme.textTheme.bodyMedium?.copyWith(
                    fontFamily: 'Oswald', fontWeight: FontWeight.bold)),
          ),
          Container(
              decoration: BoxDecoration(
                  color: theme.colorScheme.surface,
                  border: Border(
                      bottom: BorderSide(
                          color: theme.colorScheme.secondary, width: 1.0))),
              padding: const EdgeInsets.fromLTRB(5, 0, 2, 3),
              child: Text(character.factions.join(' .  '),
                  style: theme.textTheme.bodySmall?.copyWith(fontSize: 12))),
          Container(
            padding: const EdgeInsets.fromLTRB(15, 10, 20, 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                StockCharacterModsWidget(mods: character.mods),
                Expanded(child: Container()),
                Container(
                    height: 128,
                    width: 128,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        border: Border.all(
                            color: imageBorderColor(context), width: 5)),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: FutureBuilder<Image>(
                            future: ImageHandler.getImage(
                                path: ['characters', character.image],
                                height: 128),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return snapshot.requireData;
                              } else if (snapshot.hasError) {
                                return const Image(
                                    image: AssetImage(
                                        'assets/images-ui/kaboom.jpg'));
                              } else {
                                return const Image(
                                    image: AssetImage(
                                        'assets/images-ui/loading.jpg'));
                              }
                            }))),
              ],
            ),
          )
        ]));
  }

  Color imageBorderColor(BuildContext context) {
    return character.alignment == 'Light Side'
        ? Theme.of(context).dividerColor
        : DarkSideColors.imageBorderColor;
  }
}
