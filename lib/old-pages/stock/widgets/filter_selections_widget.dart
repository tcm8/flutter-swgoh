import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_swgoh/old-pages/stock/state/stock_filter_state.dart';
import 'package:flutter_swgoh/theme/theme_constants.dart';
import 'package:get_it/get_it.dart';

class FilterSelectionsWidget extends StatefulWidget {
  const FilterSelectionsWidget({Key? key}) : super(key: key);

  @override
  State<FilterSelectionsWidget> createState() => _FilterSelectionsWidgetState();
}

class _FilterSelectionsWidgetState extends State<FilterSelectionsWidget> {
  final filterState = GetIt.I<StockFilterState>();

  String filterText = 'No filters';
  String nameText = '';
  List<String> currentFactions = [];
  List<String> currentAbilities = [];
  final List<StreamSubscription> _subscriptions = [];

  @override
  void initState() {
    var factionSub = filterState.factionStream.listen((event) {
      currentFactions = event;
      _rebuildText();
    });
    var abilitySub = filterState.abilityStream.listen((event) {
      currentFactions = event;
      _rebuildText();
    });
    var nameSub = filterState.nameStream.listen((event) {
      nameText = event;
      _rebuildText();
    });

    _subscriptions.add(factionSub);
    _subscriptions.add(abilitySub);
    _subscriptions.add(nameSub);

    super.initState();
  }

  @override
  dispose() {
    for (var sub in _subscriptions) {
      sub.cancel();
    }

    super.dispose();
  }

  _rebuildText() {
    setState(() {
      if (nameText.isEmpty &&
          currentFactions.isEmpty &&
          currentAbilities.isEmpty) {
        filterText = 'No filters';
      } else {
        filterText =
            '$nameText ${currentFactions.join(', ')}${currentAbilities.join(', ')}';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Row(children: [
      Expanded(
          child: Container(
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.inverseSurface,
                  border: Border(top: BorderSide(color: theme.dividerColor))),
              child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 4.0),
                  child: Text(filterText,
                      style: TextStyle(
                          color: theme.colorScheme.onInverseSurface,
                          fontSize: ThemeConstants.bodyFontSizeSmall)))))
    ]);
  }
}
