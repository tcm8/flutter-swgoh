import 'package:flutter/material.dart';

class ModSlotWidget extends StatelessWidget {
  final String slot;
  final String target;
  final Map<String, double> slotOffsets = {
    "arrow": -0.96,
    "circle": -0.33,
    "triangle": 0.32,
    "cross": 1
  };
  final Map<String, double> setOffsets = {
    "Health": -0.98,
    "Offense": -0.765,
    "Defense": -0.46,
    "Speed": -0.30,
    "Critical Chance": -0.11,
    "Critical Damage": 0.13,
    "Potency": 0.34,
    "Tenacity": .58,
    "Protection": .785,
    "Critical Avoidance": 1,
    "-": 2
  };

  final Map<String, Map<String, double>> positions = {
    "arrow": {"top": 6, "right": 7},
    "circle": {"top": 14, "right": 14},
    "triangle": {"top": 15, "right": 13},
    "cross": {"top": 13, "right": 13}
  };

  ModSlotWidget({Key? key, this.slot = '', this.target = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          width: 46,
          height: 46,
          child: Container(
            decoration:
                BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
          ),
        ),
        SizedBox(
            width: 46,
            height: 46,
            child: Container(
                margin: const EdgeInsets.only(right: 3),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        alignment: Alignment(slotOffsets[slot]!, -1),
                        fit: BoxFit.none,
                        scale: 1.8,
                        image: const AssetImage(
                            'assets/images-ui/mod-slot-sprite.png'))))),
        Positioned(
          top: positions[slot]!['top'],
          right: positions[slot]!['right'],
          child: SizedBox(
              width: 18,
              height: 18,
              child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          alignment: Alignment(setOffsets[target]!, -1),
                          fit: BoxFit.none,
                          scale: 1.2,
                          image: const AssetImage(
                              'assets/images-ui/mod-icon-sprite.png'))))),
        )
      ],
    );
  }
}
