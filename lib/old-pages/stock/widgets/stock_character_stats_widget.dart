import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/models/character_stats.dart';
import 'package:flutter_swgoh/data/models/character_targets.dart';

class StockCharacterStatsWidget extends StatelessWidget {
  static const labelWidth = 105.0;

  final CharacterStats stats;
  final CharacterTargets targets;

  const StockCharacterStatsWidget(
      {Key? key,
      this.stats = const CharacterStats(),
      this.targets = const CharacterTargets()})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.fromLTRB(0, 5, 20, 0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Power")),
            Text(stats.power.toString())
          ]),
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Health")),
            Text(stats.health.round().toString())
          ]),
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Protection")),
            Text(stats.protection.round().toString())
          ]),
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Speed")),
            Text(stats.speed.round().toString())
          ]),
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Potency")),
            Text((stats.potency * 100).round().toString())
          ]),
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Tenacity")),
            Text((stats.tenacity * 100).round().toString())
          ]),
          Row(children: [
            const SizedBox(width: labelWidth, child: Text("Armor")),
            Text(stats.armor.round().toString())
          ])
        ]));
  }

  formatString(String strValue) {
    // var percent = '';
    // if (strValue.contains('%')) {
    //   percent = '%';
    // }
    // const value = Math.round(parseFloat(str.replace(/,/g, '')));
    //
    // return value + percent;
  }
}
