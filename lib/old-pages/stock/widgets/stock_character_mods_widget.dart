import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/models/recommended_mods.dart';

import './mod_set_widget.dart';
import './mod_slot_widget.dart';

class StockCharacterModsWidget extends StatelessWidget {
  final RecommendedMods mods;
  const StockCharacterModsWidget(
      {Key? key, this.mods = const RecommendedMods()})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(crossAxisAlignment: CrossAxisAlignment.start, children: _sets()),
        Row(crossAxisAlignment: CrossAxisAlignment.start, children: _slots()),
      ],
    );
  }

  List<Widget> _sets() {
    List<Widget> sets = [];

    if (mods.set1 != '') {
      sets.add(ModSetWidget(set: mods.set1));
    }
    if (mods.set2 != '') {
      sets.add(ModSetWidget(set: mods.set2));
    }
    if (mods.set3 != '') {
      sets.add(ModSetWidget(set: mods.set3));
    }

    return sets;
  }

  List<Widget> _slots() {
    List<Widget> slots = [];

    if (mods.arrow != '') {
      slots.add(ModSlotWidget(slot: 'arrow', target: mods.arrow));
    }
    if (mods.triangle != '') {
      slots.add(ModSlotWidget(slot: 'triangle', target: mods.triangle));
    }
    if (mods.cross != '') {
      slots.add(ModSlotWidget(slot: 'cross', target: mods.cross));
    }
    if (mods.circle != '') {
      slots.add(ModSlotWidget(slot: 'circle', target: mods.circle));
    }

    return slots;
  }
}
