import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/models/stock_character.dart';
import 'package:flutter_swgoh/old-pages/stock/state/stock_page_state.dart';
import 'package:flutter_swgoh/old-pages/stock/widgets/filter_selections_widget.dart';
import 'package:flutter_swgoh/shared/widgets/character_filter_widget.dart';

import './widgets/stock_character_widget.dart';

class StockPage extends StatelessWidget {
  StockPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // bool readingJson = watchX((StockPageState state) => state.readingData);
    // List<StockCharacter> characterList =
    //     watchX((StockPageState state) => state.characterList);
    bool readingJson = false;
    List<StockCharacter> characterList = [];

    return Column(children: [
      Expanded(
          child: readingJson
              ? const Center(
                  child: Text("Looking for data",
                      style: TextStyle(fontSize: 18, height: 1.6)))
              : characterList.isEmpty
                  ? const Center(
                      child: Text("No matches found.",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 18, height: 1.6)))
                  : ListView.builder(
                      itemCount: characterList.length,
                      itemBuilder: (context, index) {
                        return StockCharacterWidget(
                            character: characterList[index]);
                      })),
      const FilterSelectionsWidget(),
      const CharacterFilterWidget()
    ]);
  }
}
