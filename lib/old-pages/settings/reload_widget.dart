import 'package:flutter/material.dart';

class ReloadWidget extends StatefulWidget {
  final Function? handler;
  const ReloadWidget({this.handler, super.key});

  @override
  State<ReloadWidget> createState() => _ReloadWidgetState();
}

class _ReloadWidgetState extends State<ReloadWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  bool _spinning = false;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 2));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 46,
      child: InkWell(
          splashColor: Theme.of(context).colorScheme.secondary,
          onTap: () async {
            if (_spinning) {
              _controller.stop();
              _spinning = false;
            } else {
              if (widget.handler != null) {
                _controller.repeat();
                _spinning = true;
                await widget.handler!();
                _controller.stop();
                _spinning = false;
              }
            }
          },
          child: RotationTransition(
            turns: _controller,
            child: const Icon(Icons.refresh, size: 32.0),
          )),
    );
  }
}
