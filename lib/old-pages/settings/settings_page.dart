import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/stock_characters_provider.dart';
import 'package:flutter_swgoh/old-pages/settings/colors_preview.dart';
import 'package:flutter_swgoh/old-pages/settings/reload_widget.dart';
import 'package:flutter_swgoh/old-pages/settings/settings_state.dart';
import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:flutter_swgoh/shared/state/app.state.dart';

import 'package:get_it/get_it.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final appState = GetIt.I<AppState>();
  final isar = GetIt.I<IsarService>();
  final stockCharacterProvider = GetIt.I<StockCharacterProvider>();
  final swgohService = GetIt.I<SwgohService>();
  final state = GetIt.I<SettingsState>();

  final TextEditingController _acctTxtController = TextEditingController();

  bool darkSideToggle = false;
  bool addingAccount = false;
  String stockBtnText = 'Refresh Data';
  String stockTimeText = 'unknown';
  List<Account> accounts = [];
  Map<String, bool> loading = {};

  @override
  void initState() {
    super.initState();
    isar.loadAccounts().then((result) => setState(() {
          accounts = result;
          for (Account a in accounts) {
            loading.addAll({a.acctId!: false});
          }
        }));
  }

  @override
  Widget build(BuildContext context) {
    // final loadingStr = watchX((SettingsState state) => state.loadingStr);
    const loadingStr = "";
    return Container(
        padding: const EdgeInsets.only(left: 10.0, top: 25.0, right: 10.0),
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Text('Character Data',
              textAlign: TextAlign.start,
              style: Theme.of(context).textTheme.titleLarge),
          const SizedBox(height: 10),
          Row(
            children: [
              ElevatedButton(
                  onPressed: () {
                    // settingsState.loadStockCharactersFromServer();
                  },
                  child: Text(stockBtnText)),
              Expanded(
                  child: Text(
                "Last update: $stockTimeText",
                textAlign: TextAlign.end,
              ))
            ],
          ),
          const SizedBox(height: 25),
          /*
           *  Account List
           */
          Text('My Data',
              textAlign: TextAlign.start,
              style: Theme.of(context).textTheme.titleLarge),
          Builder(builder: (context) {
            List<Widget> accts = [];
            for (Account a in accounts) {
              accts.add(Row(
                children: [
                  ReloadWidget(handler: () async {
                    loading[a.acctId!] = true;
                    await swgohService.fetchPlayerRoster(a.acctId!);
                    a.lastUpdate = DateTime.now()
                        .toIso8601String()
                        .substring(0, 16)
                        .replaceAll(r'T', ' ');
                    isar.saveAccount(a);
                    loading[a.acctId!] = false;
                  }),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(a.name ?? ''),
                        Text(loading[a.acctId!] == true
                            ? loadingStr
                            : a.lastUpdate ?? 'unknown')
                      ])),
                  Text(a.acctId ?? '')
                ],
              ));
            }
            return Column(children: accts);
          }),
          addingAccount
              ? TextField(
                  controller: _acctTxtController,
                  onSubmitted: (value) async {
                    Account account =
                        await swgohService.fetchAccountInfo(value);
                    await isar.saveAccount(account);
                    setState(() {
                      accounts.add(account);
                      addingAccount = false;
                      _acctTxtController.text = '';
                    });
                  })
              : Container(),
          Row(
            children: [
              ElevatedButton.icon(
                  icon: const Icon(Icons.add, size: 22.0),
                  onPressed: () => setState((() {
                        addingAccount = !addingAccount;
                      })),
                  label: const Text("Add Account")),
            ],
          ),
          const SizedBox(height: 25),
          Text('App Theme',
              textAlign: TextAlign.start,
              style: Theme.of(context).textTheme.headlineLarge),
          Row(
            children: [
              Text('Light Side',
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall
                      ?.copyWith(fontSize: 18)),
              Expanded(
                  child: Switch(
                      value: darkSideToggle,
                      activeColor: Theme.of(context).colorScheme.primary,
                      inactiveTrackColor:
                          Theme.of(context).colorScheme.secondary,
                      inactiveThumbColor:
                          Theme.of(context).colorScheme.onSurface,
                      onChanged: (value) {
                        setState(() {
                          darkSideToggle = value;
                        });
                        appState.setThemeData(value);
                      })),
              Text('Dark Side',
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall
                      ?.copyWith(fontSize: 18)),
            ],
          ),
          TextButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const ColorPreview(),
                  ),
                );
              },
              child: const Text('Theme Preview'))
        ]));
  }
}
