import 'package:flutter/material.dart';

class ColorPreviewBlock extends StatelessWidget {
  final String text;
  final List<Color> colors;

  const ColorPreviewBlock({Key? key, required this.text, required this.colors})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: colors[0]),
            borderRadius: const BorderRadius.all(Radius.circular(8))),
        child:
            colors.length == 2 ? _twoUp(text, colors) : _threeUp(text, colors),
      ),
    );
  }

  Widget _twoUp(String text, List<Color> colors) {
    return Container(
      decoration: BoxDecoration(
        color: colors[0],
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Center(
          child: Text(
            text,
            style: TextStyle(color: colors[1]),
          ),
        ),
      ),
    );
  }

  Widget _threeUp(String text, List<Color> colors) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.vertical(
              top: Radius.circular(8), bottom: Radius.circular(0)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Center(child: Text(text, style: TextStyle(color: colors[0]))),
        ),
      ),
      Container(
          decoration: BoxDecoration(color: colors[0]),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Center(
                child: Text('on$text', style: TextStyle(color: colors[1]))),
          )),
      Container(
          decoration: BoxDecoration(
              color: colors[2],
              borderRadius: const BorderRadius.vertical(
                  top: Radius.circular(0), bottom: Radius.circular(6))),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Center(
                child: Text('$text container',
                    style: TextStyle(color: colors[3]))),
          ))
    ]);
  }
}
