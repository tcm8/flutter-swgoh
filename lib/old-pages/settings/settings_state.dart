import 'package:flutter/cupertino.dart';

class SettingsState {
  final loadingStr = ValueNotifier<String>('Starting...');
  int total = 0;

  setTotal(int t) {
    total = t;
    loadingStr.value = '0 of $total';
  }

  setCount(int c) {
    loadingStr.value = '$c of $total';
  }
}
