import 'package:flutter/material.dart';

import 'color_preview_block.dart';

class ColorPreview extends StatelessWidget {
  const ColorPreview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);

    return SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
          ColorPreviewBlock(text: 'Background', colors: [
            themeData.colorScheme.background,
            themeData.colorScheme.onBackground
          ]),
          ColorPreviewBlock(text: 'Primary', colors: [
            themeData.colorScheme.primary,
            themeData.colorScheme.onPrimary,
            themeData.colorScheme.primaryContainer,
            themeData.colorScheme.onPrimaryContainer
          ]),
          ColorPreviewBlock(text: 'Inverse Primary', colors: [
            themeData.colorScheme.inversePrimary,
            themeData.colorScheme.primary
          ]),
          ColorPreviewBlock(text: 'Secondary', colors: [
            themeData.colorScheme.secondary,
            themeData.colorScheme.onSecondary,
            themeData.colorScheme.secondaryContainer,
            themeData.colorScheme.onSecondaryContainer
          ]),
          ColorPreviewBlock(text: 'Tertiary', colors: [
            themeData.colorScheme.tertiary,
            themeData.colorScheme.onTertiary,
            themeData.colorScheme.tertiaryContainer,
            themeData.colorScheme.onTertiaryContainer
          ]),
          ColorPreviewBlock(text: 'Error', colors: [
            themeData.colorScheme.error,
            themeData.colorScheme.onError,
            themeData.colorScheme.errorContainer,
            themeData.colorScheme.onErrorContainer
          ]),
          ColorPreviewBlock(text: 'Surface', colors: [
            themeData.colorScheme.surface,
            themeData.colorScheme.onSurface
          ]),
          ColorPreviewBlock(text: 'SurfaceVariant', colors: [
            themeData.colorScheme.surfaceVariant,
            themeData.colorScheme.onSurfaceVariant
          ]),
          ColorPreviewBlock(text: 'InverseSurface', colors: [
            themeData.colorScheme.inverseSurface,
            themeData.colorScheme.onInverseSurface
          ]),
          ColorPreviewBlock(text: 'Scrim', colors: [
            themeData.colorScheme.background,
            themeData.colorScheme.scrim
          ]),
          ColorPreviewBlock(text: 'Shadow', colors: [
            themeData.colorScheme.background,
            themeData.colorScheme.shadow
          ]),
        ]))));
  }
}
