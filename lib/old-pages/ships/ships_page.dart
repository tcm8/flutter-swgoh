import 'package:flutter/material.dart';

class ShipsPage extends StatelessWidget {
  const ShipsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text('Ships', style: Theme.of(context).textTheme.displayMedium));
  }
}
