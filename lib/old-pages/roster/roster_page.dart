import 'package:flutter/material.dart';
import 'package:flutter_swgoh/old-pages/roster/roster_character_widget.dart';
import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/schema/roster/isar_roster_service.dart';
import 'package:flutter_swgoh/schema/roster/roster_character.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:get_it/get_it.dart';

class RosterPage extends StatefulWidget {
  const RosterPage({Key? key}) : super(key: key);

  @override
  RosterPageState createState() => RosterPageState();
}

class RosterPageState extends State<RosterPage> {
  final accountService = GetIt.I<IsarService>();
  final rosterService = GetIt.I<IsarRosterService>();

  List<Account> accounts = [];
  Account selectedAccount = Account();
  List<RosterCharacter> characters = [];

  @override
  void initState() {
    super.initState();
    loadAccounts();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Container(
        decoration: BoxDecoration(
            border: Border(
                bottom:
                    BorderSide(width: 1, color: theme.colorScheme.secondary))),
        child: Row(children: [
          const Expanded(
              child: Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text('Account'),
          )),
          DropdownButton<Account>(
              value: selectedAccount,
              items: accounts
                  .map((a) =>
                      DropdownMenuItem<Account>(value: a, child: Text(a.name!)))
                  .toList(),
              onChanged: (value) {
                setState(() {
                  selectedAccount = value!;
                });
              })
        ]),
      ),
      /* Come back to this
      Row(children: [
        Expanded(
          child: DropdownButton<String>(
              value: 'power',
              items: const [
                DropdownMenuItem(value: "power", child: Text('Power'))
              ],
              onChanged: (event) => {
                    // foo
                  }),
        ),
        InkWell(
            splashColor: Theme.of(context).colorScheme.secondary,
            onTap: () {},
            child: SizedBox(
              height: 45,
              width: 45,
              child: Container(
                margin: const EdgeInsets.all(2),
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: const BorderRadius.all(Radius.circular(6))),
                child: const Icon(Icons.sort, semanticLabel: 'Filters'),
              ),
            )),
      ]),
      */
      Expanded(
        child: ListView.builder(
            itemCount: characters.length,
            itemBuilder: (context, index) {
              return RosterCharacterWidget(character: characters[index]);
            }),
      )
    ]);
  }

  loadAccounts() async {
    /*
    List<Account> accounts = await accountService.loadAccounts();
    List<RosterCharacter> roster =
        await rosterService.getAccountRoster(accounts.first);
    setState(() {
      this.accounts = accounts;
      selectedAccount = this.accounts.first;
      characters = roster;
    });
   */
  }
}
