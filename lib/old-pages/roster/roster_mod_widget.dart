import 'package:flutter/material.dart';

class RosterModWidget extends StatelessWidget {
  final String slot;
  final int dots;
  final String set;
  final Map<String, double> slotOffsets = {
    "square5": -1,
    "arrow5": -0.8,
    "diamond5": -0.64,
    "triangle5": -0.45,
    "circle5": -0.28,
    "cross5": -0.1,
    "square6": 0.1,
    "arrow6": 0.28,
    "diamond6": 0.46,
    "triangle6": 0.64,
    "circle6": 0.82,
    "cross6": 1
  };

  RosterModWidget(
      {required this.slot, required this.dots, required this.set, super.key});

  @override
  Widget build(BuildContext context) {
    final slotName = '$slot$dots';
    return Padding(
      padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 4.0),
      child: Stack(alignment: Alignment.center, children: [
        SizedBox(
            height: 45,
            width: 45,
            child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        alignment: Alignment(slotOffsets[slotName]!, 0),
                        scale: 1.78,
                        fit: BoxFit.none,
                        image: const AssetImage(
                            'assets/images-ui/roster-slot-sheet.png')))))
      ]),
    );
  }
}
