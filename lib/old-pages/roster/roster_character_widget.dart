import 'package:flutter/material.dart';
import 'package:flutter_swgoh/old-pages/roster/roster_mod_widget.dart';
import 'package:flutter_swgoh/schema/roster/roster_character.dart';
import 'package:flutter_swgoh/shared/image_handler.dart';
import 'package:flutter_swgoh/theme/swgoh_colors.dart';
import 'package:logger/logger.dart';

class RosterCharacterWidget extends StatelessWidget {
  final RosterCharacter character;
  final logger = Logger();

  RosterCharacterWidget({super.key, required this.character});

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return Container(
      padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
      decoration: BoxDecoration(
          border: Border(
              bottom:
                  BorderSide(color: theme.colorScheme.secondary, width: 2.0))),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(character.name!,
            style: const TextStyle(
                fontFamily: 'Oswald',
                fontSize: 18,
                fontWeight: FontWeight.bold)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(alignment: Alignment.center, children: [
              Image(image: _getGearImage()),
              Text(_getLevel(),
                  style: theme.textTheme.bodyMedium?.copyWith(
                      fontSize: 16,
                      color: theme.colorScheme.onPrimaryContainer)),
            ]),
            Expanded(
                child: Column(children: [
              Row(children: [
                RosterModWidget(
                    slot: 'square',
                    dots: character.square!.dots!,
                    set: character.square!.set!),
                RosterModWidget(
                    slot: 'arrow',
                    dots: character.square!.dots!,
                    set: character.square!.set!),
              ]),
              Row(children: [
                RosterModWidget(
                    slot: 'diamond',
                    dots: character.square!.dots!,
                    set: character.square!.set!),
                RosterModWidget(
                    slot: 'triangle',
                    dots: character.square!.dots!,
                    set: character.square!.set!),
              ]),
              Row(children: [
                RosterModWidget(
                    slot: 'circle',
                    dots: character.square!.dots!,
                    set: character.square!.set!),
                RosterModWidget(
                    slot: 'cross',
                    dots: character.square!.dots!,
                    set: character.square!.set!),
              ])
            ])),
            Container(
                height: 128,
                width: 128,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    border: Border.all(
                        color: portraitBorderColor(context), width: 5)),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: FutureBuilder<Image>(
                        future: ImageHandler.getImage(
                            path: ['characters', character.image ?? ''],
                            height: 128),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.requireData;
                          } else if (snapshot.hasError) {
                            return const Image(
                                image:
                                    AssetImage('assets/images-ui/kaboom.jpg'));
                          } else {
                            return const Image(
                                image:
                                    AssetImage('assets/images-ui/loading.jpg'));
                          }
                        }))),
          ],
        )
      ]),
    );
  }

  String _getLevel() {
    int level = character.gearLevel! <= 12
        ? character.gearLevel!
        : character.relicLevel!;
    return level.toString();
  }

  AssetImage _getGearImage() {
    String image = character.gearLevel! <= 12
        ? 'gear-${character.gearLevel}'
        : 'relic-${character.alignment}';
    String path = 'assets/images-ui/$image.png';
    return AssetImage(path);
  }

  Color portraitBorderColor(BuildContext context) {
    return character.alignment == 'Light Side'
        ? Theme.of(context).dividerColor
        : DarkSideColors.imageBorderColor;
  }
}
