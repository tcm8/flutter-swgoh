import 'package:flutter/material.dart';

class SquadsPage extends StatefulWidget {
  const SquadsPage({Key? key}) : super(key: key);

  @override
  SquadsPageState createState() => SquadsPageState();
}

class SquadsPageState extends State<SquadsPage> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text('Squads', style: Theme.of(context).textTheme.displayMedium));
  }
}
