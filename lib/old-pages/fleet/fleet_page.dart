import 'package:flutter/material.dart';

class FleetPage extends StatelessWidget {
  const FleetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text('Fleet', style: Theme.of(context).textTheme.displayMedium));
  }
}
