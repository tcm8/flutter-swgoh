import 'package:flutter/material.dart';
import 'package:flutter_swgoh/pages/units/widgets/stat_widget.dart';
import 'package:flutter_swgoh/schema/units/empties.dart';
import 'package:flutter_swgoh/schema/units/unit.dart';
import 'package:flutter_swgoh/theme/theme_constants.dart';

class StatsTile extends StatelessWidget {
  final Unit unit;

  const StatsTile({super.key, required this.unit});

  @override
  Widget build(BuildContext context) {
    UnitStats current = unit.currentStats ?? emptyStats;
    UnitStats target = unit.targetStats ?? emptyStats;
    return Padding(
        padding: const EdgeInsets.fromLTRB(8, 4, 8, 2),
        child: Container(
            decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.surface,
                borderRadius: ThemeConstants.cardRadius),
            child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StatWidget(
                            name: 'Armor',
                            value: current.armor!,
                            target: target.armor!),
                        StatWidget(
                            name: 'Damage',
                            value: current.damage!.toDouble(),
                            target: target.damage!.toDouble()),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StatWidget(
                            name: 'Health',
                            value: current.health!.toDouble(),
                            target: target.health!.toDouble()),
                        StatWidget(
                            name: 'Potency',
                            value: current.potency!,
                            target: target.potency!),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StatWidget(
                            name: 'Protection',
                            value: current.protection!.toDouble(),
                            target: target.protection!.toDouble()),
                        StatWidget(
                            name: 'Special',
                            value: current.specialDamage!.toDouble(),
                            target: target.specialDamage!.toDouble()),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        StatWidget(
                            name: 'Relic',
                            value: current.relic!.toDouble(),
                            target: target.relic!.toDouble()),
                        StatWidget(
                            name: 'Speed',
                            value: current.speed!.toDouble(),
                            target: target.speed!.toDouble()),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        StatWidget(
                            name: 'Tenacity',
                            value: current.tenacity!.toDouble(),
                            target: target.tenacity!.toDouble())
                      ],
                    )
                  ],
                ))));
  }
}
