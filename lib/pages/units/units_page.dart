import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/mock_data/units.dart';
import 'package:flutter_swgoh/pages/units/overview_widget.dart';
import 'package:flutter_swgoh/pages/units/unit_card.dart';

import '../../schema/units/unit.dart';

class UnitsPage extends StatelessWidget {
  final List<Unit> units = UnitsMock.unitList();

  UnitsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(slivers: [
      SliverAppBar(
          expandedHeight: 200,
          collapsedHeight: 35,
          toolbarHeight: 0,
          pinned: true,
          bottom: const PreferredSize(
              preferredSize: Size(5, 15),
              child: Row(children: [Text("Filters"), Text("Go Here")])),
          flexibleSpace: FlexibleSpaceBar(background: OverviewWidget())),
      SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
        return UnitCard(unit: units[index]);
      }, childCount: units.length))
    ]);
  }
}
