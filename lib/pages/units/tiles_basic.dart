import 'package:flutter/material.dart';
import 'package:flutter_swgoh/pages/units/widgets/unit_image.dart';
import 'package:flutter_swgoh/pages/units/widgets/unit_level.dart';
import 'package:flutter_swgoh/pages/units/widgets/unit_name.dart';
import 'package:flutter_swgoh/schema/units/unit_null_objects.dart';
import 'package:flutter_swgoh/theme/theme_constants.dart';

import '../../schema/units/unit.dart';
import 'widgets/unit_mods.dart';

class BaseTile extends StatelessWidget {
  final Unit unit;

  const BaseTile({super.key, required this.unit});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    TextTheme textTheme = Theme.of(context).textTheme;
    return Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
        child: Container(
            decoration: BoxDecoration(
              color: colorScheme.surface,
              borderRadius: ThemeConstants.cardRadius,
            ),
            child: Padding(
                padding: ThemeConstants.cardInnerPadding,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 95,
                              child: Stack(children: [
                                UnitImage(
                                    alignment: unit.alignment ?? 'ERROR',
                                    imagePath: unit.image ?? ''),
                                Positioned(
                                  left: 20,
                                  bottom: -8,
                                  child: UnitLevel(
                                      level: unit.level ?? 1,
                                      relic: unit.relic ?? false,
                                      alignment:
                                          unit.alignment ?? "Light Side"),
                                )
                              ]),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  UnitName(name: unit.name ?? ''),
                                  UnitMods(
                                      recommended: unit.recommendedMods ??
                                          emptyRecommendedMods,
                                      equipped: unit.equippedMods ?? emptyMods)
                                ],
                              ),
                            )
                          ]),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(unit.factions?.join(", ") ?? '',
                            style: textTheme.bodySmall),
                      )
                    ]))));
  }
}
