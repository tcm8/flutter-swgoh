import 'package:flutter/material.dart';

class UnitLevel extends StatelessWidget {
  final int level;
  final bool relic;
  final String alignment;

  const UnitLevel(
      {required this.level,
      required this.relic,
      required this.alignment,
      super.key});

  @override
  Widget build(BuildContext context) {
    String aligned = alignment.toLowerCase().replaceAll(' ', '-');
    String imagePath = relic
        ? "assets/images-ui/relic-$aligned.png"
        : "assets/images-ui/gear-$level.png";
    return Stack(alignment: Alignment.center, children: [
      Image.asset(imagePath),
      Text(
        level.toString(),
        style: Theme.of(context)
            .textTheme
            .bodySmall!
            .copyWith(color: const Color(0xFFFFFFFF), fontSize: 13),
      )
    ]);
  }
}
