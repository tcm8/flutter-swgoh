import 'package:flutter/material.dart';

class StatWidget extends StatelessWidget {
  final String name;
  final double value;
  final double target;

  const StatWidget(
      {required this.name,
      required this.value,
      required this.target,
      super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Tooltip(
            message: '${formatNum(target)}${showPercent()}',
            child: Row(children: [
              value >= target
                  ? const Icon(Icons.arrow_drop_up, color: Colors.green)
                  : const Icon(
                      Icons.arrow_drop_down,
                      color: Colors.red,
                    ),
              Text(
                '$name: ${formatNum(value)}${showPercent()}',
                style: Theme.of(context)
                    .textTheme
                    .bodySmall!
                    .copyWith(fontSize: 16),
              )
            ])));
  }

  String showPercent() {
    return ['Potency', 'Tenacity'].contains(name) ? "%" : "";
  }

  String formatNum(num val) {
    return ['Potency', 'Tenacity'].contains(name) ? '$val' : '${val.toInt()}';
  }
}
