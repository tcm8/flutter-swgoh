import 'package:flutter/material.dart';

import '../../../schema/units/unit.dart';
import 'mod_names.dart';
import 'unit_mod.dart';

class UnitMods extends StatelessWidget {
  final Mods equipped;
  final RecommendedMods recommended;

  const UnitMods(
      {required this.equipped, required this.recommended, super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        UnitMod(
            slot: ModSlots.arrow,
            dots: equipped.arrow!.dots!,
            level: equipped.arrow!.level!,
            set: equipped.arrow!.set!),
        UnitMod(
            slot: ModSlots.triangle,
            dots: equipped.triangle!.dots!,
            level: equipped.triangle!.level!,
            set: equipped.triangle!.set!),
        UnitMod(
            slot: ModSlots.cross,
            dots: equipped.cross!.dots!,
            level: equipped.cross!.level!,
            set: equipped.cross!.set!),
        UnitMod(
            slot: ModSlots.square,
            dots: equipped.square!.dots!,
            level: equipped.square!.level!,
            set: equipped.square!.set!),
        UnitMod(
            slot: ModSlots.diamond,
            dots: equipped.diamond!.dots!,
            level: equipped.diamond!.level!,
            set: equipped.diamond!.set!),
        UnitMod(
            slot: ModSlots.circle,
            dots: equipped.circle!.dots!,
            level: equipped.circle!.level!,
            set: equipped.circle!.set!)
      ],
    );
  }
}
