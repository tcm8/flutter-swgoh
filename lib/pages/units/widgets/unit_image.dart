import 'package:flutter/material.dart';
import 'package:flutter_swgoh/theme/swgoh_colors.dart';

import '../../../shared/image_handler.dart';
import '../../../theme/light_side/light_side_colors.dart';

class UnitImage extends StatelessWidget {
  final String alignment;
  final String imagePath;

  const UnitImage({super.key, this.imagePath = '', this.alignment = 'ERROR'});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 16.0, bottom: 8.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            border: Border.all(color: imageBorderColor(), width: 5)),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: FutureBuilder<Image>(
                future: ImageHandler.getImage(
                    path: ['characters', imagePath], height: 84),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return snapshot.requireData;
                  } else if (snapshot.hasError) {
                    return const Image(
                        height: 64,
                        image: AssetImage('assets/images-ui/kaboom.jpg'));
                  } else {
                    return const Image(
                        height: 64,
                        image: AssetImage('assets/images-ui/loading.jpg'));
                  }
                })),
      ),
    );
  }

  Color imageBorderColor() {
    switch (alignment) {
      case 'Light Side':
        return LightSideColors.imageBorderColor;

      case 'Dark Side':
        return DarkSideColors.imageBorderColor;

      default:
        return const Color(0xff000000);
    }
  }
}
