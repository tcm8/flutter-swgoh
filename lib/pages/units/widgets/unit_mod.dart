import 'package:flutter/material.dart';

import 'mod_names.dart';

class UnitMod extends StatelessWidget {
  final ModSlots slot;
  final int dots;
  final String set;
  final String level;

  final Map<String, Alignment> setOffsets = const {
    "health": Alignment(-1, 0),
    "offense": Alignment(-0.77, 0),
    "defense": Alignment(-0.54, 0),
    "speed": Alignment(-0.33, 0),
    "critChance": Alignment(-0.12, 0),
    "critDmg": Alignment(0.1, 0),
    "potency": Alignment(0.33, 0),
    "tenacity": Alignment(0.56, 0),
    "protection": Alignment(0.78, 0),
    "critAvoid": Alignment(1, 0),
  };

  final Map<String, Alignment> stackOffsets = const {
    "arrow": Alignment(0.4, -0.25),
    "triangle": Alignment(-0.05, 0.38),
    "cross": Alignment.center,
    "square": Alignment.center,
    "diamond": Alignment.center,
    "circle": Alignment.center
  };

  final Map<String, Color> levelColors = const {
    "a": Colors.amber,
    "b": Colors.deepPurple,
    "c": Colors.blue,
    "d": Colors.green,
    "e": Colors.white
  };

  const UnitMod(
      {required this.slot,
      required this.dots,
      required this.level,
      required this.set,
      super.key});

  @override
  Widget build(BuildContext context) {
    String slotImg = "$dots$level-${slot.name}.png";
    return Padding(
        padding: const EdgeInsets.fromLTRB(4.0, 0, 0, 4.0),
        child: Stack(alignment: stackOffsets[slot.name]!, children: [
          SizedBox(
              height: 40,
              width: 40,
              child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          scale: 1.78,
                          fit: BoxFit.none,
                          image: AssetImage(
                              'assets/images-ui/mod-slots/$slotImg'))))),
          SizedBox(
            height: 18,
            width: 18,
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      alignment: setOffsets[set] ?? setOffsets["protection"]!,
                      fit: BoxFit.none,
                      scale: 1.3,
                      colorFilter: ColorFilter.mode(
                          levelColors[level]!, BlendMode.modulate),
                      image: const AssetImage(
                          'assets/images-ui/mod-icon-sprite.png'))),
            ),
          )
        ]));
  }
}
