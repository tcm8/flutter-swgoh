enum ModSlots { square, diamond, circle, arrow, triangle, cross }

enum ModSets {
  health,
  defense,
  critDmg,
  critChance,
  tenacity,
  offense,
  potency,
  speed,
  missing
}

enum ModProps {
  offense,
  protection,
  speed,
  defense,
  tenacity,
  critDmg,
  potency
}
