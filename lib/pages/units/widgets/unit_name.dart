import 'package:flutter/material.dart';
import 'package:flutter_swgoh/theme/light_side/light_side_colors.dart';

class UnitName extends StatelessWidget {
  final String name;

  const UnitName({Key? key, this.name = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Container(
        decoration: const BoxDecoration(
            border: Border(
                bottom:
                    BorderSide(width: 1, color: LightSideColors.analogA400))),
        child: Text(name,
            style: theme.textTheme.bodyLarge
                ?.copyWith(color: theme.colorScheme.onSurfaceVariant)),
      ),
    );
  }
}
