import 'package:flutter/material.dart';
import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:intl/intl.dart';
import 'package:watch_it/watch_it.dart';

class OverviewWidget extends StatelessWidget with WatchItMixin {
  final NumberFormat numbers = NumberFormat("###,###", "en_US");
  final IsarService _isarService = GetIt.I<IsarService>();

  OverviewWidget({super.key});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    TextTheme textScheme = Theme.of(context).textTheme;
    return Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                opacity: 0.15,
                image: AssetImage("assets/wallpaper/falcon.jpg"))),
        child: SafeArea(
            child: SizedBox(
          height: 75,
          child: Container(
            decoration: const BoxDecoration(color: Color(0x4D050506)),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: StreamBuilder<List<Account>>(
                stream: _isarService.selectedAccount(),
                builder: (context, snapshot) {
                  Account a = Account();
                  if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                    a = snapshot.data!.first;
                  }

                  return Column(
                      children: [
                        accountTitle(a, colorScheme, textScheme),
                        statTitles(textScheme),
                        stats(a, textScheme)
                      ]);
                }
              ))))));
  }

  Widget accountTitle(
      Account account, ColorScheme colorScheme, TextTheme textTheme) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Expanded(
            child: Text(account.name ?? 'No Account Selected',
                textAlign: TextAlign.start,
                style: textTheme.bodyLarge
                    ?.copyWith(color: colorScheme.onSurface))),
        Text(
          account.gp ?? "0",
          style: textTheme.bodyMedium?.copyWith(color: colorScheme.onSurface),
        )
      ]),
    );
  }

  Widget statTitles(TextTheme textTheme) {
    return DefaultTextStyle.merge(
        style: textTheme.bodySmall,
        child: const Row(children: [
          Expanded(child: Text("Units", textAlign: TextAlign.center)),
          Expanded(child: Text("7 Star", textAlign: TextAlign.center)),
          Expanded(child: Text("G12", textAlign: TextAlign.center)),
          Expanded(child: Text("Relic", textAlign: TextAlign.center)),
          Expanded(child: Text("Zeta", textAlign: TextAlign.center)),
          Expanded(child: Text("Omi", textAlign: TextAlign.center))
        ]));
  }

  Widget stats(Account account, TextTheme textTheme) {
    return DefaultTextStyle.merge(
        style: textTheme.bodySmall,
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(
              child: Text(account.unitCount.toString(),
                  textAlign: TextAlign.center)),
          Expanded(
              child: Text(account.sevenStarCount.toString(),
                  textAlign: TextAlign.center)),
          Expanded(
              child: Text(account.g12Count.toString(),
                  textAlign: TextAlign.center)),
          Expanded(
              child: Text(account.relicCount.toString(),
                  textAlign: TextAlign.center)),
          Expanded(
              child: Text(account.zetaCount.toString(),
                  textAlign: TextAlign.center)),
          Expanded(
              child: Text(account.omicronCount.toString(),
                  textAlign: TextAlign.center))
        ]));
  }
}
