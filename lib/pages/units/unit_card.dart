import 'package:flutter/material.dart';
import 'package:flutter_swgoh/pages/units/tiles_basic.dart';
import 'package:flutter_swgoh/pages/units/tiles_stats.dart';
import 'package:flutter_swgoh/schema/units/unit.dart';

class UnitCard extends StatelessWidget {
  final Unit unit;

  const UnitCard({super.key, required this.unit});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [BaseTile(unit: unit), StatsTile(unit: unit)],
    );
  }
}
