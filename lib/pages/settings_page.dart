import 'package:flutter/material.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:get_it/get_it.dart';

import '../schema/account.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final isar = GetIt.I<IsarService>();
  final swgoh = GetIt.I<SwgohService>();

  final ValueNotifier<bool> showAccountField = ValueNotifier(false);
  final ValueNotifier<bool> accountInFlight = ValueNotifier(false);
  final TextEditingController accountIdController = TextEditingController();

  @override
  void dispose() {
    showAccountField.dispose();
    accountInFlight.dispose();
    accountIdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
        appBar: AppBar(title: const Text('Settings')),
        body: Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Account", style: theme.textTheme.bodyLarge),
              Padding(
                  padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _accountWidgets()))
            ])));
  }

  List<Widget> _accountWidgets() {
    return [
      Row(children: [
        Expanded(
          child: StreamBuilder<List<Account>>(
              stream: isar.accountList(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Account>> snapshot) {
                if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                  return DropdownMenu(
                    expandedInsets: const EdgeInsets.only(right: 16),
                    inputDecorationTheme: _accountListDecoration(),
                    initialSelection: snapshot.data!.first,
                      dropdownMenuEntries: snapshot.data!
                      .map<DropdownMenuEntry<Account>>((Account value) {
                          return DropdownMenuEntry<Account>(value: value, label: value.acctId!);
                      }).toList());
                }

                return const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("No Accounts found"),
                );
              }),
        ),
        OutlinedButton(
            style: OutlinedButton.styleFrom(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 6)),
            onPressed: () {
              final current = showAccountField.value;
              showAccountField.value = !current;
            },
            child: const Icon(Icons.add, size: 20.0))
      ]),
      ValueListenableBuilder<bool>(
          valueListenable: showAccountField,
          builder: (context, value, _) {
            if (value) {
              return Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Expanded(
                    child: TextField(
                        controller: accountIdController,
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 0, horizontal: 16),
                            border: OutlineInputBorder(),
                            hintText: "Account ID"))),
                Padding(
                  padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                  child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              vertical: 12, horizontal: 6)),
                      onPressed: () async {
                        if (accountInFlight.value) {
                          return;
                        }

                        final accountId = accountIdController.text;
                        try {
                          accountInFlight.value = true;
                          Account account =
                              await swgoh.fetchAccountInfo(accountId);
                          account.selected = true;
                          await isar.saveAccount(account);
                        } on Exception catch (_) {}
                        accountInFlight.value = false;
                        showAccountField.value = false;
                        accountIdController.text = '';
                      },
                      child: ValueListenableBuilder<bool>(
                          valueListenable: accountInFlight,
                          builder: (context, inFlight, _) {
                            if (inFlight) {
                              return const Icon(Icons.refresh);
                            } else {
                              return const Icon(Icons.download);
                            }
                          })),
                )
              ]);
            }

            return Container();
          })
    ];
  }

  InputDecorationTheme _accountListDecoration() {
    return const InputDecorationTheme(
      border: OutlineInputBorder(),
        contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 8));
  }
}
