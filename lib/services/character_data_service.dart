import 'dart:convert';
import 'dart:io';

import 'package:flutter_swgoh/services/swgoh_gg/character_list.dart';
import 'package:get_it/get_it.dart';
import 'package:html/parser.dart' as parser;
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

import '../schema/roster/isar_roster_service.dart';
import './swgoh_gg/swgoh_service.dart';
import '../data/models/stock_character.dart';

class CharacterDataService {
  final swgohService = GetIt.I<SwgohService>();
  final rosterService = GetIt.I<IsarRosterService>();

  Future<List<StockCharacter>> collectStockCharacterData() async {
    var characterResponse = await swgohService.fetchStockCharacters();
    var document = parser.parse(characterResponse.body);
    List<dynamic> listData = await CharacterList().parse(document);
    List<String> json = [];
    List<StockCharacter> characters = listData[0];
    for (var character in characters) {
      json.add(character.toJson());
    }

    List<String> factions = listData[1].toList();
    List<String> abilities = listData[2].toList();
    String factionJson = jsonEncode(factions);
    String abilitiesJson = jsonEncode(abilities);

    String docPath = (await getApplicationDocumentsDirectory()).path;
    File jsonFile = File('$docPath/swgoh.json');
    DateFormat formatter = DateFormat('dd/MM/yyyy hh:mm');
    DateTime now = DateTime.now();
    jsonFile.writeAsStringSync('{'
        '"generatedAt": ${formatter.format(now)},'
        '"characters": [${json.join(',')}],'
        '"factions": $factionJson,'
        '"abilityClasses": $abilitiesJson'
        '}');

    return characters;
  }

  Future<void> collectPlayerData(String playerId) async {
    List<dynamic> listData = await swgohService.fetchPlayerRoster(playerId);

    for (var character in listData) {
      rosterService.saveCharacter(character);
    }
  }
}
