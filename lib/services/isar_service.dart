import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/schema/roster/roster_character.dart';
import 'package:flutter_swgoh/shared/state/path_state.dart';
import 'package:get_it/get_it.dart';
import 'package:isar/isar.dart';

class IsarService {
  final String instanceName = 'isarDb';
  late Isar _db;
  PathState pathState = GetIt.I<PathState>();

  Stream<List<Account>> selectedAccount() {
    return _db.accounts
        .filter()
        .selectedEqualTo(true)
        .watch(fireImmediately: true);
  }

  Stream<List<Account>> accountList() {
    return _db.accounts.where()
        .watch(fireImmediately: true);
  }

  Future<List<Account>> loadAccounts() async {
    return await isar.accounts.findAll();
  }

  get isar => _db;

  Future<Account?> activeAccount() {
    return _db.accounts.filter().selectedEqualTo(true).findFirst();
  }

  Future<List<Account>> allAccounts() {
    return _db.accounts.where().findAll();
  }

  saveAccount(Account account) async {
    return await _db.writeTxn(() => _db.accounts.put(account));
  }

  Future<IsarService> init() async {
    _db = await Isar.open([AccountSchema, RosterCharacterSchema],
        name: instanceName, directory: pathState.docPath);
    return this;
  }
}
