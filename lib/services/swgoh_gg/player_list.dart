import 'dart:io';

// import 'package:flutter_swgoh/pages/settings/settings_state.dart';
import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/schema/roster/isar_roster_service.dart';
import 'package:flutter_swgoh/schema/roster/roster_character.dart';
import 'package:flutter_swgoh/services/swgoh_gg/player_data/player_details_data.dart';
import 'package:flutter_swgoh/services/swgoh_gg/player_data/player_mod_data.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:get_it/get_it.dart';
import 'package:html/parser.dart' as parser;
import 'package:http/http.dart';
import 'package:logger/logger.dart';

class PlayerList {
  List<String> gearClasses = [
    '.character-portrait__relic',
    '.character-portrait__gframe--tier-12',
    '.character-portrait__gframe--tier-11',
    '.character-portrait__gframe--tier-10',
    '.character-portrait__gframe--tier-9',
    '.character-portrait__gframe--tier-8',
    '.character-portrait__gframe--tier-7',
    '.character-portrait__gframe--tier-6',
    '.character-portrait__gframe--tier-5',
    '.character-portrait__gframe--tier-4',
    '.character-portrait__gframe--tier-3',
    '.character-portrait__gframe--tier-2',
    '.character-portrait__gframe--tier-1'
  ];

  final roster = GetIt.I<IsarRosterService>();
  // final settingsState = GetIt.I<SettingsState>();
  final logger = Logger();

  Future<List<dynamic>> parse(Response response, Account account) async {
    var document = parser.parse(response.body);
    var characterList = document.querySelectorAll('.collection-char');

    List<RosterCharacter> characters = [];
    var count = 0;
    var total = await roster.getCount(account);
    // settingsState.setTotal(total);
    for (var toon in characterList) {
      // name
      String name =
          toon.querySelector('.collection-char-name-link')?.text.trim() ??
              'Name Not Found';

      // id
      String id = name.replaceAll(RegExp(r'''[()",']'''), '').toLowerCase();
      id = id.replaceAll(' ', '-');
      id = id.replaceAll(RegExp(r'[-]{2,}'), '-');

      // image name
      var imgSrc = toon
              .querySelector('img.character-portrait__img')
              ?.attributes['src'] ??
          '';
      var image = await SwgohService().fetchCharacterImage(imgSrc);

      // link
      var link = toon.querySelector('a');
      String url = link?.attributes['href'] ?? 'error';

      // stars
      var deadStars =
          toon.querySelectorAll('.character-portrait__star--inactive');
      var stars = 7 - deadStars.length;

      // gear
      var gearLevel = 1;
      var relicLevel = 0;
      var charLevel = 1;
      for (var gClass in gearClasses) {
        var gear = toon.querySelector(gClass);
        if (gear != null) {
          var firstSplit = gClass.split('__').last;
          if (firstSplit == 'relic') {
            gearLevel = 13;
            relicLevel = int.parse(gear.text);
            charLevel = 85;
          } else {
            var secondSplit = firstSplit.split('-').last;
            gearLevel = int.parse(secondSplit);
            var levelNode = toon.querySelector('.character-portrait__level');
            charLevel = int.parse(levelNode?.text ?? '0');
          }
          break;
        }
      }

      var character = RosterCharacter()
        ..charId = id
        ..name = name
        ..url = url
        ..image = image
        ..stars = stars
        ..gearLevel = gearLevel
        ..relicLevel = relicLevel
        ..level = charLevel
        ..accountId = account.acctId;

      var response = await SwgohService().fetchPage(url);
      var characterDoc = parser.parse(response.body);
      character.alignment =
          await PlayerDetailsData().parseAlignment(characterDoc);
      character.factions =
          await PlayerDetailsData().parseFactions(characterDoc);
      await PlayerModData().parseMods(characterDoc, character);

      await roster.saveCharacter(character);

      // settingsState.setCount(++count);

      // be a good citizen and don't pound swgoh.gg
      sleep(const Duration(seconds: 2));
    }

    return characters;
  }
}
