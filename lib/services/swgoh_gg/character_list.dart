import 'dart:io';

import 'package:flutter_swgoh/data/models/stock_character.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:html/dom.dart';

import '../../data/models/parsed_character_data.dart';
import 'character_home.dart';
import 'character_mod_data.dart';

class CharacterList {
  Future<List<dynamic>> parse(Document document) async {
    var elementList = document.querySelectorAll('li.character');
    List<StockCharacter> characters = [];
    Set<String> allFactions = {};
    Set<String> allAbilities = {};

    for (var toon in elementList) {
      var heading = toon.querySelector('.media-heading');
      var name =
          heading?.getElementsByTagName('h5').elementAt(0).text.trim() ?? '';

      String id = name.replaceAll(RegExp(r'''[()",']'''), '').toLowerCase();
      id = id.replaceAll(' ', '-');
      id = id.replaceAll(RegExp(r'-{2,}'), '-');

      var url = toon.querySelector('a')?.attributes['href'] ?? '';
      ParsedCharacterData data = await CharacterHome.parseStats(url);

      await CharacterModData.fetchPage(url);
      var mods = CharacterModData.parseMods();
      var targets = await CharacterModData.parseTargets();

      var imgSrc = toon.querySelector('img')?.attributes['src'] ?? '';
      var image = await SwgohService().fetchCharacterImage(imgSrc);

      var character = StockCharacter(
          id: id,
          name: name,
          url: url,
          image: image,
          alignment: data.alignment,
          stats: data.stats,
          factions: data.factions,
          abilityClasses: data.abilityClasses,
          mods: mods,
          targets: targets);

      allFactions.addAll(character.factions);
      allAbilities.addAll(character.abilityClasses);

      characters.add(character);

      // be a good citizen and don't pound swgoh.gg
      sleep(const Duration(seconds: 2));
    }

    return [characters, allFactions, allAbilities];
  }
}
