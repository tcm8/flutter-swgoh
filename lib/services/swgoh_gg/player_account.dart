import 'package:flutter_swgoh/schema/account.dart';
import 'package:html/dom.dart';
import 'package:http/http.dart';
import 'package:html/parser.dart' as parser;

class PlayerAccount {
  static Future<Account> parse(String id, Response response) async {
    var document = parser.parse(response.body);
    var playerName = document
            .querySelector(".content-container-aside .panel-title")
            ?.text
            .trim() ??
        '';
    var stats = document
        .querySelectorAll(".player-stat-info__stats .player-stat-info__stat");

    var gpString = stats.first
            .querySelector(".player-stat-info__stat-value strong")
            ?.text ??
        "0";

    Account account = Account()
      ..acctId = id
      ..name = playerName
      ..lastUpdate = DateTime.now().toIso8601String()
      ..gp = gpString;

    stats =
        document.querySelectorAll(".profile-group-content ul.panel-menu li");
    for (var li in stats) {
      _parseStat(li, account);
    }

    return account;
  }

  static void _parseStat(Element li, Account acct) {
    List<String> stat = li.text.trim().split("\n");
    switch (stat[0]) {
      case 'Characters':
        acct.unitCount = int.parse(stat[1]);
        break;

      case 'Zetas':
        acct.zetaCount = int.parse(stat[1]);
        break;

      case 'Omicrons':
        acct.omicronCount = int.parse(stat[1]);
        break;

      case '7* Characters':
        acct.sevenStarCount = int.parse(stat[1]);
        break;

      case 'Gear 13':
        acct.relicCount = int.parse(stat[1]);
        break;

      case 'Gear 12':
        acct.g12Count = int.parse(stat[1]);
        break;

      case 'Ships':
        acct.shipCount = int.parse(stat[1]);
        break;

      case '7* Ships':
        acct.sevenStarShips = int.parse(stat[1]);
        break;
    }
  }
}
