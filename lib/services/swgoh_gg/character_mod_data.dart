import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';

import '../../data/models/character_targets.dart';
import '../../data/models/recommended_mods.dart';

class CharacterModData {
  static late Document pageData;

  static Future<void> fetchPage(String url) async {
    var response = await SwgohService().fetchPage('${url}best-mods');
    pageData = parse(response.body);
  }

  static RecommendedMods parseMods() {
    final modBlock = pageData.querySelectorAll('.mod-sets-stats li');

    if (modBlock.isEmpty) {
      return const RecommendedMods();
    }

    final setBlock = modBlock[0].querySelectorAll('span');
    var set1 = setBlock[1].text;
    var set2 = setBlock[3].text;
    String set3 = '';
    if (setBlock.length > 4) {
      set3 = setBlock[5].text;
    }

    final triangle = modBlock[1].querySelector('strong')?.text ?? '';
    final cross = modBlock[2].querySelector('strong')?.text ?? '';
    final circle = modBlock[3].querySelector('strong')?.text ?? '';
    final arrow = modBlock[4].querySelector('strong')?.text ?? '';

    return RecommendedMods(
        set1: set1,
        set2: set2,
        set3: set3,
        triangle: triangle,
        circle: circle,
        cross: cross,
        arrow: arrow);
  }

  static Future<CharacterTargets> parseTargets() async {
    final dataSegment =
        pageData.querySelectorAll('.content-container-primary ul li')[3];
    final dataBlock = dataSegment.querySelectorAll('ul');
    if (dataBlock.isEmpty) {
      return const CharacterTargets();
    }
    final targetBlock = dataBlock[1];
    final list = targetBlock.querySelectorAll('li');
    Map<String, dynamic> dataMap = {};
    for (var l in list) {
      List<String> parts = l.text.split(': ');
      dataMap[parts[0].toLowerCase()] = parts[1];
    }

    return CharacterTargets.fromMap(dataMap);
  }
}
