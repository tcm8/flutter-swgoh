import 'dart:io';

import 'package:flutter_swgoh/services/swgoh_gg/player_list.dart';
import 'package:flutter_swgoh/services/swgoh_gg/player_account.dart';
import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/shared/state/path_state.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

class SwgohService {
  final PathState pathState = GetIt.I<PathState>();
  final String domain = "https://swgoh.gg";

  Future<http.Response> fetchStockCharacters() async {
    return await fetchPage('/');
  }

  Future<Account> fetchAccountInfo(String id) async {
    var response = await fetchPage('/p/$id');
    return await PlayerAccount.parse(id, response);
  }

  Future<String> fetchCharacterImage(String src) async {
    if (src.isEmpty) {
      return '';
    }

    String docPath = pathState.docPath;
    String path = "$docPath/characters";
    var filename = src.split('/').last;

    if (!File("$path$filename").existsSync()) {
      File file = File('$path/$filename');
      file.create(recursive: true);
      var request = Uri.parse(src);
      var response = await http.get(request);

      await file.writeAsBytes(response.bodyBytes);
    }

    return filename;
  }

  Future<List<dynamic>> fetchPlayerRoster(String playerId) async {
    final account = await fetchAccountInfo(playerId);
    var response = await fetchPage('/p/$playerId/characters');
    return PlayerList().parse(response, account);
  }

  Future<http.Response> fetchPage(String path) async {
    var request = Uri.parse('$domain$path');
    var response = await http.get(request);
    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('Response error for $path: ${response.statusCode}');
    }
  }
}
