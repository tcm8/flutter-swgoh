import 'dart:convert';

import 'package:flutter_swgoh/data/models/character_stats.dart';
import 'package:flutter_swgoh/data/models/parsed_character_data.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:html/parser.dart' as parser;

class CharacterHome {
  static Future<ParsedCharacterData> parseStats(String url) async {
    var response = await SwgohService().fetchPage(url);
    var document = parser.parse(response.body);
    var scriptTags = document.getElementsByTagName('script');

    Map<String, dynamic> collectedData = {};
    var collectingData = false;
    for (var js in scriptTags) {
      if (js.attributes.isNotEmpty) {
        continue;
      }

      if (js.text.contains('unit_data_lookup')) {
        var lines = js.text.split('\n');
        var key = '';
        for (var line in lines) {
          line = line.trim();
          if (collectingData && line.isNotEmpty) {
            if (line == '};') {
              break;
            }
            if (line == '},') {
              continue;
            }

            if (line.length > 2) {
              if (line.substring(0, 5) == 'table') {
                continue;
              }

              if (line.substring(0, 3) == 'raw') {
                var colon = line.indexOf(':');
                var trimmed = line.substring(colon + 1).trim();
                trimmed = trimmed.substring(0, trimmed.length - 1);
                //trimmed += '}';
                try {
                  var data = json.decode(trimmed);
                  collectedData[key] = data;
                } on FormatException catch (e) {
                  print('$key data failed to parse');
                  var notnull = e.offset ?? 0;
                  print(e.source.substring(notnull - 10, notnull + 1));
                }

                continue;
              }

              var colon = line.indexOf(':');
              key = line.substring(0, colon);
              continue;
            }
          }

          if (line.contains(RegExp(r'^window\.unit_data_lookup'))) {
            collectingData = true;
          }
        }
      }
    }

    Map<String, CharacterStats> stats = {
      'TW': CharacterStats.fromGgData(collectedData['GEAR_12']),
      'TH': CharacterStats.fromGgData(collectedData['GEAR_13']),
      'R1': CharacterStats.fromGgData(collectedData['RELIC_1']),
      'R2': CharacterStats.fromGgData(collectedData['RELIC_2']),
      'R3': CharacterStats.fromGgData(collectedData['RELIC_3']),
      'R4': CharacterStats.fromGgData(collectedData['RELIC_4']),
      'R5': CharacterStats.fromGgData(collectedData['RELIC_5']),
      'R6': CharacterStats.fromGgData(collectedData['RELIC_6']),
      'R7': CharacterStats.fromGgData(collectedData['RELIC_7']),
      'R8': CharacterStats.fromGgData(collectedData['RELIC_8']),
      'R9': CharacterStats.fromGgData(collectedData['RELIC_9']),
    };
    List<String> factions =
        collectedData['GEAR_12']['unit_data']['categories'].cast<String>();
    List<String> abilityClasses =
        collectedData['GEAR_12']['unit_data']['ability_classes'].cast<String>();
    String alignment = collectedData['GEAR_12']['unit_data']['alignment'];

    return ParsedCharacterData(
        alignment: alignment,
        stats: stats,
        factions: factions,
        abilityClasses: abilityClasses);
  }
}
