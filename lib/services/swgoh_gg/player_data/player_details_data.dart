import 'package:html/dom.dart';

class PlayerDetailsData {
  Future<String> parseAlignment(Document pageData) async {
    final summary = pageData.querySelector('.pc-char-overview');
    // alignment
    return summary!
        .querySelector('.char-alignment')!
        .classes
        .firstWhere((value) => value.startsWith('char-alignment-'))
        .split('ment-')
        .last;
  }

  Future<List<String>> parseFactions(Document pageData) async {
    final categories = pageData
        .querySelectorAll('.pc-char-overview-categories .char-category');
    List<String> fList = [];
    for (var el in categories) {
      fList.add(el.children[0].text);
    }

    return fList;
  }
}
