import 'package:flutter_swgoh/schema/roster/isar_roster_service.dart';
import 'package:flutter_swgoh/schema/roster/roster_character.dart';
import 'package:get_it/get_it.dart';
import 'package:html/dom.dart';

class PlayerModData {
  final roster = GetIt.I<IsarRosterService>();

  Map<String, String> modMap = {
    'statmod-base--set-1': 'health',
    'statmod-base--set-2': 'offense',
    'statmod-base--set-3': 'defense',
    'statmod-base--set-4': 'speed',
    'statmod-base--set-5': 'crit-chance',
    'statmod-base--set-6': 'crit-damage',
    'statmod-base--set-7': 'potency',
    'statmod-base--set-8': 'tenacity',
  };

  Map<String, String> slotMap = {
    'statmod-base--slot-1': 'square',
    'statmod-base--slot-2': 'arrow',
    'statmod-base--slot-3': 'diamond',
    'statmod-base--slot-4': 'triangle',
    'statmod-base--slot-5': 'circle',
    'statmod-base--slot-6': 'cross',
  };

  Future<void> parseMods(Document pageData, RosterCharacter character) async {
    final modBlocks = pageData.querySelectorAll('.pc-statmod-list .pc-statmod');
    for (var modData in modBlocks) {
      var base = modData.querySelector('.statmod-base');

      var slotClass = base?.classes.reduce(
          (value, element) => element.contains('slot') ? element : value);
      var slot = slotMap[slotClass] ?? 'foobar';

      var baseClass = base?.classes.reduce(
          (value, element) => element.contains('set') ? element : value);
      var set = modMap[baseClass] ?? 'foobar';

      var inner = modData.querySelector('.statmod-base-inner');
      var innerClass = inner?.classes.reduce(
          (value, element) => element.contains('shapekey') ? element : value);
      var pieces = innerClass?.split('-') ?? ['0', '0', '0'];
      var dots = int.parse(pieces[pieces.length - 3]);
      var level = int.parse(pieces[pieces.length - 1]);

      var mod = RosterMod()
        ..dots = dots
        ..level = level
        ..set = set;

      switch (slot) {
        case 'square':
          character.square = mod;
          break;

        case 'diamond':
          character.diamond = mod;
          break;

        case 'circle':
          character.circle = mod;
          break;

        case 'arrow':
          character.arrow = mod;
          break;

        case 'triangle':
          character.triangle = mod;
          break;

        case 'cross':
          character.cross = mod;
          break;

        default:
      }
    }
  }
}
