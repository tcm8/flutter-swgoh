import 'package:flutter/cupertino.dart';

class ThemeConstants {
  static const bodyFontSizeSmall = 17.0;
  static const bodyFontSizeMedium = 20.0;
  static const bodyFontSizeLarge = 26.0;
  static const cardOuterPadding = EdgeInsets.fromLTRB(8, 8, 8, 0);
  static const cardInnerPadding = EdgeInsets.fromLTRB(12, 12, 12, 3);
  static const cardRadius = BorderRadius.all(Radius.circular(8));
}
