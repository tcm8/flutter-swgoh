import 'package:flutter/material.dart';

class LightSideColors {
  static const primary50 = Color(0xffe4e5ea);
  static const primary100 = Color(0xffbbbfce);
  static const primary200 = Color(0xff9095ac);
  static const primary300 = Color(0xff686e8b);
  static const primary400 = Color(0xff4b5276);
  static const primary500 = Color(0xff2f3762);
  static const primary600 = Color(0xff2a305b);
  static const primary700 = Color(0xff232851);
  static const primary800 = Color(0xff1c2044);
  static const primary900 = Color(0xff11112d);

  static const comp50 = Color(0xfffffed9);
  static const comp100 = Color(0xfffcf9d4);
  static const comp200 = Color(0xfff7f4cf);
  static const comp300 = Color(0xfff1eec9);
  static const comp400 = Color(0xffcfcca9);
  static const comp500 = Color(0xffb1af8c);
  static const comp600 = Color(0xff868463);
  static const comp700 = Color(0xff717050);
  static const comp800 = Color(0xff515031);
  static const comp900 = Color(0xff2d2d11);

  static const analogA50 = Color(0xffe5ecf7);
  static const analogA100 = Color(0xffc3d0de);
  static const analogA200 = Color(0xffa1b0c2);
  static const analogA300 = Color(0xff7e91a7);
  static const analogA400 = Color(0xff657a92);
  static const analogA500 = Color(0xff4c647f);
  static const analogA600 = Color(0xff3f576f);
  static const analogA700 = Color(0xff314559);
  static const analogA800 = Color(0xff223344);
  static const analogA900 = Color(0xff111f2d);

  static const analogB50 = Color(0xfffff0ff);
  static const analogB100 = Color(0xffffe8ff);
  static const analogB200 = Color(0xfff4deff);
  static const analogB300 = Color(0xffe3cef5);
  static const analogB400 = Color(0xffbeaacf);
  static const analogB500 = Color(0xff9e8aae);
  static const analogB600 = Color(0xff756284);
  static const analogB700 = Color(0xff614f70);
  static const analogB800 = Color(0xff413150);
  static const analogB900 = Color(0xff1f112d);

  static const triadA50 = Color(0xffffe1ee);
  static const triadA100 = Color(0xffdebed1);
  static const triadA200 = Color(0xffba97ae);
  static const triadA300 = Color(0xff9a708b);
  static const triadA400 = Color(0xff825473);
  static const triadA500 = Color(0xff6b385b);
  static const triadA600 = Color(0xff5f3152);
  static const triadA700 = Color(0xff4f2645);
  static const triadA800 = Color(0xff3e1c3a);
  static const triadA900 = Color(0xff2d112d);

  static const triadB50 = Color(0xfff3e6e1);
  static const triadB100 = Color(0xffd6c1bd);
  static const triadB200 = Color(0xffb49995);
  static const triadB300 = Color(0xff94726c);
  static const triadB400 = Color(0xff7d564f);
  static const triadB500 = Color(0xff663b34);
  static const triadB600 = Color(0xff5b332d);
  static const triadB700 = Color(0xff4c2824);
  static const triadB800 = Color(0xff3d1d1c);
  static const triadB900 = Color(0xff2d1111);

  static const primary = analogA400;
  static const background = primary50;
  static const imageBorderColor = Color(0xff4ba3fc);
}
