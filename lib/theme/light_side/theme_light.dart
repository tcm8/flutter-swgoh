import 'package:flutter/material.dart';
import 'package:flutter_swgoh/theme/theme_constants.dart';

import './light_side_colors.dart';

class LightSideThemeLight {
  static final themeData = ThemeData(
      colorScheme: _thisColorScheme(),
      textTheme: _thisTextTheme(),
      dialogBackgroundColor: LightSideColors.analogA100,
      dividerColor: LightSideColors.primary,
      scaffoldBackgroundColor: LightSideColors.background,
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          selectedItemColor: LightSideColors.primary50,
          unselectedItemColor: LightSideColors.analogA500,
          backgroundColor: LightSideColors.analogA300,
          elevation: 0.0,
          type: BottomNavigationBarType.fixed));

  static ColorScheme _thisColorScheme() {
    return ColorScheme.fromSeed(
      seedColor: LightSideColors.primary,
      brightness: Brightness.light,
      // primary: LightSideColors.primary,
      // onPrimary: Color(0xffffffff),
      // primaryContainer: Color(0xffa1efff),
      // onPrimaryContainer: LightSideColors.primary50,
      // secondary: LightSideColors.analogA100,
      // onSecondary: LightSideColors.primary400,
      // secondaryContainer: Color(0xffcae8ea),
      // onSecondaryContainer: Color(0xff111313),
      // tertiary: Color(0xff585c82),
      // onTertiary: Color(0xffffffff),
      // tertiaryContainer: Color(0xffdfe0ff),
      // onTertiaryContainer: Color(0xff131314),
      // error: Color(0xffba1a1a),
      // onError: Color(0xffffffff),
      // errorContainer: Color(0xffffdad6),
      // onErrorContainer: Color(0xff141212),
      // background: LightSideColors.background,
      // onBackground: LightSideColors.primary200,
      // surface: Color(0xfff8fafb),
      // onSurface: Color(0xff090909),
      // surfaceVariant: LightSideColors.analogA50,
      // onSurfaceVariant: LightSideColors.analogB500,
      // outline: Color(0xff7c7c7c),
      // outlineVariant: Color(0xffc8c8c8),
      // shadow: Color(0xff000000),
      // scrim: Color(0xff000000),
      // inverseSurface: LightSideColors.analogA200,
      // onInverseSurface: Color(0xfff5f5f5),
      // inversePrimary: Color(0xff90dbe5),
      // surfaceTint: Color(0xff006876)
    );
  }

  static TextTheme _thisTextTheme() {
    return const TextTheme(
        bodyLarge: TextStyle(
            fontSize: ThemeConstants.bodyFontSizeLarge,
            color: LightSideColors.primary),
        bodyMedium: TextStyle(
            fontSize: ThemeConstants.bodyFontSizeMedium,
            color: LightSideColors.primary),
        bodySmall: TextStyle(
            fontSize: ThemeConstants.bodyFontSizeSmall,
            color: LightSideColors.primary));
  }
}
