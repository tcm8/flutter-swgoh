import 'package:flutter/material.dart';
import 'package:flutter_swgoh/theme/swgoh_colors.dart';

import '../theme_constants.dart';
import 'light_side_colors.dart';

class LightSideThemeDark {
  static final themeData = ThemeData(
      colorScheme: _thisColorScheme(),
      textTheme: _thisTextTheme(),
      dialogBackgroundColor: LightSideColorsOld.dialogBgColorDark,
      dividerColor: LightSideColorsOld.darkAccent,
      scaffoldBackgroundColor: LightSideColors.analogA800,
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          selectedItemColor: LightSideColorsOld.darkAccent,
          unselectedItemColor: Colors.white24,
          backgroundColor: LightSideColorsOld.extraDark,
          elevation: 0.0,
          type: BottomNavigationBarType.fixed));

  static ColorScheme _thisColorScheme() {
    return ColorScheme.fromSeed(
      seedColor: LightSideColors.primary,
      brightness: Brightness.dark,
      primary: LightSideColors.primary300,
      // onPrimary: Color(0xffffffff),
      // primaryContainer: Color(0xffa1efff),
      // onPrimaryContainer: LightSideColors.primary50,
      // secondary: LightSideColors.analogA100,
      // onSecondary: LightSideColors.primary400,
      // secondaryContainer: Color(0xffcae8ea),
      // onSecondaryContainer: Color(0xff111313),
      // tertiary: Color(0xff585c82),
      // onTertiary: Color(0xffffffff),
      // tertiaryContainer: Color(0xffdfe0ff),
      // onTertiaryContainer: Color(0xff131314),
      // error: Color(0xffba1a1a),
      // onError: Color(0xffffffff),
      // errorContainer: Color(0xffffdad6),
      // onErrorContainer: Color(0xff141212),
      // background: LightSideColors.background,
      // onBackground: LightSideColors.primary200,
      surface: LightSideColors.analogA900,
      // AppBar Title
      onSurface: LightSideColors.analogA200,
      // surfaceVariant: LightSideColors.analogA300,
      onSurfaceVariant: LightSideColors.analogA400,
      // outline: Color(0xff7c7c7c),
      // outlineVariant: Color(0xffc8c8c8),
      // shadow: Color(0xff000000),
      // scrim: Color(0xff000000),
      // inverseSurface: LightSideColors.analogA200,
      // onInverseSurface: Color(0xfff5f5f5),
      // inversePrimary: Color(0xff90dbe5),
      // surfaceTint: Color(0xff006876)
    );
  }

  static TextTheme _thisTextTheme() {
    return const TextTheme(
        bodyLarge: TextStyle(
            fontSize: ThemeConstants.bodyFontSizeLarge,
            color: LightSideColors.primary200),
        bodyMedium: TextStyle(
            fontSize: ThemeConstants.bodyFontSizeMedium,
            color: LightSideColors.primary200),
        bodySmall: TextStyle(
            fontSize: ThemeConstants.bodyFontSizeSmall,
            color: LightSideColors.primary200));
  }
}
