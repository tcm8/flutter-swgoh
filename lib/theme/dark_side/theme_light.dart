import 'package:flutter/material.dart';
import 'package:flutter_swgoh/theme/swgoh_colors.dart';

class DarkSideThemeLight {
  static final themeData = ThemeData(
      colorScheme: const ColorScheme(
          brightness: Brightness.light,
          background: DarkSideColors.bright,
          onBackground: DarkSideColors.brightAccent,
          primary: DarkSideColors.brightAccent,
          onPrimary: DarkSideColors.bright,
          secondary: DarkSideColors.darkAccent,
          onSecondary: DarkSideColors.extraBright,
          error: Colors.orange,
          onError: Colors.white,
          surface: DarkSideColors.brightAccent,
          onSurface: DarkSideColors.darkAccent),
      dividerColor: DarkSideColors.darkAccent,
      scaffoldBackgroundColor: DarkSideColors.bright,
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          selectedItemColor: DarkSideColors.bright,
          unselectedItemColor: Colors.white24,
          backgroundColor: DarkSideColors.darkAccent,
          elevation: 0.0,
          type: BottomNavigationBarType.fixed));
}
