import 'package:flutter/material.dart';
import 'package:flutter_swgoh/theme/swgoh_colors.dart';

class DarkSideThemeDark {
  static final themeData = ThemeData(
      colorScheme: const ColorScheme(
          brightness: Brightness.dark,
          primary: DarkSideColors.darkAccent,
          onPrimary: DarkSideColors.bright,
          secondary: DarkSideColors.darkAccent,
          onSecondary: DarkSideColors.dark,
          error: Colors.orange,
          onError: Colors.white,
          background: DarkSideColors.dark,
          onBackground: DarkSideColors.bright,
          surface: DarkSideColors.bright,
          onSurface: DarkSideColors.dark),
      dividerColor: DarkSideColors.darkAccent,
      scaffoldBackgroundColor: DarkSideColors.dark,
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          selectedItemColor: DarkSideColors.darkAccent,
          unselectedItemColor: Colors.white24,
          backgroundColor: DarkSideColors.extraDark,
          elevation: 0.0,
          type: BottomNavigationBarType.fixed));
}
