import 'package:flutter/material.dart';

class LightSideColorsOld {
  static const extraBright = Color(0xffffffff);
  static const bright = Color(0xffc6d7e0);
  static const brightAccent = Color(0xff66c3f1);
  static const imageBorderColor = Color(0xff089dde);
  static const dialogBgColorDark = Color(0xe51a283a);
  static const dialogBgColorLight = Color(0xe5bddffa);

  static const darkAccent = Color(0xff2a6496);
  static const dark = Color(0xff1a283a);
  static const extraDark = Color(0xff17171f);
}

class DarkSideColors {
  static const extraBright = Color(0xffffffff);
  static const bright = Color(0xfff8e3e3);
  static const brightAccent = Color(0xffff8f94);
  static const imageBorderColor = Color(0xffaf141b);

  static const darkAccent = Color(0xffbe5662);
  static const dark = Color(0xff210f0f);
  static const extraDark = Color(0xff110c0c);
}
