// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetAccountCollection on Isar {
  IsarCollection<Account> get accounts => this.collection();
}

const AccountSchema = CollectionSchema(
  name: r'Account',
  id: -6646797162501847804,
  properties: {
    r'acctId': PropertySchema(
      id: 0,
      name: r'acctId',
      type: IsarType.string,
    ),
    r'darkSideCount': PropertySchema(
      id: 1,
      name: r'darkSideCount',
      type: IsarType.long,
    ),
    r'g12Count': PropertySchema(
      id: 2,
      name: r'g12Count',
      type: IsarType.long,
    ),
    r'gp': PropertySchema(
      id: 3,
      name: r'gp',
      type: IsarType.string,
    ),
    r'lastUpdate': PropertySchema(
      id: 4,
      name: r'lastUpdate',
      type: IsarType.string,
    ),
    r'lightSideCount': PropertySchema(
      id: 5,
      name: r'lightSideCount',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 6,
      name: r'name',
      type: IsarType.string,
    ),
    r'omicronCount': PropertySchema(
      id: 7,
      name: r'omicronCount',
      type: IsarType.long,
    ),
    r'relicCount': PropertySchema(
      id: 8,
      name: r'relicCount',
      type: IsarType.long,
    ),
    r'selected': PropertySchema(
      id: 9,
      name: r'selected',
      type: IsarType.bool,
    ),
    r'sevenStarCount': PropertySchema(
      id: 10,
      name: r'sevenStarCount',
      type: IsarType.long,
    ),
    r'sevenStarShips': PropertySchema(
      id: 11,
      name: r'sevenStarShips',
      type: IsarType.long,
    ),
    r'shipCount': PropertySchema(
      id: 12,
      name: r'shipCount',
      type: IsarType.long,
    ),
    r'unitCount': PropertySchema(
      id: 13,
      name: r'unitCount',
      type: IsarType.long,
    ),
    r'zetaCount': PropertySchema(
      id: 14,
      name: r'zetaCount',
      type: IsarType.long,
    )
  },
  estimateSize: _accountEstimateSize,
  serialize: _accountSerialize,
  deserialize: _accountDeserialize,
  deserializeProp: _accountDeserializeProp,
  idName: r'id',
  indexes: {
    r'acctId': IndexSchema(
      id: 6661974980656274530,
      name: r'acctId',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'acctId',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {},
  getId: _accountGetId,
  getLinks: _accountGetLinks,
  attach: _accountAttach,
  version: '3.1.0+1',
);

int _accountEstimateSize(
  Account object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.acctId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.gp;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.lastUpdate;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _accountSerialize(
  Account object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.acctId);
  writer.writeLong(offsets[1], object.darkSideCount);
  writer.writeLong(offsets[2], object.g12Count);
  writer.writeString(offsets[3], object.gp);
  writer.writeString(offsets[4], object.lastUpdate);
  writer.writeLong(offsets[5], object.lightSideCount);
  writer.writeString(offsets[6], object.name);
  writer.writeLong(offsets[7], object.omicronCount);
  writer.writeLong(offsets[8], object.relicCount);
  writer.writeBool(offsets[9], object.selected);
  writer.writeLong(offsets[10], object.sevenStarCount);
  writer.writeLong(offsets[11], object.sevenStarShips);
  writer.writeLong(offsets[12], object.shipCount);
  writer.writeLong(offsets[13], object.unitCount);
  writer.writeLong(offsets[14], object.zetaCount);
}

Account _accountDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = Account();
  object.acctId = reader.readStringOrNull(offsets[0]);
  object.darkSideCount = reader.readLongOrNull(offsets[1]);
  object.g12Count = reader.readLongOrNull(offsets[2]);
  object.gp = reader.readStringOrNull(offsets[3]);
  object.id = id;
  object.lastUpdate = reader.readStringOrNull(offsets[4]);
  object.lightSideCount = reader.readLongOrNull(offsets[5]);
  object.name = reader.readStringOrNull(offsets[6]);
  object.omicronCount = reader.readLongOrNull(offsets[7]);
  object.relicCount = reader.readLongOrNull(offsets[8]);
  object.selected = reader.readBoolOrNull(offsets[9]);
  object.sevenStarCount = reader.readLongOrNull(offsets[10]);
  object.sevenStarShips = reader.readLongOrNull(offsets[11]);
  object.shipCount = reader.readLongOrNull(offsets[12]);
  object.unitCount = reader.readLongOrNull(offsets[13]);
  object.zetaCount = reader.readLongOrNull(offsets[14]);
  return object;
}

P _accountDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    case 2:
      return (reader.readLongOrNull(offset)) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    case 7:
      return (reader.readLongOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readBoolOrNull(offset)) as P;
    case 10:
      return (reader.readLongOrNull(offset)) as P;
    case 11:
      return (reader.readLongOrNull(offset)) as P;
    case 12:
      return (reader.readLongOrNull(offset)) as P;
    case 13:
      return (reader.readLongOrNull(offset)) as P;
    case 14:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _accountGetId(Account object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _accountGetLinks(Account object) {
  return [];
}

void _accountAttach(IsarCollection<dynamic> col, Id id, Account object) {
  object.id = id;
}

extension AccountByIndex on IsarCollection<Account> {
  Future<Account?> getByAcctId(String? acctId) {
    return getByIndex(r'acctId', [acctId]);
  }

  Account? getByAcctIdSync(String? acctId) {
    return getByIndexSync(r'acctId', [acctId]);
  }

  Future<bool> deleteByAcctId(String? acctId) {
    return deleteByIndex(r'acctId', [acctId]);
  }

  bool deleteByAcctIdSync(String? acctId) {
    return deleteByIndexSync(r'acctId', [acctId]);
  }

  Future<List<Account?>> getAllByAcctId(List<String?> acctIdValues) {
    final values = acctIdValues.map((e) => [e]).toList();
    return getAllByIndex(r'acctId', values);
  }

  List<Account?> getAllByAcctIdSync(List<String?> acctIdValues) {
    final values = acctIdValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'acctId', values);
  }

  Future<int> deleteAllByAcctId(List<String?> acctIdValues) {
    final values = acctIdValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'acctId', values);
  }

  int deleteAllByAcctIdSync(List<String?> acctIdValues) {
    final values = acctIdValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'acctId', values);
  }

  Future<Id> putByAcctId(Account object) {
    return putByIndex(r'acctId', object);
  }

  Id putByAcctIdSync(Account object, {bool saveLinks = true}) {
    return putByIndexSync(r'acctId', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByAcctId(List<Account> objects) {
    return putAllByIndex(r'acctId', objects);
  }

  List<Id> putAllByAcctIdSync(List<Account> objects, {bool saveLinks = true}) {
    return putAllByIndexSync(r'acctId', objects, saveLinks: saveLinks);
  }
}

extension AccountQueryWhereSort on QueryBuilder<Account, Account, QWhere> {
  QueryBuilder<Account, Account, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension AccountQueryWhere on QueryBuilder<Account, Account, QWhereClause> {
  QueryBuilder<Account, Account, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> acctIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'acctId',
        value: [null],
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> acctIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'acctId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> acctIdEqualTo(
      String? acctId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'acctId',
        value: [acctId],
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterWhereClause> acctIdNotEqualTo(
      String? acctId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'acctId',
              lower: [],
              upper: [acctId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'acctId',
              lower: [acctId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'acctId',
              lower: [acctId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'acctId',
              lower: [],
              upper: [acctId],
              includeUpper: false,
            ));
      }
    });
  }
}

extension AccountQueryFilter
    on QueryBuilder<Account, Account, QFilterCondition> {
  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'acctId',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'acctId',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'acctId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'acctId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'acctId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'acctId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'acctId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'acctId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'acctId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'acctId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'acctId',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> acctIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'acctId',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> darkSideCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'darkSideCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      darkSideCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'darkSideCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> darkSideCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'darkSideCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      darkSideCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'darkSideCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> darkSideCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'darkSideCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> darkSideCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'darkSideCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> g12CountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'g12Count',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> g12CountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'g12Count',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> g12CountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'g12Count',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> g12CountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'g12Count',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> g12CountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'g12Count',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> g12CountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'g12Count',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'gp',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'gp',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'gp',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'gp',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'gp',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'gp',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'gp',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'gp',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpContains(String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'gp',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'gp',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'gp',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> gpIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'gp',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lastUpdate',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lastUpdate',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastUpdate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lastUpdate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lastUpdate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lastUpdate',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'lastUpdate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'lastUpdate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'lastUpdate',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'lastUpdate',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastUpdate',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lastUpdateIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'lastUpdate',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lightSideCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'lightSideCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      lightSideCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'lightSideCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lightSideCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lightSideCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      lightSideCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lightSideCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lightSideCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lightSideCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> lightSideCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lightSideCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> omicronCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'omicronCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      omicronCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'omicronCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> omicronCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'omicronCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> omicronCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'omicronCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> omicronCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'omicronCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> omicronCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'omicronCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> relicCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relicCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> relicCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relicCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> relicCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relicCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> relicCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'relicCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> relicCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'relicCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> relicCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'relicCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> selectedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'selected',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> selectedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'selected',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> selectedEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'selected',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sevenStarCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      sevenStarCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sevenStarCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sevenStarCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      sevenStarCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'sevenStarCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'sevenStarCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'sevenStarCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarShipsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sevenStarShips',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      sevenStarShipsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sevenStarShips',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarShipsEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sevenStarShips',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition>
      sevenStarShipsGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'sevenStarShips',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarShipsLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'sevenStarShips',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> sevenStarShipsBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'sevenStarShips',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> shipCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'shipCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> shipCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'shipCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> shipCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'shipCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> shipCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'shipCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> shipCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'shipCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> shipCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'shipCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> unitCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unitCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> unitCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unitCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> unitCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unitCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> unitCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unitCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> unitCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unitCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> unitCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unitCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> zetaCountIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'zetaCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> zetaCountIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'zetaCount',
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> zetaCountEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'zetaCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> zetaCountGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'zetaCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> zetaCountLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'zetaCount',
        value: value,
      ));
    });
  }

  QueryBuilder<Account, Account, QAfterFilterCondition> zetaCountBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'zetaCount',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension AccountQueryObject
    on QueryBuilder<Account, Account, QFilterCondition> {}

extension AccountQueryLinks
    on QueryBuilder<Account, Account, QFilterCondition> {}

extension AccountQuerySortBy on QueryBuilder<Account, Account, QSortBy> {
  QueryBuilder<Account, Account, QAfterSortBy> sortByAcctId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'acctId', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByAcctIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'acctId', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByDarkSideCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'darkSideCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByDarkSideCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'darkSideCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByG12Count() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'g12Count', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByG12CountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'g12Count', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByGp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gp', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByGpDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gp', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByLastUpdate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdate', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByLastUpdateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdate', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByLightSideCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lightSideCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByLightSideCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lightSideCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByOmicronCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'omicronCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByOmicronCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'omicronCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByRelicCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByRelicCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortBySelected() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'selected', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortBySelectedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'selected', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortBySevenStarCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortBySevenStarCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortBySevenStarShips() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarShips', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortBySevenStarShipsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarShips', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByShipCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shipCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByShipCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shipCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByUnitCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByUnitCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByZetaCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'zetaCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> sortByZetaCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'zetaCount', Sort.desc);
    });
  }
}

extension AccountQuerySortThenBy
    on QueryBuilder<Account, Account, QSortThenBy> {
  QueryBuilder<Account, Account, QAfterSortBy> thenByAcctId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'acctId', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByAcctIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'acctId', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByDarkSideCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'darkSideCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByDarkSideCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'darkSideCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByG12Count() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'g12Count', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByG12CountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'g12Count', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByGp() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gp', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByGpDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gp', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByLastUpdate() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdate', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByLastUpdateDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastUpdate', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByLightSideCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lightSideCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByLightSideCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lightSideCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByOmicronCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'omicronCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByOmicronCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'omicronCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByRelicCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByRelicCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenBySelected() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'selected', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenBySelectedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'selected', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenBySevenStarCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenBySevenStarCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenBySevenStarShips() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarShips', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenBySevenStarShipsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'sevenStarShips', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByShipCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shipCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByShipCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'shipCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByUnitCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByUnitCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitCount', Sort.desc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByZetaCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'zetaCount', Sort.asc);
    });
  }

  QueryBuilder<Account, Account, QAfterSortBy> thenByZetaCountDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'zetaCount', Sort.desc);
    });
  }
}

extension AccountQueryWhereDistinct
    on QueryBuilder<Account, Account, QDistinct> {
  QueryBuilder<Account, Account, QDistinct> distinctByAcctId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'acctId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByDarkSideCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'darkSideCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByG12Count() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'g12Count');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByGp(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'gp', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByLastUpdate(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lastUpdate', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByLightSideCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lightSideCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByOmicronCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'omicronCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByRelicCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'relicCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctBySelected() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'selected');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctBySevenStarCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'sevenStarCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctBySevenStarShips() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'sevenStarShips');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByShipCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'shipCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByUnitCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unitCount');
    });
  }

  QueryBuilder<Account, Account, QDistinct> distinctByZetaCount() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'zetaCount');
    });
  }
}

extension AccountQueryProperty
    on QueryBuilder<Account, Account, QQueryProperty> {
  QueryBuilder<Account, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<Account, String?, QQueryOperations> acctIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'acctId');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> darkSideCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'darkSideCount');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> g12CountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'g12Count');
    });
  }

  QueryBuilder<Account, String?, QQueryOperations> gpProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'gp');
    });
  }

  QueryBuilder<Account, String?, QQueryOperations> lastUpdateProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lastUpdate');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> lightSideCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lightSideCount');
    });
  }

  QueryBuilder<Account, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> omicronCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'omicronCount');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> relicCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'relicCount');
    });
  }

  QueryBuilder<Account, bool?, QQueryOperations> selectedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'selected');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> sevenStarCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'sevenStarCount');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> sevenStarShipsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'sevenStarShips');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> shipCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'shipCount');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> unitCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unitCount');
    });
  }

  QueryBuilder<Account, int?, QQueryOperations> zetaCountProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'zetaCount');
    });
  }
}
