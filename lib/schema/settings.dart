import 'package:isar/isar.dart';

part 'settings.g.dart';

@collection
class Settings {
  Id? id;
  List<String>? accounts;
  String? theme;
}
