import 'package:isar/isar.dart';

part 'account.g.dart';

@collection
class Account {
  Id id = Isar.autoIncrement;
  bool? selected;

  @Index(unique: true, replace: true)
  String? acctId;

  String? name;
  String? lastUpdate;
  String? gp;
  int? unitCount;
  int? relicCount;
  int? g12Count;
  int? zetaCount;
  int? omicronCount;
  int? sevenStarCount;
  int? lightSideCount;
  int? darkSideCount;
  int? shipCount;
  int? sevenStarShips;
}
