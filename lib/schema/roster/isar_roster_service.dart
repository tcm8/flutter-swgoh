import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/schema/roster/roster_character.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:get_it/get_it.dart';
import 'package:isar/isar.dart';

class IsarRosterService {
  late Isar isar;
  IsarService service = GetIt.I<IsarService>();

  IsarRosterService() {
    isar = service.isar;
  }

  Future<int> getCount(Account account) async {
    return await isar.accounts.filter().acctIdEqualTo(account.acctId).count();
  }

  Future<List<RosterCharacter>> getAccountRoster(Account account) async {
    return await isar.rosterCharacters
        .where()
        .accountIdEqualTo(account.acctId)
        .findAll();
  }

  saveCharacter(RosterCharacter char) async {
    return await isar.writeTxn(() async {
      await isar.rosterCharacters.putByCharIdAccountId(char);
    });
  }
}
