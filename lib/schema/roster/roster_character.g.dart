// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'roster_character.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetRosterCharacterCollection on Isar {
  IsarCollection<RosterCharacter> get rosterCharacters => this.collection();
}

const RosterCharacterSchema = CollectionSchema(
  name: r'RosterCharacter',
  id: 7888910641657739378,
  properties: {
    r'accountId': PropertySchema(
      id: 0,
      name: r'accountId',
      type: IsarType.string,
    ),
    r'alignment': PropertySchema(
      id: 1,
      name: r'alignment',
      type: IsarType.string,
    ),
    r'arrow': PropertySchema(
      id: 2,
      name: r'arrow',
      type: IsarType.object,
      target: r'RosterMod',
    ),
    r'charId': PropertySchema(
      id: 3,
      name: r'charId',
      type: IsarType.string,
    ),
    r'circle': PropertySchema(
      id: 4,
      name: r'circle',
      type: IsarType.object,
      target: r'RosterMod',
    ),
    r'cross': PropertySchema(
      id: 5,
      name: r'cross',
      type: IsarType.object,
      target: r'RosterMod',
    ),
    r'diamond': PropertySchema(
      id: 6,
      name: r'diamond',
      type: IsarType.object,
      target: r'RosterMod',
    ),
    r'factions': PropertySchema(
      id: 7,
      name: r'factions',
      type: IsarType.stringList,
    ),
    r'gearLevel': PropertySchema(
      id: 8,
      name: r'gearLevel',
      type: IsarType.long,
    ),
    r'image': PropertySchema(
      id: 9,
      name: r'image',
      type: IsarType.string,
    ),
    r'level': PropertySchema(
      id: 10,
      name: r'level',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 11,
      name: r'name',
      type: IsarType.string,
    ),
    r'relicLevel': PropertySchema(
      id: 12,
      name: r'relicLevel',
      type: IsarType.long,
    ),
    r'square': PropertySchema(
      id: 13,
      name: r'square',
      type: IsarType.object,
      target: r'RosterMod',
    ),
    r'stars': PropertySchema(
      id: 14,
      name: r'stars',
      type: IsarType.long,
    ),
    r'triangle': PropertySchema(
      id: 15,
      name: r'triangle',
      type: IsarType.object,
      target: r'RosterMod',
    ),
    r'url': PropertySchema(
      id: 16,
      name: r'url',
      type: IsarType.string,
    )
  },
  estimateSize: _rosterCharacterEstimateSize,
  serialize: _rosterCharacterSerialize,
  deserialize: _rosterCharacterDeserialize,
  deserializeProp: _rosterCharacterDeserializeProp,
  idName: r'id',
  indexes: {
    r'charId_accountId': IndexSchema(
      id: -7733220958592478436,
      name: r'charId_accountId',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'charId',
          type: IndexType.hash,
          caseSensitive: true,
        ),
        IndexPropertySchema(
          name: r'accountId',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    ),
    r'accountId': IndexSchema(
      id: -1591555361937770434,
      name: r'accountId',
      unique: false,
      replace: false,
      properties: [
        IndexPropertySchema(
          name: r'accountId',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {r'RosterMod': RosterModSchema, r'ModStat': ModStatSchema},
  getId: _rosterCharacterGetId,
  getLinks: _rosterCharacterGetLinks,
  attach: _rosterCharacterAttach,
  version: '3.1.0+1',
);

int _rosterCharacterEstimateSize(
  RosterCharacter object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.accountId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.alignment;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.arrow;
    if (value != null) {
      bytesCount += 3 +
          RosterModSchema.estimateSize(
              value, allOffsets[RosterMod]!, allOffsets);
    }
  }
  {
    final value = object.charId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.circle;
    if (value != null) {
      bytesCount += 3 +
          RosterModSchema.estimateSize(
              value, allOffsets[RosterMod]!, allOffsets);
    }
  }
  {
    final value = object.cross;
    if (value != null) {
      bytesCount += 3 +
          RosterModSchema.estimateSize(
              value, allOffsets[RosterMod]!, allOffsets);
    }
  }
  {
    final value = object.diamond;
    if (value != null) {
      bytesCount += 3 +
          RosterModSchema.estimateSize(
              value, allOffsets[RosterMod]!, allOffsets);
    }
  }
  {
    final list = object.factions;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.image;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.square;
    if (value != null) {
      bytesCount += 3 +
          RosterModSchema.estimateSize(
              value, allOffsets[RosterMod]!, allOffsets);
    }
  }
  {
    final value = object.triangle;
    if (value != null) {
      bytesCount += 3 +
          RosterModSchema.estimateSize(
              value, allOffsets[RosterMod]!, allOffsets);
    }
  }
  {
    final value = object.url;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _rosterCharacterSerialize(
  RosterCharacter object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.accountId);
  writer.writeString(offsets[1], object.alignment);
  writer.writeObject<RosterMod>(
    offsets[2],
    allOffsets,
    RosterModSchema.serialize,
    object.arrow,
  );
  writer.writeString(offsets[3], object.charId);
  writer.writeObject<RosterMod>(
    offsets[4],
    allOffsets,
    RosterModSchema.serialize,
    object.circle,
  );
  writer.writeObject<RosterMod>(
    offsets[5],
    allOffsets,
    RosterModSchema.serialize,
    object.cross,
  );
  writer.writeObject<RosterMod>(
    offsets[6],
    allOffsets,
    RosterModSchema.serialize,
    object.diamond,
  );
  writer.writeStringList(offsets[7], object.factions);
  writer.writeLong(offsets[8], object.gearLevel);
  writer.writeString(offsets[9], object.image);
  writer.writeLong(offsets[10], object.level);
  writer.writeString(offsets[11], object.name);
  writer.writeLong(offsets[12], object.relicLevel);
  writer.writeObject<RosterMod>(
    offsets[13],
    allOffsets,
    RosterModSchema.serialize,
    object.square,
  );
  writer.writeLong(offsets[14], object.stars);
  writer.writeObject<RosterMod>(
    offsets[15],
    allOffsets,
    RosterModSchema.serialize,
    object.triangle,
  );
  writer.writeString(offsets[16], object.url);
}

RosterCharacter _rosterCharacterDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = RosterCharacter();
  object.accountId = reader.readStringOrNull(offsets[0]);
  object.alignment = reader.readStringOrNull(offsets[1]);
  object.arrow = reader.readObjectOrNull<RosterMod>(
    offsets[2],
    RosterModSchema.deserialize,
    allOffsets,
  );
  object.charId = reader.readStringOrNull(offsets[3]);
  object.circle = reader.readObjectOrNull<RosterMod>(
    offsets[4],
    RosterModSchema.deserialize,
    allOffsets,
  );
  object.cross = reader.readObjectOrNull<RosterMod>(
    offsets[5],
    RosterModSchema.deserialize,
    allOffsets,
  );
  object.diamond = reader.readObjectOrNull<RosterMod>(
    offsets[6],
    RosterModSchema.deserialize,
    allOffsets,
  );
  object.factions = reader.readStringList(offsets[7]);
  object.gearLevel = reader.readLongOrNull(offsets[8]);
  object.id = id;
  object.image = reader.readStringOrNull(offsets[9]);
  object.level = reader.readLongOrNull(offsets[10]);
  object.name = reader.readStringOrNull(offsets[11]);
  object.relicLevel = reader.readLongOrNull(offsets[12]);
  object.square = reader.readObjectOrNull<RosterMod>(
    offsets[13],
    RosterModSchema.deserialize,
    allOffsets,
  );
  object.stars = reader.readLongOrNull(offsets[14]);
  object.triangle = reader.readObjectOrNull<RosterMod>(
    offsets[15],
    RosterModSchema.deserialize,
    allOffsets,
  );
  object.url = reader.readStringOrNull(offsets[16]);
  return object;
}

P _rosterCharacterDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readObjectOrNull<RosterMod>(
        offset,
        RosterModSchema.deserialize,
        allOffsets,
      )) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readObjectOrNull<RosterMod>(
        offset,
        RosterModSchema.deserialize,
        allOffsets,
      )) as P;
    case 5:
      return (reader.readObjectOrNull<RosterMod>(
        offset,
        RosterModSchema.deserialize,
        allOffsets,
      )) as P;
    case 6:
      return (reader.readObjectOrNull<RosterMod>(
        offset,
        RosterModSchema.deserialize,
        allOffsets,
      )) as P;
    case 7:
      return (reader.readStringList(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readLongOrNull(offset)) as P;
    case 11:
      return (reader.readStringOrNull(offset)) as P;
    case 12:
      return (reader.readLongOrNull(offset)) as P;
    case 13:
      return (reader.readObjectOrNull<RosterMod>(
        offset,
        RosterModSchema.deserialize,
        allOffsets,
      )) as P;
    case 14:
      return (reader.readLongOrNull(offset)) as P;
    case 15:
      return (reader.readObjectOrNull<RosterMod>(
        offset,
        RosterModSchema.deserialize,
        allOffsets,
      )) as P;
    case 16:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _rosterCharacterGetId(RosterCharacter object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _rosterCharacterGetLinks(RosterCharacter object) {
  return [];
}

void _rosterCharacterAttach(
    IsarCollection<dynamic> col, Id id, RosterCharacter object) {
  object.id = id;
}

extension RosterCharacterByIndex on IsarCollection<RosterCharacter> {
  Future<RosterCharacter?> getByCharIdAccountId(
      String? charId, String? accountId) {
    return getByIndex(r'charId_accountId', [charId, accountId]);
  }

  RosterCharacter? getByCharIdAccountIdSync(String? charId, String? accountId) {
    return getByIndexSync(r'charId_accountId', [charId, accountId]);
  }

  Future<bool> deleteByCharIdAccountId(String? charId, String? accountId) {
    return deleteByIndex(r'charId_accountId', [charId, accountId]);
  }

  bool deleteByCharIdAccountIdSync(String? charId, String? accountId) {
    return deleteByIndexSync(r'charId_accountId', [charId, accountId]);
  }

  Future<List<RosterCharacter?>> getAllByCharIdAccountId(
      List<String?> charIdValues, List<String?> accountIdValues) {
    final len = charIdValues.length;
    assert(accountIdValues.length == len,
        'All index values must have the same length');
    final values = <List<dynamic>>[];
    for (var i = 0; i < len; i++) {
      values.add([charIdValues[i], accountIdValues[i]]);
    }

    return getAllByIndex(r'charId_accountId', values);
  }

  List<RosterCharacter?> getAllByCharIdAccountIdSync(
      List<String?> charIdValues, List<String?> accountIdValues) {
    final len = charIdValues.length;
    assert(accountIdValues.length == len,
        'All index values must have the same length');
    final values = <List<dynamic>>[];
    for (var i = 0; i < len; i++) {
      values.add([charIdValues[i], accountIdValues[i]]);
    }

    return getAllByIndexSync(r'charId_accountId', values);
  }

  Future<int> deleteAllByCharIdAccountId(
      List<String?> charIdValues, List<String?> accountIdValues) {
    final len = charIdValues.length;
    assert(accountIdValues.length == len,
        'All index values must have the same length');
    final values = <List<dynamic>>[];
    for (var i = 0; i < len; i++) {
      values.add([charIdValues[i], accountIdValues[i]]);
    }

    return deleteAllByIndex(r'charId_accountId', values);
  }

  int deleteAllByCharIdAccountIdSync(
      List<String?> charIdValues, List<String?> accountIdValues) {
    final len = charIdValues.length;
    assert(accountIdValues.length == len,
        'All index values must have the same length');
    final values = <List<dynamic>>[];
    for (var i = 0; i < len; i++) {
      values.add([charIdValues[i], accountIdValues[i]]);
    }

    return deleteAllByIndexSync(r'charId_accountId', values);
  }

  Future<Id> putByCharIdAccountId(RosterCharacter object) {
    return putByIndex(r'charId_accountId', object);
  }

  Id putByCharIdAccountIdSync(RosterCharacter object, {bool saveLinks = true}) {
    return putByIndexSync(r'charId_accountId', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByCharIdAccountId(List<RosterCharacter> objects) {
    return putAllByIndex(r'charId_accountId', objects);
  }

  List<Id> putAllByCharIdAccountIdSync(List<RosterCharacter> objects,
      {bool saveLinks = true}) {
    return putAllByIndexSync(r'charId_accountId', objects,
        saveLinks: saveLinks);
  }
}

extension RosterCharacterQueryWhereSort
    on QueryBuilder<RosterCharacter, RosterCharacter, QWhere> {
  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension RosterCharacterQueryWhere
    on QueryBuilder<RosterCharacter, RosterCharacter, QWhereClause> {
  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause> idEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      idGreaterThan(Id id, {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause> idLessThan(
      Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdIsNullAnyAccountId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'charId_accountId',
        value: [null],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdIsNotNullAnyAccountId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'charId_accountId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdEqualToAnyAccountId(String? charId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'charId_accountId',
        value: [charId],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdNotEqualToAnyAccountId(String? charId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [],
              upper: [charId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [charId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [charId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [],
              upper: [charId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdEqualToAccountIdIsNull(String? charId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'charId_accountId',
        value: [charId, null],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdEqualToAccountIdIsNotNull(String? charId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'charId_accountId',
        lower: [charId, null],
        includeLower: false,
        upper: [
          charId,
        ],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdAccountIdEqualTo(String? charId, String? accountId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'charId_accountId',
        value: [charId, accountId],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      charIdEqualToAccountIdNotEqualTo(String? charId, String? accountId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [charId],
              upper: [charId, accountId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [charId, accountId],
              includeLower: false,
              upper: [charId],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [charId, accountId],
              includeLower: false,
              upper: [charId],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'charId_accountId',
              lower: [charId],
              upper: [charId, accountId],
              includeUpper: false,
            ));
      }
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      accountIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'accountId',
        value: [null],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      accountIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'accountId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      accountIdEqualTo(String? accountId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'accountId',
        value: [accountId],
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterWhereClause>
      accountIdNotEqualTo(String? accountId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'accountId',
              lower: [],
              upper: [accountId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'accountId',
              lower: [accountId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'accountId',
              lower: [accountId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'accountId',
              lower: [],
              upper: [accountId],
              includeUpper: false,
            ));
      }
    });
  }
}

extension RosterCharacterQueryFilter
    on QueryBuilder<RosterCharacter, RosterCharacter, QFilterCondition> {
  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'accountId',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'accountId',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'accountId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'accountId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'accountId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'accountId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'accountId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'accountId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'accountId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'accountId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'accountId',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      accountIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'accountId',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'alignment',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'alignment',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'alignment',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'alignment',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alignment',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      alignmentIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'alignment',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      arrowIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'arrow',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      arrowIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'arrow',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'charId',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'charId',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'charId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'charId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'charId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'charId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'charId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'charId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'charId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'charId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'charId',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      charIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'charId',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      circleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'circle',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      circleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'circle',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      crossIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'cross',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      crossIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'cross',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      diamondIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'diamond',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      diamondIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'diamond',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'factions',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'factions',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'factions',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'factions',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'factions',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'factions',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      factionsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      gearLevelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'gearLevel',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      gearLevelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'gearLevel',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      gearLevelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'gearLevel',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      gearLevelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'gearLevel',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      gearLevelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'gearLevel',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      gearLevelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'gearLevel',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'image',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'image',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'image',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'image',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'image',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      imageIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'image',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      levelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      relicLevelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relicLevel',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      relicLevelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relicLevel',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      relicLevelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relicLevel',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      relicLevelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'relicLevel',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      relicLevelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'relicLevel',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      relicLevelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'relicLevel',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      squareIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'square',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      squareIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'square',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      starsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'stars',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      starsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'stars',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      starsEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'stars',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      starsGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'stars',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      starsLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'stars',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      starsBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'stars',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      triangleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'triangle',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      triangleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'triangle',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'url',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'url',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      urlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'url',
        value: '',
      ));
    });
  }
}

extension RosterCharacterQueryObject
    on QueryBuilder<RosterCharacter, RosterCharacter, QFilterCondition> {
  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition> arrow(
      FilterQuery<RosterMod> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'arrow');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition> circle(
      FilterQuery<RosterMod> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'circle');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition> cross(
      FilterQuery<RosterMod> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'cross');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition> diamond(
      FilterQuery<RosterMod> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'diamond');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition> square(
      FilterQuery<RosterMod> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'square');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterFilterCondition>
      triangle(FilterQuery<RosterMod> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'triangle');
    });
  }
}

extension RosterCharacterQueryLinks
    on QueryBuilder<RosterCharacter, RosterCharacter, QFilterCondition> {}

extension RosterCharacterQuerySortBy
    on QueryBuilder<RosterCharacter, RosterCharacter, QSortBy> {
  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByAccountId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accountId', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByAccountIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accountId', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByAlignment() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByAlignmentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByCharId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'charId', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByCharIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'charId', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByGearLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gearLevel', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByGearLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gearLevel', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByImage() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByImageDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByRelicLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicLevel', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByRelicLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicLevel', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByStars() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stars', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      sortByStarsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stars', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> sortByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }
}

extension RosterCharacterQuerySortThenBy
    on QueryBuilder<RosterCharacter, RosterCharacter, QSortThenBy> {
  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByAccountId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accountId', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByAccountIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accountId', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByAlignment() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByAlignmentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByCharId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'charId', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByCharIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'charId', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByGearLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gearLevel', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByGearLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'gearLevel', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByImage() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByImageDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByRelicLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicLevel', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByRelicLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relicLevel', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByStars() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stars', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy>
      thenByStarsDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'stars', Sort.desc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QAfterSortBy> thenByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }
}

extension RosterCharacterQueryWhereDistinct
    on QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> {
  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByAccountId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'accountId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByAlignment(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'alignment', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByCharId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'charId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct>
      distinctByFactions() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'factions');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct>
      distinctByGearLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'gearLevel');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByImage(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'image', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'level');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct>
      distinctByRelicLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'relicLevel');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByStars() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'stars');
    });
  }

  QueryBuilder<RosterCharacter, RosterCharacter, QDistinct> distinctByUrl(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'url', caseSensitive: caseSensitive);
    });
  }
}

extension RosterCharacterQueryProperty
    on QueryBuilder<RosterCharacter, RosterCharacter, QQueryProperty> {
  QueryBuilder<RosterCharacter, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<RosterCharacter, String?, QQueryOperations> accountIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'accountId');
    });
  }

  QueryBuilder<RosterCharacter, String?, QQueryOperations> alignmentProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'alignment');
    });
  }

  QueryBuilder<RosterCharacter, RosterMod?, QQueryOperations> arrowProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'arrow');
    });
  }

  QueryBuilder<RosterCharacter, String?, QQueryOperations> charIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'charId');
    });
  }

  QueryBuilder<RosterCharacter, RosterMod?, QQueryOperations> circleProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'circle');
    });
  }

  QueryBuilder<RosterCharacter, RosterMod?, QQueryOperations> crossProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'cross');
    });
  }

  QueryBuilder<RosterCharacter, RosterMod?, QQueryOperations>
      diamondProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'diamond');
    });
  }

  QueryBuilder<RosterCharacter, List<String>?, QQueryOperations>
      factionsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'factions');
    });
  }

  QueryBuilder<RosterCharacter, int?, QQueryOperations> gearLevelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'gearLevel');
    });
  }

  QueryBuilder<RosterCharacter, String?, QQueryOperations> imageProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'image');
    });
  }

  QueryBuilder<RosterCharacter, int?, QQueryOperations> levelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'level');
    });
  }

  QueryBuilder<RosterCharacter, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<RosterCharacter, int?, QQueryOperations> relicLevelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'relicLevel');
    });
  }

  QueryBuilder<RosterCharacter, RosterMod?, QQueryOperations> squareProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'square');
    });
  }

  QueryBuilder<RosterCharacter, int?, QQueryOperations> starsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'stars');
    });
  }

  QueryBuilder<RosterCharacter, RosterMod?, QQueryOperations>
      triangleProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'triangle');
    });
  }

  QueryBuilder<RosterCharacter, String?, QQueryOperations> urlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'url');
    });
  }
}

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const RosterModSchema = Schema(
  name: r'RosterMod',
  id: 4689689932557052862,
  properties: {
    r'dots': PropertySchema(
      id: 0,
      name: r'dots',
      type: IsarType.long,
    ),
    r'first': PropertySchema(
      id: 1,
      name: r'first',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'fourth': PropertySchema(
      id: 2,
      name: r'fourth',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'level': PropertySchema(
      id: 3,
      name: r'level',
      type: IsarType.long,
    ),
    r'primary': PropertySchema(
      id: 4,
      name: r'primary',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'second': PropertySchema(
      id: 5,
      name: r'second',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'set': PropertySchema(
      id: 6,
      name: r'set',
      type: IsarType.string,
    ),
    r'third': PropertySchema(
      id: 7,
      name: r'third',
      type: IsarType.object,
      target: r'ModStat',
    )
  },
  estimateSize: _rosterModEstimateSize,
  serialize: _rosterModSerialize,
  deserialize: _rosterModDeserialize,
  deserializeProp: _rosterModDeserializeProp,
);

int _rosterModEstimateSize(
  RosterMod object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.first;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.fourth;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.primary;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.second;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.set;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.third;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  return bytesCount;
}

void _rosterModSerialize(
  RosterMod object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.dots);
  writer.writeObject<ModStat>(
    offsets[1],
    allOffsets,
    ModStatSchema.serialize,
    object.first,
  );
  writer.writeObject<ModStat>(
    offsets[2],
    allOffsets,
    ModStatSchema.serialize,
    object.fourth,
  );
  writer.writeLong(offsets[3], object.level);
  writer.writeObject<ModStat>(
    offsets[4],
    allOffsets,
    ModStatSchema.serialize,
    object.primary,
  );
  writer.writeObject<ModStat>(
    offsets[5],
    allOffsets,
    ModStatSchema.serialize,
    object.second,
  );
  writer.writeString(offsets[6], object.set);
  writer.writeObject<ModStat>(
    offsets[7],
    allOffsets,
    ModStatSchema.serialize,
    object.third,
  );
}

RosterMod _rosterModDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = RosterMod();
  object.dots = reader.readLongOrNull(offsets[0]);
  object.first = reader.readObjectOrNull<ModStat>(
    offsets[1],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.fourth = reader.readObjectOrNull<ModStat>(
    offsets[2],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.level = reader.readLongOrNull(offsets[3]);
  object.primary = reader.readObjectOrNull<ModStat>(
    offsets[4],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.second = reader.readObjectOrNull<ModStat>(
    offsets[5],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.set = reader.readStringOrNull(offsets[6]);
  object.third = reader.readObjectOrNull<ModStat>(
    offsets[7],
    ModStatSchema.deserialize,
    allOffsets,
  );
  return object;
}

P _rosterModDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 2:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 3:
      return (reader.readLongOrNull(offset)) as P;
    case 4:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 5:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    case 7:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension RosterModQueryFilter
    on QueryBuilder<RosterMod, RosterMod, QFilterCondition> {
  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> dotsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dots',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> dotsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dots',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> dotsEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dots',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> dotsGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dots',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> dotsLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dots',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> dotsBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dots',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> firstIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'first',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> firstIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'first',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> fourthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'fourth',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> fourthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'fourth',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> levelEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> primaryIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'primary',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> primaryIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'primary',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> secondIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'second',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> secondIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'second',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'set',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'set',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'set',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'set',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'set',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> setIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'set',
        value: '',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> thirdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'third',
      ));
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> thirdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'third',
      ));
    });
  }
}

extension RosterModQueryObject
    on QueryBuilder<RosterMod, RosterMod, QFilterCondition> {
  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> first(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'first');
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> fourth(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'fourth');
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> primary(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'primary');
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> second(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'second');
    });
  }

  QueryBuilder<RosterMod, RosterMod, QAfterFilterCondition> third(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'third');
    });
  }
}

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const ModStatSchema = Schema(
  name: r'ModStat',
  id: 3211736682080649080,
  properties: {
    r'name': PropertySchema(
      id: 0,
      name: r'name',
      type: IsarType.string,
    ),
    r'value': PropertySchema(
      id: 1,
      name: r'value',
      type: IsarType.double,
    )
  },
  estimateSize: _modStatEstimateSize,
  serialize: _modStatSerialize,
  deserialize: _modStatDeserialize,
  deserializeProp: _modStatDeserializeProp,
);

int _modStatEstimateSize(
  ModStat object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _modStatSerialize(
  ModStat object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.name);
  writer.writeDouble(offsets[1], object.value);
}

ModStat _modStatDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ModStat();
  object.name = reader.readStringOrNull(offsets[0]);
  object.value = reader.readDoubleOrNull(offsets[1]);
  return object;
}

P _modStatDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readDoubleOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension ModStatQueryFilter
    on QueryBuilder<ModStat, ModStat, QFilterCondition> {
  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'value',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'value',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'value',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }
}

extension ModStatQueryObject
    on QueryBuilder<ModStat, ModStat, QFilterCondition> {}
