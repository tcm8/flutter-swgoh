import 'package:isar/isar.dart';

part 'roster_character.g.dart';

@collection
class RosterCharacter {
  Id id = Isar.autoIncrement;

  @Index(unique: true, replace: true, composite: [CompositeIndex('accountId')])
  String? charId;

  @Index(type: IndexType.hash)
  String? accountId;

  String? name;
  String? url;
  String? alignment;
  String? image;
  List<String>? factions;
  int? level;
  int? relicLevel;
  int? gearLevel;
  int? stars;

  RosterMod? square;
  RosterMod? diamond;
  RosterMod? circle;
  RosterMod? arrow;
  RosterMod? triangle;
  RosterMod? cross;
}

@embedded
class RosterMod {
  String? set;
  int? level;
  int? dots;
  ModStat? primary;
  ModStat? first;
  ModStat? second;
  ModStat? third;
  ModStat? fourth;
}

@embedded
class ModStat {
  String? name;
  double? value;
}
