import 'package:isar/isar.dart';

part 'unit.g.dart';

@collection
class Unit {
  Id id = Isar.autoIncrement;

  @Index(unique: true, replace: true)
  String? unitId;

  String? name;
  String? url;
  String? alignment;
  String? image;
  int? level;
  bool? relic;
  List<String>? factions;
  List<String>? classes;
  bool? activated;
  bool? favorite;

  RecommendedMods? recommendedMods;
  Mods? equippedMods;

  UnitStats? currentStats;
  UnitStats? targetStats;
}

@embedded
class Mods {
  ModSlot? square;
  ModSlot? diamond;
  ModSlot? circle;
  ModSlot? arrow;
  ModSlot? triangle;
  ModSlot? cross;
}

@embedded
class ModSlot {
  String? set;
  String? level;
  int? dots;
  ModStat? primary;
  ModStat? first;
  ModStat? second;
  ModStat? third;
  ModStat? fourth;
}

@embedded
class ModStat {
  String? name;
  double? value;
}

@embedded
class UnitStats {
  double? armor;
  int? damage;
  int? health;
  double? potency;
  int? protection;
  int? relic;
  int? specialDamage;
  int? speed;
  double? tenacity;
}

@embedded
class RecommendedMods {
  List<String>? sets;
  String? triangle;
  String? circle;
  String? cross;
  String? arrow;
}

@embedded
class Kyrotech {
  int? shockProd;
  int? battleComputer;
}
