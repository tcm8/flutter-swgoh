// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unit.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetUnitCollection on Isar {
  IsarCollection<Unit> get units => this.collection();
}

const UnitSchema = CollectionSchema(
  name: r'Unit',
  id: 5852079958688209740,
  properties: {
    r'activated': PropertySchema(
      id: 0,
      name: r'activated',
      type: IsarType.bool,
    ),
    r'alignment': PropertySchema(
      id: 1,
      name: r'alignment',
      type: IsarType.string,
    ),
    r'classes': PropertySchema(
      id: 2,
      name: r'classes',
      type: IsarType.stringList,
    ),
    r'currentStats': PropertySchema(
      id: 3,
      name: r'currentStats',
      type: IsarType.object,
      target: r'UnitStats',
    ),
    r'equippedMods': PropertySchema(
      id: 4,
      name: r'equippedMods',
      type: IsarType.object,
      target: r'Mods',
    ),
    r'factions': PropertySchema(
      id: 5,
      name: r'factions',
      type: IsarType.stringList,
    ),
    r'favorite': PropertySchema(
      id: 6,
      name: r'favorite',
      type: IsarType.bool,
    ),
    r'image': PropertySchema(
      id: 7,
      name: r'image',
      type: IsarType.string,
    ),
    r'level': PropertySchema(
      id: 8,
      name: r'level',
      type: IsarType.long,
    ),
    r'name': PropertySchema(
      id: 9,
      name: r'name',
      type: IsarType.string,
    ),
    r'recommendedMods': PropertySchema(
      id: 10,
      name: r'recommendedMods',
      type: IsarType.object,
      target: r'RecommendedMods',
    ),
    r'relic': PropertySchema(
      id: 11,
      name: r'relic',
      type: IsarType.bool,
    ),
    r'targetStats': PropertySchema(
      id: 12,
      name: r'targetStats',
      type: IsarType.object,
      target: r'UnitStats',
    ),
    r'unitId': PropertySchema(
      id: 13,
      name: r'unitId',
      type: IsarType.string,
    ),
    r'url': PropertySchema(
      id: 14,
      name: r'url',
      type: IsarType.string,
    )
  },
  estimateSize: _unitEstimateSize,
  serialize: _unitSerialize,
  deserialize: _unitDeserialize,
  deserializeProp: _unitDeserializeProp,
  idName: r'id',
  indexes: {
    r'unitId': IndexSchema(
      id: -6311000305075127788,
      name: r'unitId',
      unique: true,
      replace: true,
      properties: [
        IndexPropertySchema(
          name: r'unitId',
          type: IndexType.hash,
          caseSensitive: true,
        )
      ],
    )
  },
  links: {},
  embeddedSchemas: {
    r'RecommendedMods': RecommendedModsSchema,
    r'Mods': ModsSchema,
    r'ModSlot': ModSlotSchema,
    r'ModStat': ModStatSchema,
    r'UnitStats': UnitStatsSchema
  },
  getId: _unitGetId,
  getLinks: _unitGetLinks,
  attach: _unitAttach,
  version: '3.1.0+1',
);

int _unitEstimateSize(
  Unit object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.alignment;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.classes;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.currentStats;
    if (value != null) {
      bytesCount += 3 +
          UnitStatsSchema.estimateSize(
              value, allOffsets[UnitStats]!, allOffsets);
    }
  }
  {
    final value = object.equippedMods;
    if (value != null) {
      bytesCount +=
          3 + ModsSchema.estimateSize(value, allOffsets[Mods]!, allOffsets);
    }
  }
  {
    final list = object.factions;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.image;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.recommendedMods;
    if (value != null) {
      bytesCount += 3 +
          RecommendedModsSchema.estimateSize(
              value, allOffsets[RecommendedMods]!, allOffsets);
    }
  }
  {
    final value = object.targetStats;
    if (value != null) {
      bytesCount += 3 +
          UnitStatsSchema.estimateSize(
              value, allOffsets[UnitStats]!, allOffsets);
    }
  }
  {
    final value = object.unitId;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.url;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _unitSerialize(
  Unit object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeBool(offsets[0], object.activated);
  writer.writeString(offsets[1], object.alignment);
  writer.writeStringList(offsets[2], object.classes);
  writer.writeObject<UnitStats>(
    offsets[3],
    allOffsets,
    UnitStatsSchema.serialize,
    object.currentStats,
  );
  writer.writeObject<Mods>(
    offsets[4],
    allOffsets,
    ModsSchema.serialize,
    object.equippedMods,
  );
  writer.writeStringList(offsets[5], object.factions);
  writer.writeBool(offsets[6], object.favorite);
  writer.writeString(offsets[7], object.image);
  writer.writeLong(offsets[8], object.level);
  writer.writeString(offsets[9], object.name);
  writer.writeObject<RecommendedMods>(
    offsets[10],
    allOffsets,
    RecommendedModsSchema.serialize,
    object.recommendedMods,
  );
  writer.writeBool(offsets[11], object.relic);
  writer.writeObject<UnitStats>(
    offsets[12],
    allOffsets,
    UnitStatsSchema.serialize,
    object.targetStats,
  );
  writer.writeString(offsets[13], object.unitId);
  writer.writeString(offsets[14], object.url);
}

Unit _unitDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = Unit();
  object.activated = reader.readBoolOrNull(offsets[0]);
  object.alignment = reader.readStringOrNull(offsets[1]);
  object.classes = reader.readStringList(offsets[2]);
  object.currentStats = reader.readObjectOrNull<UnitStats>(
    offsets[3],
    UnitStatsSchema.deserialize,
    allOffsets,
  );
  object.equippedMods = reader.readObjectOrNull<Mods>(
    offsets[4],
    ModsSchema.deserialize,
    allOffsets,
  );
  object.factions = reader.readStringList(offsets[5]);
  object.favorite = reader.readBoolOrNull(offsets[6]);
  object.id = id;
  object.image = reader.readStringOrNull(offsets[7]);
  object.level = reader.readLongOrNull(offsets[8]);
  object.name = reader.readStringOrNull(offsets[9]);
  object.recommendedMods = reader.readObjectOrNull<RecommendedMods>(
    offsets[10],
    RecommendedModsSchema.deserialize,
    allOffsets,
  );
  object.relic = reader.readBoolOrNull(offsets[11]);
  object.targetStats = reader.readObjectOrNull<UnitStats>(
    offsets[12],
    UnitStatsSchema.deserialize,
    allOffsets,
  );
  object.unitId = reader.readStringOrNull(offsets[13]);
  object.url = reader.readStringOrNull(offsets[14]);
  return object;
}

P _unitDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readBoolOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringList(offset)) as P;
    case 3:
      return (reader.readObjectOrNull<UnitStats>(
        offset,
        UnitStatsSchema.deserialize,
        allOffsets,
      )) as P;
    case 4:
      return (reader.readObjectOrNull<Mods>(
        offset,
        ModsSchema.deserialize,
        allOffsets,
      )) as P;
    case 5:
      return (reader.readStringList(offset)) as P;
    case 6:
      return (reader.readBoolOrNull(offset)) as P;
    case 7:
      return (reader.readStringOrNull(offset)) as P;
    case 8:
      return (reader.readLongOrNull(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readObjectOrNull<RecommendedMods>(
        offset,
        RecommendedModsSchema.deserialize,
        allOffsets,
      )) as P;
    case 11:
      return (reader.readBoolOrNull(offset)) as P;
    case 12:
      return (reader.readObjectOrNull<UnitStats>(
        offset,
        UnitStatsSchema.deserialize,
        allOffsets,
      )) as P;
    case 13:
      return (reader.readStringOrNull(offset)) as P;
    case 14:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _unitGetId(Unit object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _unitGetLinks(Unit object) {
  return [];
}

void _unitAttach(IsarCollection<dynamic> col, Id id, Unit object) {
  object.id = id;
}

extension UnitByIndex on IsarCollection<Unit> {
  Future<Unit?> getByUnitId(String? unitId) {
    return getByIndex(r'unitId', [unitId]);
  }

  Unit? getByUnitIdSync(String? unitId) {
    return getByIndexSync(r'unitId', [unitId]);
  }

  Future<bool> deleteByUnitId(String? unitId) {
    return deleteByIndex(r'unitId', [unitId]);
  }

  bool deleteByUnitIdSync(String? unitId) {
    return deleteByIndexSync(r'unitId', [unitId]);
  }

  Future<List<Unit?>> getAllByUnitId(List<String?> unitIdValues) {
    final values = unitIdValues.map((e) => [e]).toList();
    return getAllByIndex(r'unitId', values);
  }

  List<Unit?> getAllByUnitIdSync(List<String?> unitIdValues) {
    final values = unitIdValues.map((e) => [e]).toList();
    return getAllByIndexSync(r'unitId', values);
  }

  Future<int> deleteAllByUnitId(List<String?> unitIdValues) {
    final values = unitIdValues.map((e) => [e]).toList();
    return deleteAllByIndex(r'unitId', values);
  }

  int deleteAllByUnitIdSync(List<String?> unitIdValues) {
    final values = unitIdValues.map((e) => [e]).toList();
    return deleteAllByIndexSync(r'unitId', values);
  }

  Future<Id> putByUnitId(Unit object) {
    return putByIndex(r'unitId', object);
  }

  Id putByUnitIdSync(Unit object, {bool saveLinks = true}) {
    return putByIndexSync(r'unitId', object, saveLinks: saveLinks);
  }

  Future<List<Id>> putAllByUnitId(List<Unit> objects) {
    return putAllByIndex(r'unitId', objects);
  }

  List<Id> putAllByUnitIdSync(List<Unit> objects, {bool saveLinks = true}) {
    return putAllByIndexSync(r'unitId', objects, saveLinks: saveLinks);
  }
}

extension UnitQueryWhereSort on QueryBuilder<Unit, Unit, QWhere> {
  QueryBuilder<Unit, Unit, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension UnitQueryWhere on QueryBuilder<Unit, Unit, QWhereClause> {
  QueryBuilder<Unit, Unit, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> unitIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'unitId',
        value: [null],
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> unitIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.between(
        indexName: r'unitId',
        lower: [null],
        includeLower: false,
        upper: [],
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> unitIdEqualTo(String? unitId) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IndexWhereClause.equalTo(
        indexName: r'unitId',
        value: [unitId],
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterWhereClause> unitIdNotEqualTo(String? unitId) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'unitId',
              lower: [],
              upper: [unitId],
              includeUpper: false,
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'unitId',
              lower: [unitId],
              includeLower: false,
              upper: [],
            ));
      } else {
        return query
            .addWhereClause(IndexWhereClause.between(
              indexName: r'unitId',
              lower: [unitId],
              includeLower: false,
              upper: [],
            ))
            .addWhereClause(IndexWhereClause.between(
              indexName: r'unitId',
              lower: [],
              upper: [unitId],
              includeUpper: false,
            ));
      }
    });
  }
}

extension UnitQueryFilter on QueryBuilder<Unit, Unit, QFilterCondition> {
  QueryBuilder<Unit, Unit, QAfterFilterCondition> activatedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'activated',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> activatedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'activated',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> activatedEqualTo(
      bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'activated',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'alignment',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'alignment',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'alignment',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'alignment',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'alignment',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'alignment',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> alignmentIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'alignment',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'classes',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'classes',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'classes',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'classes',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'classes',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'classes',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'classes',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'classes',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'classes',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'classes',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'classes',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'classes',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesLengthEqualTo(
      int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'classes',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'classes',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'classes',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'classes',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'classes',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> classesLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'classes',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> currentStatsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'currentStats',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> currentStatsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'currentStats',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> equippedModsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'equippedMods',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> equippedModsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'equippedMods',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'factions',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'factions',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'factions',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'factions',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'factions',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'factions',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'factions',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsLengthEqualTo(
      int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> factionsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'factions',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> favoriteIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'favorite',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> favoriteIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'favorite',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> favoriteEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'favorite',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> idEqualTo(Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'image',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'image',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'image',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageContains(String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'image',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageMatches(String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'image',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'image',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> imageIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'image',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> levelEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> levelGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> levelLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> levelBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameContains(String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameMatches(String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> recommendedModsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'recommendedMods',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> recommendedModsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'recommendedMods',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> relicIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relic',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> relicIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relic',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> relicEqualTo(bool? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relic',
        value: value,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> targetStatsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'targetStats',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> targetStatsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'targetStats',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'unitId',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'unitId',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unitId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'unitId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'unitId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'unitId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'unitId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'unitId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdContains(String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'unitId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdMatches(String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'unitId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'unitId',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> unitIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'unitId',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'url',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'url',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlContains(String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'url',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlMatches(String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'url',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'url',
        value: '',
      ));
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> urlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'url',
        value: '',
      ));
    });
  }
}

extension UnitQueryObject on QueryBuilder<Unit, Unit, QFilterCondition> {
  QueryBuilder<Unit, Unit, QAfterFilterCondition> currentStats(
      FilterQuery<UnitStats> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'currentStats');
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> equippedMods(
      FilterQuery<Mods> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'equippedMods');
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> recommendedMods(
      FilterQuery<RecommendedMods> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'recommendedMods');
    });
  }

  QueryBuilder<Unit, Unit, QAfterFilterCondition> targetStats(
      FilterQuery<UnitStats> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'targetStats');
    });
  }
}

extension UnitQueryLinks on QueryBuilder<Unit, Unit, QFilterCondition> {}

extension UnitQuerySortBy on QueryBuilder<Unit, Unit, QSortBy> {
  QueryBuilder<Unit, Unit, QAfterSortBy> sortByActivated() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activated', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByActivatedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activated', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByAlignment() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByAlignmentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByFavorite() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'favorite', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByFavoriteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'favorite', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByImage() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByImageDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByRelic() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relic', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByRelicDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relic', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByUnitId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitId', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByUnitIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitId', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> sortByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }
}

extension UnitQuerySortThenBy on QueryBuilder<Unit, Unit, QSortThenBy> {
  QueryBuilder<Unit, Unit, QAfterSortBy> thenByActivated() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activated', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByActivatedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'activated', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByAlignment() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByAlignmentDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'alignment', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByFavorite() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'favorite', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByFavoriteDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'favorite', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByImage() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByImageDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'image', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByLevelDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'level', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'name', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByRelic() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relic', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByRelicDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'relic', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByUnitId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitId', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByUnitIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'unitId', Sort.desc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.asc);
    });
  }

  QueryBuilder<Unit, Unit, QAfterSortBy> thenByUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'url', Sort.desc);
    });
  }
}

extension UnitQueryWhereDistinct on QueryBuilder<Unit, Unit, QDistinct> {
  QueryBuilder<Unit, Unit, QDistinct> distinctByActivated() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'activated');
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByAlignment(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'alignment', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByClasses() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'classes');
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByFactions() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'factions');
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByFavorite() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'favorite');
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByImage(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'image', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByLevel() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'level');
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'name', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByRelic() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'relic');
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByUnitId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'unitId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<Unit, Unit, QDistinct> distinctByUrl(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'url', caseSensitive: caseSensitive);
    });
  }
}

extension UnitQueryProperty on QueryBuilder<Unit, Unit, QQueryProperty> {
  QueryBuilder<Unit, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<Unit, bool?, QQueryOperations> activatedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'activated');
    });
  }

  QueryBuilder<Unit, String?, QQueryOperations> alignmentProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'alignment');
    });
  }

  QueryBuilder<Unit, List<String>?, QQueryOperations> classesProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'classes');
    });
  }

  QueryBuilder<Unit, UnitStats?, QQueryOperations> currentStatsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'currentStats');
    });
  }

  QueryBuilder<Unit, Mods?, QQueryOperations> equippedModsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'equippedMods');
    });
  }

  QueryBuilder<Unit, List<String>?, QQueryOperations> factionsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'factions');
    });
  }

  QueryBuilder<Unit, bool?, QQueryOperations> favoriteProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'favorite');
    });
  }

  QueryBuilder<Unit, String?, QQueryOperations> imageProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'image');
    });
  }

  QueryBuilder<Unit, int?, QQueryOperations> levelProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'level');
    });
  }

  QueryBuilder<Unit, String?, QQueryOperations> nameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'name');
    });
  }

  QueryBuilder<Unit, RecommendedMods?, QQueryOperations>
      recommendedModsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'recommendedMods');
    });
  }

  QueryBuilder<Unit, bool?, QQueryOperations> relicProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'relic');
    });
  }

  QueryBuilder<Unit, UnitStats?, QQueryOperations> targetStatsProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'targetStats');
    });
  }

  QueryBuilder<Unit, String?, QQueryOperations> unitIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'unitId');
    });
  }

  QueryBuilder<Unit, String?, QQueryOperations> urlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'url');
    });
  }
}

// **************************************************************************
// IsarEmbeddedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const ModsSchema = Schema(
  name: r'Mods',
  id: 1148330992183859843,
  properties: {
    r'arrow': PropertySchema(
      id: 0,
      name: r'arrow',
      type: IsarType.object,
      target: r'ModSlot',
    ),
    r'circle': PropertySchema(
      id: 1,
      name: r'circle',
      type: IsarType.object,
      target: r'ModSlot',
    ),
    r'cross': PropertySchema(
      id: 2,
      name: r'cross',
      type: IsarType.object,
      target: r'ModSlot',
    ),
    r'diamond': PropertySchema(
      id: 3,
      name: r'diamond',
      type: IsarType.object,
      target: r'ModSlot',
    ),
    r'square': PropertySchema(
      id: 4,
      name: r'square',
      type: IsarType.object,
      target: r'ModSlot',
    ),
    r'triangle': PropertySchema(
      id: 5,
      name: r'triangle',
      type: IsarType.object,
      target: r'ModSlot',
    )
  },
  estimateSize: _modsEstimateSize,
  serialize: _modsSerialize,
  deserialize: _modsDeserialize,
  deserializeProp: _modsDeserializeProp,
);

int _modsEstimateSize(
  Mods object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.arrow;
    if (value != null) {
      bytesCount += 3 +
          ModSlotSchema.estimateSize(value, allOffsets[ModSlot]!, allOffsets);
    }
  }
  {
    final value = object.circle;
    if (value != null) {
      bytesCount += 3 +
          ModSlotSchema.estimateSize(value, allOffsets[ModSlot]!, allOffsets);
    }
  }
  {
    final value = object.cross;
    if (value != null) {
      bytesCount += 3 +
          ModSlotSchema.estimateSize(value, allOffsets[ModSlot]!, allOffsets);
    }
  }
  {
    final value = object.diamond;
    if (value != null) {
      bytesCount += 3 +
          ModSlotSchema.estimateSize(value, allOffsets[ModSlot]!, allOffsets);
    }
  }
  {
    final value = object.square;
    if (value != null) {
      bytesCount += 3 +
          ModSlotSchema.estimateSize(value, allOffsets[ModSlot]!, allOffsets);
    }
  }
  {
    final value = object.triangle;
    if (value != null) {
      bytesCount += 3 +
          ModSlotSchema.estimateSize(value, allOffsets[ModSlot]!, allOffsets);
    }
  }
  return bytesCount;
}

void _modsSerialize(
  Mods object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeObject<ModSlot>(
    offsets[0],
    allOffsets,
    ModSlotSchema.serialize,
    object.arrow,
  );
  writer.writeObject<ModSlot>(
    offsets[1],
    allOffsets,
    ModSlotSchema.serialize,
    object.circle,
  );
  writer.writeObject<ModSlot>(
    offsets[2],
    allOffsets,
    ModSlotSchema.serialize,
    object.cross,
  );
  writer.writeObject<ModSlot>(
    offsets[3],
    allOffsets,
    ModSlotSchema.serialize,
    object.diamond,
  );
  writer.writeObject<ModSlot>(
    offsets[4],
    allOffsets,
    ModSlotSchema.serialize,
    object.square,
  );
  writer.writeObject<ModSlot>(
    offsets[5],
    allOffsets,
    ModSlotSchema.serialize,
    object.triangle,
  );
}

Mods _modsDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = Mods();
  object.arrow = reader.readObjectOrNull<ModSlot>(
    offsets[0],
    ModSlotSchema.deserialize,
    allOffsets,
  );
  object.circle = reader.readObjectOrNull<ModSlot>(
    offsets[1],
    ModSlotSchema.deserialize,
    allOffsets,
  );
  object.cross = reader.readObjectOrNull<ModSlot>(
    offsets[2],
    ModSlotSchema.deserialize,
    allOffsets,
  );
  object.diamond = reader.readObjectOrNull<ModSlot>(
    offsets[3],
    ModSlotSchema.deserialize,
    allOffsets,
  );
  object.square = reader.readObjectOrNull<ModSlot>(
    offsets[4],
    ModSlotSchema.deserialize,
    allOffsets,
  );
  object.triangle = reader.readObjectOrNull<ModSlot>(
    offsets[5],
    ModSlotSchema.deserialize,
    allOffsets,
  );
  return object;
}

P _modsDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readObjectOrNull<ModSlot>(
        offset,
        ModSlotSchema.deserialize,
        allOffsets,
      )) as P;
    case 1:
      return (reader.readObjectOrNull<ModSlot>(
        offset,
        ModSlotSchema.deserialize,
        allOffsets,
      )) as P;
    case 2:
      return (reader.readObjectOrNull<ModSlot>(
        offset,
        ModSlotSchema.deserialize,
        allOffsets,
      )) as P;
    case 3:
      return (reader.readObjectOrNull<ModSlot>(
        offset,
        ModSlotSchema.deserialize,
        allOffsets,
      )) as P;
    case 4:
      return (reader.readObjectOrNull<ModSlot>(
        offset,
        ModSlotSchema.deserialize,
        allOffsets,
      )) as P;
    case 5:
      return (reader.readObjectOrNull<ModSlot>(
        offset,
        ModSlotSchema.deserialize,
        allOffsets,
      )) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension ModsQueryFilter on QueryBuilder<Mods, Mods, QFilterCondition> {
  QueryBuilder<Mods, Mods, QAfterFilterCondition> arrowIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'arrow',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> arrowIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'arrow',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> circleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'circle',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> circleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'circle',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> crossIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'cross',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> crossIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'cross',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> diamondIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'diamond',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> diamondIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'diamond',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> squareIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'square',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> squareIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'square',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> triangleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'triangle',
      ));
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> triangleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'triangle',
      ));
    });
  }
}

extension ModsQueryObject on QueryBuilder<Mods, Mods, QFilterCondition> {
  QueryBuilder<Mods, Mods, QAfterFilterCondition> arrow(
      FilterQuery<ModSlot> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'arrow');
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> circle(
      FilterQuery<ModSlot> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'circle');
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> cross(
      FilterQuery<ModSlot> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'cross');
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> diamond(
      FilterQuery<ModSlot> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'diamond');
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> square(
      FilterQuery<ModSlot> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'square');
    });
  }

  QueryBuilder<Mods, Mods, QAfterFilterCondition> triangle(
      FilterQuery<ModSlot> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'triangle');
    });
  }
}

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const ModSlotSchema = Schema(
  name: r'ModSlot',
  id: 3878741152215558070,
  properties: {
    r'dots': PropertySchema(
      id: 0,
      name: r'dots',
      type: IsarType.long,
    ),
    r'first': PropertySchema(
      id: 1,
      name: r'first',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'fourth': PropertySchema(
      id: 2,
      name: r'fourth',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'level': PropertySchema(
      id: 3,
      name: r'level',
      type: IsarType.string,
    ),
    r'primary': PropertySchema(
      id: 4,
      name: r'primary',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'second': PropertySchema(
      id: 5,
      name: r'second',
      type: IsarType.object,
      target: r'ModStat',
    ),
    r'set': PropertySchema(
      id: 6,
      name: r'set',
      type: IsarType.string,
    ),
    r'third': PropertySchema(
      id: 7,
      name: r'third',
      type: IsarType.object,
      target: r'ModStat',
    )
  },
  estimateSize: _modSlotEstimateSize,
  serialize: _modSlotSerialize,
  deserialize: _modSlotDeserialize,
  deserializeProp: _modSlotDeserializeProp,
);

int _modSlotEstimateSize(
  ModSlot object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.first;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.fourth;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.level;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.primary;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.second;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  {
    final value = object.set;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.third;
    if (value != null) {
      bytesCount += 3 +
          ModStatSchema.estimateSize(value, allOffsets[ModStat]!, allOffsets);
    }
  }
  return bytesCount;
}

void _modSlotSerialize(
  ModSlot object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.dots);
  writer.writeObject<ModStat>(
    offsets[1],
    allOffsets,
    ModStatSchema.serialize,
    object.first,
  );
  writer.writeObject<ModStat>(
    offsets[2],
    allOffsets,
    ModStatSchema.serialize,
    object.fourth,
  );
  writer.writeString(offsets[3], object.level);
  writer.writeObject<ModStat>(
    offsets[4],
    allOffsets,
    ModStatSchema.serialize,
    object.primary,
  );
  writer.writeObject<ModStat>(
    offsets[5],
    allOffsets,
    ModStatSchema.serialize,
    object.second,
  );
  writer.writeString(offsets[6], object.set);
  writer.writeObject<ModStat>(
    offsets[7],
    allOffsets,
    ModStatSchema.serialize,
    object.third,
  );
}

ModSlot _modSlotDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ModSlot();
  object.dots = reader.readLongOrNull(offsets[0]);
  object.first = reader.readObjectOrNull<ModStat>(
    offsets[1],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.fourth = reader.readObjectOrNull<ModStat>(
    offsets[2],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.level = reader.readStringOrNull(offsets[3]);
  object.primary = reader.readObjectOrNull<ModStat>(
    offsets[4],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.second = reader.readObjectOrNull<ModStat>(
    offsets[5],
    ModStatSchema.deserialize,
    allOffsets,
  );
  object.set = reader.readStringOrNull(offsets[6]);
  object.third = reader.readObjectOrNull<ModStat>(
    offsets[7],
    ModStatSchema.deserialize,
    allOffsets,
  );
  return object;
}

P _modSlotDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 2:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 3:
      return (reader.readStringOrNull(offset)) as P;
    case 4:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 5:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    case 6:
      return (reader.readStringOrNull(offset)) as P;
    case 7:
      return (reader.readObjectOrNull<ModStat>(
        offset,
        ModStatSchema.deserialize,
        allOffsets,
      )) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension ModSlotQueryFilter
    on QueryBuilder<ModSlot, ModSlot, QFilterCondition> {
  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> dotsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'dots',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> dotsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'dots',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> dotsEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'dots',
        value: value,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> dotsGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'dots',
        value: value,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> dotsLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'dots',
        value: value,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> dotsBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'dots',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> firstIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'first',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> firstIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'first',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> fourthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'fourth',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> fourthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'fourth',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'level',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'level',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'level',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'level',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'level',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'level',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'level',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'level',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'level',
        value: '',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> levelIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'level',
        value: '',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> primaryIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'primary',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> primaryIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'primary',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> secondIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'second',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> secondIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'second',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'set',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'set',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'set',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'set',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'set',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'set',
        value: '',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> setIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'set',
        value: '',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> thirdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'third',
      ));
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> thirdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'third',
      ));
    });
  }
}

extension ModSlotQueryObject
    on QueryBuilder<ModSlot, ModSlot, QFilterCondition> {
  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> first(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'first');
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> fourth(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'fourth');
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> primary(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'primary');
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> second(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'second');
    });
  }

  QueryBuilder<ModSlot, ModSlot, QAfterFilterCondition> third(
      FilterQuery<ModStat> q) {
    return QueryBuilder.apply(this, (query) {
      return query.object(q, r'third');
    });
  }
}

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const ModStatSchema = Schema(
  name: r'ModStat',
  id: 3211736682080649080,
  properties: {
    r'name': PropertySchema(
      id: 0,
      name: r'name',
      type: IsarType.string,
    ),
    r'value': PropertySchema(
      id: 1,
      name: r'value',
      type: IsarType.double,
    )
  },
  estimateSize: _modStatEstimateSize,
  serialize: _modStatSerialize,
  deserialize: _modStatDeserialize,
  deserializeProp: _modStatDeserializeProp,
);

int _modStatEstimateSize(
  ModStat object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.name;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _modStatSerialize(
  ModStat object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.name);
  writer.writeDouble(offsets[1], object.value);
}

ModStat _modStatDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = ModStat();
  object.name = reader.readStringOrNull(offsets[0]);
  object.value = reader.readDoubleOrNull(offsets[1]);
  return object;
}

P _modStatDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readDoubleOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension ModStatQueryFilter
    on QueryBuilder<ModStat, ModStat, QFilterCondition> {
  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'name',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'name',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'name',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'name',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> nameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'name',
        value: '',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'value',
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'value',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'value',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'value',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<ModStat, ModStat, QAfterFilterCondition> valueBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'value',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }
}

extension ModStatQueryObject
    on QueryBuilder<ModStat, ModStat, QFilterCondition> {}

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const UnitStatsSchema = Schema(
  name: r'UnitStats',
  id: -2493546816354942523,
  properties: {
    r'armor': PropertySchema(
      id: 0,
      name: r'armor',
      type: IsarType.double,
    ),
    r'damage': PropertySchema(
      id: 1,
      name: r'damage',
      type: IsarType.long,
    ),
    r'health': PropertySchema(
      id: 2,
      name: r'health',
      type: IsarType.long,
    ),
    r'potency': PropertySchema(
      id: 3,
      name: r'potency',
      type: IsarType.double,
    ),
    r'protection': PropertySchema(
      id: 4,
      name: r'protection',
      type: IsarType.long,
    ),
    r'relic': PropertySchema(
      id: 5,
      name: r'relic',
      type: IsarType.long,
    ),
    r'specialDamage': PropertySchema(
      id: 6,
      name: r'specialDamage',
      type: IsarType.long,
    ),
    r'speed': PropertySchema(
      id: 7,
      name: r'speed',
      type: IsarType.long,
    ),
    r'tenacity': PropertySchema(
      id: 8,
      name: r'tenacity',
      type: IsarType.double,
    )
  },
  estimateSize: _unitStatsEstimateSize,
  serialize: _unitStatsSerialize,
  deserialize: _unitStatsDeserialize,
  deserializeProp: _unitStatsDeserializeProp,
);

int _unitStatsEstimateSize(
  UnitStats object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  return bytesCount;
}

void _unitStatsSerialize(
  UnitStats object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeDouble(offsets[0], object.armor);
  writer.writeLong(offsets[1], object.damage);
  writer.writeLong(offsets[2], object.health);
  writer.writeDouble(offsets[3], object.potency);
  writer.writeLong(offsets[4], object.protection);
  writer.writeLong(offsets[5], object.relic);
  writer.writeLong(offsets[6], object.specialDamage);
  writer.writeLong(offsets[7], object.speed);
  writer.writeDouble(offsets[8], object.tenacity);
}

UnitStats _unitStatsDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = UnitStats();
  object.armor = reader.readDoubleOrNull(offsets[0]);
  object.damage = reader.readLongOrNull(offsets[1]);
  object.health = reader.readLongOrNull(offsets[2]);
  object.potency = reader.readDoubleOrNull(offsets[3]);
  object.protection = reader.readLongOrNull(offsets[4]);
  object.relic = reader.readLongOrNull(offsets[5]);
  object.specialDamage = reader.readLongOrNull(offsets[6]);
  object.speed = reader.readLongOrNull(offsets[7]);
  object.tenacity = reader.readDoubleOrNull(offsets[8]);
  return object;
}

P _unitStatsDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readDoubleOrNull(offset)) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    case 2:
      return (reader.readLongOrNull(offset)) as P;
    case 3:
      return (reader.readDoubleOrNull(offset)) as P;
    case 4:
      return (reader.readLongOrNull(offset)) as P;
    case 5:
      return (reader.readLongOrNull(offset)) as P;
    case 6:
      return (reader.readLongOrNull(offset)) as P;
    case 7:
      return (reader.readLongOrNull(offset)) as P;
    case 8:
      return (reader.readDoubleOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension UnitStatsQueryFilter
    on QueryBuilder<UnitStats, UnitStats, QFilterCondition> {
  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> armorIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'armor',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> armorIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'armor',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> armorEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'armor',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> armorGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'armor',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> armorLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'armor',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> armorBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'armor',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> damageIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'damage',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> damageIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'damage',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> damageEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'damage',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> damageGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'damage',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> damageLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'damage',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> damageBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'damage',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> healthIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'health',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> healthIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'health',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> healthEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'health',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> healthGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'health',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> healthLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'health',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> healthBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'health',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> potencyIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'potency',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> potencyIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'potency',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> potencyEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'potency',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> potencyGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'potency',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> potencyLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'potency',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> potencyBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'potency',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> protectionIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'protection',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      protectionIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'protection',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> protectionEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'protection',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      protectionGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'protection',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> protectionLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'protection',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> protectionBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'protection',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> relicIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'relic',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> relicIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'relic',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> relicEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'relic',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> relicGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'relic',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> relicLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'relic',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> relicBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'relic',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      specialDamageIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'specialDamage',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      specialDamageIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'specialDamage',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      specialDamageEqualTo(int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'specialDamage',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      specialDamageGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'specialDamage',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      specialDamageLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'specialDamage',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      specialDamageBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'specialDamage',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> speedIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'speed',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> speedIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'speed',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> speedEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'speed',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> speedGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'speed',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> speedLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'speed',
        value: value,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> speedBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'speed',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> tenacityIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'tenacity',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition>
      tenacityIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'tenacity',
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> tenacityEqualTo(
    double? value, {
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'tenacity',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> tenacityGreaterThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'tenacity',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> tenacityLessThan(
    double? value, {
    bool include = false,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'tenacity',
        value: value,
        epsilon: epsilon,
      ));
    });
  }

  QueryBuilder<UnitStats, UnitStats, QAfterFilterCondition> tenacityBetween(
    double? lower,
    double? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    double epsilon = Query.epsilon,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'tenacity',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        epsilon: epsilon,
      ));
    });
  }
}

extension UnitStatsQueryObject
    on QueryBuilder<UnitStats, UnitStats, QFilterCondition> {}

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const RecommendedModsSchema = Schema(
  name: r'RecommendedMods',
  id: 8208751155011274779,
  properties: {
    r'arrow': PropertySchema(
      id: 0,
      name: r'arrow',
      type: IsarType.string,
    ),
    r'circle': PropertySchema(
      id: 1,
      name: r'circle',
      type: IsarType.string,
    ),
    r'cross': PropertySchema(
      id: 2,
      name: r'cross',
      type: IsarType.string,
    ),
    r'sets': PropertySchema(
      id: 3,
      name: r'sets',
      type: IsarType.stringList,
    ),
    r'triangle': PropertySchema(
      id: 4,
      name: r'triangle',
      type: IsarType.string,
    )
  },
  estimateSize: _recommendedModsEstimateSize,
  serialize: _recommendedModsSerialize,
  deserialize: _recommendedModsDeserialize,
  deserializeProp: _recommendedModsDeserializeProp,
);

int _recommendedModsEstimateSize(
  RecommendedMods object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.arrow;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.circle;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.cross;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final list = object.sets;
    if (list != null) {
      bytesCount += 3 + list.length * 3;
      {
        for (var i = 0; i < list.length; i++) {
          final value = list[i];
          bytesCount += value.length * 3;
        }
      }
    }
  }
  {
    final value = object.triangle;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  return bytesCount;
}

void _recommendedModsSerialize(
  RecommendedMods object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.arrow);
  writer.writeString(offsets[1], object.circle);
  writer.writeString(offsets[2], object.cross);
  writer.writeStringList(offsets[3], object.sets);
  writer.writeString(offsets[4], object.triangle);
}

RecommendedMods _recommendedModsDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = RecommendedMods();
  object.arrow = reader.readStringOrNull(offsets[0]);
  object.circle = reader.readStringOrNull(offsets[1]);
  object.cross = reader.readStringOrNull(offsets[2]);
  object.sets = reader.readStringList(offsets[3]);
  object.triangle = reader.readStringOrNull(offsets[4]);
  return object;
}

P _recommendedModsDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readStringOrNull(offset)) as P;
    case 2:
      return (reader.readStringOrNull(offset)) as P;
    case 3:
      return (reader.readStringList(offset)) as P;
    case 4:
      return (reader.readStringOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension RecommendedModsQueryFilter
    on QueryBuilder<RecommendedMods, RecommendedMods, QFilterCondition> {
  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'arrow',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'arrow',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'arrow',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'arrow',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'arrow',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'arrow',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'arrow',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'arrow',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'arrow',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'arrow',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'arrow',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      arrowIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'arrow',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'circle',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'circle',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'circle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'circle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'circle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'circle',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'circle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'circle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'circle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'circle',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'circle',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      circleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'circle',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'cross',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'cross',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'cross',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'cross',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'cross',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'cross',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'cross',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'cross',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'cross',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'cross',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'cross',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      crossIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'cross',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'sets',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'sets',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sets',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'sets',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'sets',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'sets',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'sets',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'sets',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'sets',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'sets',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'sets',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsElementIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'sets',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsLengthEqualTo(int length) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'sets',
        length,
        true,
        length,
        true,
      );
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'sets',
        0,
        true,
        0,
        true,
      );
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'sets',
        0,
        false,
        999999,
        true,
      );
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsLengthLessThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'sets',
        0,
        true,
        length,
        include,
      );
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsLengthGreaterThan(
    int length, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'sets',
        length,
        include,
        999999,
        true,
      );
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      setsLengthBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.listLength(
        r'sets',
        lower,
        includeLower,
        upper,
        includeUpper,
      );
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'triangle',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'triangle',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'triangle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'triangle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'triangle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'triangle',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'triangle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'triangle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'triangle',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'triangle',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'triangle',
        value: '',
      ));
    });
  }

  QueryBuilder<RecommendedMods, RecommendedMods, QAfterFilterCondition>
      triangleIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'triangle',
        value: '',
      ));
    });
  }
}

extension RecommendedModsQueryObject
    on QueryBuilder<RecommendedMods, RecommendedMods, QFilterCondition> {}

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

const KyrotechSchema = Schema(
  name: r'Kyrotech',
  id: -8388741076530918362,
  properties: {
    r'battleComputer': PropertySchema(
      id: 0,
      name: r'battleComputer',
      type: IsarType.long,
    ),
    r'shockProd': PropertySchema(
      id: 1,
      name: r'shockProd',
      type: IsarType.long,
    )
  },
  estimateSize: _kyrotechEstimateSize,
  serialize: _kyrotechSerialize,
  deserialize: _kyrotechDeserialize,
  deserializeProp: _kyrotechDeserializeProp,
);

int _kyrotechEstimateSize(
  Kyrotech object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  return bytesCount;
}

void _kyrotechSerialize(
  Kyrotech object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.battleComputer);
  writer.writeLong(offsets[1], object.shockProd);
}

Kyrotech _kyrotechDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = Kyrotech();
  object.battleComputer = reader.readLongOrNull(offsets[0]);
  object.shockProd = reader.readLongOrNull(offsets[1]);
  return object;
}

P _kyrotechDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLongOrNull(offset)) as P;
    case 1:
      return (reader.readLongOrNull(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

extension KyrotechQueryFilter
    on QueryBuilder<Kyrotech, Kyrotech, QFilterCondition> {
  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition>
      battleComputerIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'battleComputer',
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition>
      battleComputerIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'battleComputer',
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> battleComputerEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'battleComputer',
        value: value,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition>
      battleComputerGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'battleComputer',
        value: value,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition>
      battleComputerLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'battleComputer',
        value: value,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> battleComputerBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'battleComputer',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> shockProdIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'shockProd',
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> shockProdIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'shockProd',
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> shockProdEqualTo(
      int? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'shockProd',
        value: value,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> shockProdGreaterThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'shockProd',
        value: value,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> shockProdLessThan(
    int? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'shockProd',
        value: value,
      ));
    });
  }

  QueryBuilder<Kyrotech, Kyrotech, QAfterFilterCondition> shockProdBetween(
    int? lower,
    int? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'shockProd',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension KyrotechQueryObject
    on QueryBuilder<Kyrotech, Kyrotech, QFilterCondition> {}
