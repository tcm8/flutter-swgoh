import 'package:flutter_swgoh/schema/units/unit.dart';

final emptyStats = UnitStats()
  ..armor = 0
  ..specialDamage = 0
  ..damage = 0
  ..protection = 0
  ..tenacity = 0
  ..potency = 0
  ..relic = 0
  ..speed = 0
  ..health = 0;
