import 'package:flutter/material.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:flutter_swgoh/shared/state/app.state.dart';
import 'package:flutter_swgoh/shared/state/path_state.dart';
import 'package:flutter_swgoh/tabs_widget.dart';
import 'package:watch_it/watch_it.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  initializeSingletons();
  await GetIt.I.allReady();
  runApp(AppWidget());
}

class AppWidget extends StatelessWidget with WatchItMixin {
  AppWidget({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final darkThemeData = watchValue((AppState state) => state.darkThemeData);
    final lightThemeData = watchValue((AppState state) => state.lightThemeData);
    final themeMode = watchValue((AppState state) => state.themeMode);

    return MaterialApp(
        title: 'TCM SWGOH',
        theme: lightThemeData,
        darkTheme: darkThemeData,
        themeMode: themeMode,
        home: const TabsWidget());
  }
}

void initializeSingletons() {
  GetIt.I.registerSingleton(AppState());
  GetIt.I.registerLazySingleton(() => SwgohService());
  GetIt.I.registerSingletonAsync<PathState>(() async {
    final path = PathState();
    await path.init();
    return path;
  });
  GetIt.I.registerSingletonAsync<IsarService>(() async => IsarService().init(),
      dependsOn: [PathState]);
}
