import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_swgoh/shared/state/path_state.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/services.dart';

class ImageHandler {
  static const String imgDomain = "https://game-assets.swgoh.gg";
  static final pathState = GetIt.I<PathState>();

  static Future<Image> getImage(
      {required List<String> path, required double height}) async {
    if (path[1].isEmpty) {
      throw Exception('image path is empty');
    }

    String filePath = '${pathState.docPath}/${path.join('/')}';
    File storedImage = File(filePath);
    var exists = storedImage.existsSync();
    if (exists) {
      return Image(height: height, image: FileImage(storedImage));
    } else {
      var newFile = await _tryAsset(path: path);
      return Image(height: height, image: FileImage(newFile));
    }
  }

  static Future<File> _tryAsset({required List<String> path}) async {
    try {
      ByteData assetData = await rootBundle.load(path.join('/'));
      var buffer = assetData.buffer;
      String filePath = '${pathState.docPath}/${path.join('/')}';
      File newFile = File(filePath);
      newFile.create(recursive: true);
      await newFile.writeAsBytes(
          buffer.asUint8List(assetData.offsetInBytes, assetData.lengthInBytes));

      return newFile;
    } catch (e) {
      return await _loadFromNetwork(path: path);
    }
  }

  static Future<File> _loadFromNetwork({required List<String> path}) async {
    File newFile = File('${pathState.docPath}/${path.join('/')}');
    newFile.create(recursive: true);

    try {
      var request = Uri.parse('$imgDomain/${path[1]}');
      var response = await http.get(request);

      File newFile = File('${pathState.docPath}/${path.join('/')}');
      newFile.create(recursive: true);

      await newFile.writeAsBytes(response.bodyBytes);
      return newFile;
    } catch (ex) {
      print(ex);
      rethrow;
    }
  }
}
