import 'package:flutter/material.dart';
import 'package:flutter_swgoh/data/stock_characters_provider.dart';
// import 'package:flutter_swgoh/pages/stock/state/stock_filter_state.dart';
import 'package:get_it/get_it.dart';

// import './checkbox_list_widget.dart';

class CharacterFiltersDialog extends StatelessWidget {
  CharacterFiltersDialog({Key? key}) : super(key: key);

  final tabStyles = const TextStyle(fontSize: 18, height: 2);
  final stockCharacterProvider = GetIt.I<StockCharacterProvider>();
  // final filterState = GetIt.I<StockFilterState>();
  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);

    return DefaultTabController(
        length: 3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TabBar(
                labelPadding: const EdgeInsets.only(bottom: 2.0),
                unselectedLabelColor: theme.colorScheme.onSurfaceVariant,
                indicator: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: theme.colorScheme.primary, width: 3.0)),
                    color: theme.colorScheme.surfaceVariant),
                tabs: [
                  Text("Factions", style: tabStyles),
                  Text("Abilities", style: tabStyles),
                  Text("Mods", style: tabStyles)
                ]),
            // Expanded(
            //   child: TabBarView(children: [
            //     Center(
            //         child: CheckBoxListWidget(
            //             itemList: stockCharacterProvider.factionList,
            //             selectState: filterState.selectedFactions,
            //             callback: _factionCallback)),
            //     Center(
            //         child: CheckBoxListWidget(
            //             itemList: stockCharacterProvider.abilityList,
            //             selectState: // filterState.selectedAbilities,
            //             callback: _abilityCallback)),
            //     const Center(child: Text('Mods'))
            //   ]),
            // )
          ],
        ));
  }

  _factionCallback(String selection) {
    // filterState.selectFaction(selection);
  }

  _abilityCallback(String selection) {
    // filterState.selectAbility(selection);
  }
}
