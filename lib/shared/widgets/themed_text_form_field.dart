import 'package:flutter/material.dart';

class ThemedTextFormField extends StatelessWidget {

  final FormFieldSetter<String> onSaved;

  const ThemedTextFormField({Key? key, required this.onSaved}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onSaved: onSaved,
      style: const TextStyle(fontSize: 23),
      decoration: const InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(width: 2)),
          isDense: true,
          contentPadding: EdgeInsets.symmetric(
              vertical: 6, horizontal: 5)),
    );
  }
}
