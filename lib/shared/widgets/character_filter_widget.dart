import 'package:flutter/material.dart';
// import 'package:flutter_swgoh/pages/stock/state/stock_filter_state.dart';
// import 'package:get_it/get_it.dart';

import 'characters_filter_dialog.dart';

class CharacterFilterWidget extends StatefulWidget {
  const CharacterFilterWidget({Key? key}) : super(key: key);

  @override
  State<CharacterFilterWidget> createState() => _CharacterFilterWidgetState();
}

class _CharacterFilterWidgetState extends State<CharacterFilterWidget> {
  bool showTextSearch = true;
  // final StockFilterState filterState = GetIt.I<StockFilterState>();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            border:
                Border(top: BorderSide(color: Theme.of(context).dividerColor))),
        child: Row(children: [
          SizedBox(
            height: 45,
            width: 45,
            child: Container(
              margin: const EdgeInsets.all(2),
              decoration: BoxDecoration(
                  border: Border.all(),
                  borderRadius: const BorderRadius.all(Radius.circular(6))),
              child: const Icon(Icons.search, semanticLabel: 'Search'),
            ),
          ),
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(top: 3, bottom: 3),
                  child: TextField(
                    style: const TextStyle(fontSize: 23),
                    onChanged: (text) {
                      // filterState.setTextFilter(text);
                    },
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 2)),
                        isDense: true,
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 6, horizontal: 5)),
                  ))),
          InkWell(
              splashColor: Theme.of(context).colorScheme.secondary,
              onTap: () {
                setState(() {
                  showTextSearch = false;
                });
                showDialog(
                    context: context,
                    builder: (context) => Dialog(
                        insetPadding: const EdgeInsets.fromLTRB(0, 0, 0, 102),
                        child: CharacterFiltersDialog()));
              },
              child: SizedBox(
                height: 45,
                width: 45,
                child: Container(
                  margin: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      border: Border.all(),
                      borderRadius: const BorderRadius.all(Radius.circular(6))),
                  child:
                      const Icon(Icons.filter_list, semanticLabel: 'Filters'),
                ),
              )),
        ]));
  }
}
