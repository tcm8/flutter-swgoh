import 'package:flutter/material.dart';

class CheckBoxListWidget extends StatefulWidget {
  final Iterable itemList;
  final Iterable selectState;
  final Function callback;

  const CheckBoxListWidget(
      {Key? key,
      required this.itemList,
      required this.selectState,
      required this.callback})
      : super(key: key);

  @override
  State<CheckBoxListWidget> createState() => _CheckBoxListWidgetState();
}

class _CheckBoxListWidgetState extends State<CheckBoxListWidget> {
  late Map<String, bool> boxesState;
  String opState = 'any';

  @override
  void initState() {
    boxesState = {
      for (var item in widget.itemList) item: widget.selectState.contains(item)
    };
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: RadioListTile(
                  title: const Text('Any'),
                  value: 'any',
                  groupValue: opState,
                  onChanged: (String? val) => setState(() {
                        opState = val!;
                      })),
            ),
            Expanded(
              child: RadioListTile(
                  title: const Text('All'),
                  value: 'all',
                  groupValue: opState,
                  onChanged: (String? val) => setState(() {
                        opState = val!;
                      })),
            )
          ],
        ),
        Expanded(
          child: GridView.count(
              crossAxisCount: 2,
              shrinkWrap: true,
              childAspectRatio: 1 / .25,
              children: widget.itemList.map((element) {
                return CheckboxListTile(
                    value: boxesState[element],
                    title: Text(element),
                    onChanged: (bool? value) {
                      setState(() {
                        boxesState[element] = value ?? false;
                      });
                      widget.callback(element);
                    });
              }).toList()),
        ),
      ],
    );
  }
}
