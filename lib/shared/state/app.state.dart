import 'package:flutter/material.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:flutter_swgoh/theme/dark_side/theme_dark.dart';
import 'package:flutter_swgoh/theme/light_side/theme_light.dart';
import 'package:get_it/get_it.dart';

import '../../schema/account.dart';
import '../../theme/dark_side/theme_light.dart';
import '../../theme/light_side/theme_dark.dart';

class AppState {
  final _themeMode = ValueNotifier<ThemeMode>(ThemeMode.system);
  final _lightThemeData =
      ValueNotifier<ThemeData>(LightSideThemeLight.themeData);
  final _darkThemeData = ValueNotifier<ThemeData>(LightSideThemeDark.themeData);

  get themeMode => _themeMode;
  get lightThemeData => _lightThemeData;
  get darkThemeData => _darkThemeData;

  setThemeMode(bool isDark) {
    _themeMode.value = isDark ? ThemeMode.dark : ThemeMode.light;
  }

  setThemeData(bool isDark) {
    if (isDark) {
      _lightThemeData.value = DarkSideThemeLight.themeData;
      _darkThemeData.value = DarkSideThemeDark.themeData;
    } else {
      _lightThemeData.value = LightSideThemeLight.themeData;
      _darkThemeData.value = LightSideThemeDark.themeData;
    }
  }
}
