import 'package:path_provider/path_provider.dart';

class PathState {
  String docPath = "";

  Future<void> init() async {
    final dir = await getApplicationDocumentsDirectory();
    docPath = dir.path;
  }
}
