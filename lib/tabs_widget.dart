import 'package:flutter/material.dart';
import 'package:flutter_swgoh/old-pages/ships/ships_page.dart';
import 'package:flutter_swgoh/pages/fleets_page.dart';
import 'package:flutter_swgoh/pages/units/units_page.dart';
import 'package:flutter_swgoh/pages/settings_page.dart';
import 'package:flutter_swgoh/schema/account.dart';
import 'package:flutter_swgoh/services/isar_service.dart';
import 'package:flutter_swgoh/services/swgoh_gg/swgoh_service.dart';
import 'package:get_it/get_it.dart';

import 'pages/squads_page.dart';
import 'swgoh_icons.dart';

class TabsWidget extends StatefulWidget {
  const TabsWidget({Key? key}) : super(key: key);

  @override
  TabsWidgetState createState() => TabsWidgetState();
}

class TabsWidgetState extends State<TabsWidget> {
  int currentIndex = 0;
  final _isar = GetIt.I<IsarService>();
  final _swgoh = GetIt.I<SwgohService>();
  final pages = [
    UnitsPage(),
    const ShipsPage(),
    const SquadsPage(),
    const FleetsPage(),
    const SettingsPage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: RefreshIndicator(
            onRefresh: _refreshData, child: pages[currentIndex]),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentIndex,
          showUnselectedLabels: true,
          onTap: (index) => setState(() => currentIndex = index),
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.person_outline), label: 'Units'),
            BottomNavigationBarItem(
                icon: Icon(SwgohIcons.millenniumFalcon2), label: 'Ships'),
            BottomNavigationBarItem(icon: Icon(Icons.group), label: 'Squads'),
            BottomNavigationBarItem(
                icon: Icon(SwgohIcons.venatorNegotiator), label: 'Fleet'),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), label: 'Settings'),
          ],
        ));
  }

  Future<void> _refreshData() async {
    List<Account> accounts = await _isar.allAccounts();
    for(final acct in accounts) {
      bool selected = acct.selected ?? false;
      Account fresh = await _swgoh.fetchAccountInfo(acct.acctId!);
      fresh.selected = selected;
      await _isar.saveAccount(fresh);
    }
  }
}
